import fs from "fs";
import glob from "glob";
import path from "path";

import babel from "@rollup/plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import yaml from "@rollup/plugin-yaml";
import external from "rollup-plugin-peer-deps-external";

const config = glob
  .sync("packages/ui-*/")
  .map((dirname) => dirname.replace(/\/$/, ""))
  .map((dirname) => {
    const pkgPath = path.resolve(`${dirname}/package.json`);
    const pkg = JSON.parse(fs.readFileSync(pkgPath, "utf8"));

    return {
      input: path.resolve(`${dirname}/src/index.js`),
      output: {
        file: path.resolve(`${dirname}/${pkg.main}`),
        format: "esm",
      },
      external: ["path"],
      plugins: [
        yaml(),
        babel({
          babelHelpers: "bundled",
          babelrc: false,
          exclude: "node_modules/**",
          presets: [
            ["@babel/preset-env", { targets: "node 13" }],
            "@babel/preset-react",
          ],
        }),
        external({
          packageJsonPath: pkgPath,
        }),
        resolve(),
        commonjs(),
      ],
    };
  });

export default config;
