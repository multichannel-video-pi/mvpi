#!/usr/bin/env node

import config from "@mvpi/config";
import { Video } from "@mvpi/models";
import { program } from "commander";

const logger = console;

program.option(
  "--video-directory <videoDirectory>",
  "video directory",
  config.videos.directory
);

program.parse(process.argv);

const { videoDirectory } = program.opts();

(async () => {
  try {
    logger.info(
      `video directory simplification for paths that include: ${videoDirectory}`
    );

    const videos = await Video.select([
      [Video.column`filename`, "LIKE", `${videoDirectory}%`],
    ]);

    await Promise.all(
      videos.map(async (video) =>
        Video.updateByKey({
          id: video.id,
          filename: Video.getRelativePathToVideo(video.filename),
        })
      )
    );

    logger.info(`done -- ${videos.length} video records updated`);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
