#!/bin/bash

xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences

sleep 60

/usr/bin/chromium-browser --noerrdialogs --disable-infobars --incognito --kiosk http://127.0.0.1:8080/media &

sleep 30

while true; do
  xdotool keydown f
  xdotool keyup f
  sleep 86400
done
