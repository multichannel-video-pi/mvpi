#!/usr/bin/env node

import assert from "assert";
import mongodb from "mongodb";
import normalizr from "normalizr";
import util from "util";

import {
  Screenshot,
  Tag,
  TelevisionShow,
  TelevisionEpisode,
  Video,
  VideoTag,
} from "@mvpi/models";

const mongoClientConnect = util.promisify(mongodb.MongoClient.connect);

const { normalize, schema } = normalizr;
const logger = console;
const { MONGO_INITDB_ROOT_USERNAME, MONGO_INITDB_ROOT_PASSWORD } = process.env;

const VIDEO_MIGRATION_PROPS = [
  "bitrate",
  "duration",
  "filename",
  "isActive",
  "overview",
  "rating",
  "size",
  "title",
];

const SCREENSHOT_SCHEMA = new schema.Entity(
  "screenshots",
  {},
  { idAttribute: "filename" }
);
const TELEVISION_SHOW_SCHEMA = new schema.Entity(
  "televisionShows",
  {},
  { idAttribute: "name" }
);
const TELEVISION_EPISODE_SCEMA = new schema.Entity(
  "televisionEpisodes",
  {},
  { idAttribute: "videoId" }
);
const TAG_SCHEMA = new schema.Entity("tags", {}, { idAttribute: "name" });
const VIDEO_SCHEMA = new schema.Entity(
  "videos",
  {
    episode: TELEVISION_EPISODE_SCEMA,
    series: TELEVISION_SHOW_SCHEMA,
    screenshots: [SCREENSHOT_SCHEMA],
    tags: [TAG_SCHEMA],
  },
  {
    idAttribute: "filename",
    processStrategy: (video) => ({
      ...video,
      episode: video.series
        ? {
            episodeNumber: video.episode || null,
            seasonNumber: video.season || null,
            televisionShowId: video.series,
            videoId: video.filename,
          }
        : null,
      overview: video.description || null,
      series: video.series ? { name: video.series } : null,
      screenshots: video.snapshots
        ? video.snapshots.map((screenshot) => ({
            filename: screenshot,
            videoId: video.filename,
          }))
        : [],
      tags: video.tags ? video.tags.map((tag) => ({ name: tag })) : [],
    }),
  }
);

assert(
  MONGO_INITDB_ROOT_USERNAME,
  "MONGO_INITDB_ROOT_USERNAME is a required env variable"
);
assert(
  MONGO_INITDB_ROOT_PASSWORD,
  "MONGO_INITDB_ROOT_PASSWORD is a required env variable"
);

(async () => {
  const password = encodeURIComponent(MONGO_INITDB_ROOT_PASSWORD);
  const username = encodeURIComponent(MONGO_INITDB_ROOT_USERNAME);
  const url = `mongodb://${username}:${password}@127.0.0.1:27017/?authMechanism=DEFAULT`;

  const client = await mongoClientConnect(url, { useUnifiedTopology: true });
  const videosCollection = client.db("miketv").collection("videos");
  logger.info("connected to mongodb");

  const videosQueryResult = videosCollection.find({});
  const mongoDbVideos = await util
    .promisify(videosQueryResult.toArray)
    .call(videosQueryResult);

  const { entities } = normalize(mongoDbVideos, [VIDEO_SCHEMA]);

  const tagsIds = Object.fromEntries(
    await Promise.all(
      Object.values(entities.tags).map(async (tag) => {
        const { id } = await Tag.insert(tag);
        return [tag.name, id];
      })
    )
  );

  const videoIds = Object.fromEntries(
    await Promise.all(
      Object.values(entities.videos).map(async (video) => {
        const sanitizedVideo = Object.fromEntries(
          Object.entries(video).filter(([key]) =>
            VIDEO_MIGRATION_PROPS.includes(key)
          )
        );
        const { id } = await Video.insert(sanitizedVideo);
        await Promise.all(
          video.tags.map(async (tag) =>
            VideoTag.insert({ tagId: tagsIds[tag], videoId: id })
          )
        );
        return [video.filename, id];
      })
    )
  );

  const televisionShowIds = Object.fromEntries(
    await Promise.all(
      Object.values(entities.televisionShows).map(async (televisionShow) => {
        const { id } = await TelevisionShow.insert(televisionShow);
        return [televisionShow.name, id];
      })
    )
  );

  await Promise.all(
    Object.values(entities.screenshots)
      .map((screenshot) => ({
        ...screenshot,
        videoId: videoIds[screenshot.videoId],
      }))
      .map(async (screenshot) => Screenshot.insert(screenshot))
  );

  await Promise.all(
    Object.values(entities.televisionEpisodes)
      .map((televisionEpisode) => ({
        ...televisionEpisode,
        televisionShowId: televisionShowIds[televisionEpisode.televisionShowId],
        videoId: videoIds[televisionEpisode.videoId],
      }))
      .map(async (televisionEpisode) =>
        TelevisionEpisode.insert(televisionEpisode)
      )
  );

  // logger.info(sqliteVideos);
  logger.info("db migration complete");
  return process.exit(0);
})().catch((err) => {
  logger.error("failed to migrate db");
  logger.error(err);
  return process.exit(1);
});
