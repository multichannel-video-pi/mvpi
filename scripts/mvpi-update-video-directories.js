#!/usr/bin/env node

import config from "@mvpi/config";
import { Video } from "@mvpi/models";
import { program } from "commander";

const logger = console;

program
  .option(
    "-o, --old-video-directory <oldVideoDirectory>",
    "old video directory",
    config.videos.directory
  )
  .option(
    "-n, --new-video-directory <newVideoDirectory>",
    "new video directory",
    config.videos.directory
  );

program.parse(process.argv);

const { newVideoDirectory, oldVideoDirectory } = program.opts();

(async () => {
  try {
    if (newVideoDirectory === oldVideoDirectory) {
      throw new Error(`video directories are the same: ${newVideoDirectory}`);
    }

    logger.info(
      `video directory migration: ${oldVideoDirectory} -> ${newVideoDirectory}`
    );

    const videos = await Video.select([
      [Video.column`filename`, "LIKE", `${oldVideoDirectory}%`],
    ]);

    await Promise.all(
      videos.map(async (video) =>
        Video.updateByKey({
          id: video.id,
          filename: video.filename.replace(
            oldVideoDirectory,
            newVideoDirectory
          ),
        })
      )
    );

    logger.info(`done -- ${videos.length} video records updated`);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
