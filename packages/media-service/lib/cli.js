#!/usr/bin/env node

import config from "@mvpi/config";
import createMediaService from "./index.js";
import logger from "./logger.js";

(async () => {
  try {
    const app = await createMediaService(config.media.service.prefix);
    app.listen(config.media.service.port, () => {
      logger.info(
        `media service listening on port ${config.media.service.port}`
      );
    });
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
