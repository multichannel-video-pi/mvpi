import database from "@mvpi/database";
import cors from "@koa/cors";
import Koa from "koa";
import koaPino from "koa-pino-logger";

import {
  koaErrorHandler,
  koaErrorMiddleware,
  NotFoundError,
} from "@mvpi/errors";

import createRouter from "./createRouter.js";
import logger from "./logger.js";

const createMediaService = async (prefix) => {
  logger.debug("creating media service");
  await database();

  const app = new Koa();
  const router = createRouter(prefix);

  app.use(koaPino({ logger }));
  app.use(koaErrorMiddleware(logger));
  app.use(cors());

  app.use(router.routes());
  app.use(router.allowedMethods());

  app.use(async () => {
    throw new NotFoundError();
  });

  app.on("error", koaErrorHandler(logger));

  return app;
};

export default createMediaService;
