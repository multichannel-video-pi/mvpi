import config from "@mvpi/config";
import pino from "pino";

const logger = pino({ ...config.logger, name: "media-service" });

export default logger;
