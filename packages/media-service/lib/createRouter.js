import Router from "@koa/router";

import getVideoScreenshot from "./middleware/getVideoScreenshot.js";
import streamVideo from "./middleware/streamVideo.js";

const createRouter = (prefix) => {
  const router = prefix ? new Router({ prefix }) : new Router();

  router
    .get("/screenshots/:id", getVideoScreenshot)
    .get("/videos/:id", streamVideo);

  return router;
};

export default createRouter;
