import { Screenshot } from "@mvpi/models";
import fs from "fs";
import mime from "mime";

const getVideoScreenshot = async (ctx) => {
  const { filename } = await Screenshot.selectByKey({ id: ctx.params.id });
  ctx.set("Content-Type", mime.getType(filename));
  ctx.body = fs.createReadStream(filename);
};

export default getVideoScreenshot;
