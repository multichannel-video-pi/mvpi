import { createReadStream } from "fs";

import getMemoizedVideoMetadataById from "../util/getMemoizedVideoMetadataById.js";
import logger from "../logger.js";

const streamVideo = async (ctx) => {
  const id = ctx.params.id;
  const { contentType, filename, size } = await getMemoizedVideoMetadataById(
    id
  );

  const range = ctx.headers.range;
  if (range) {
    const [start, end = size - 1] = range
      .replace(/bytes=/, "")
      .split("-")
      .filter((part) => part)
      .map((part) => +part);

    const contentLength = end - start + 1;
    const contentRange = `bytes ${start}-${end}/${size}`;
    logger.debug(`sending partial response for ${id}: ${contentRange}`);

    ctx.set("Content-Range", contentRange);
    ctx.set("Accept-Ranges", "bytes");
    ctx.set("Content-Length", contentLength);
    ctx.set("Content-Type", contentType);

    ctx.status = 206;
    ctx.body = createReadStream(filename, { start, end });
  } else {
    ctx.set("Content-Length", size);
    ctx.set("Content-Type", contentType);

    ctx.status = 200;
    ctx.body = createReadStream(filename);
  }
};

export default streamVideo;
