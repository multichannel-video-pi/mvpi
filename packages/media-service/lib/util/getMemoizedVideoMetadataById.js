import config from "@mvpi/config";
import { Video } from "@mvpi/models";
import { promises as fs } from "fs";
import mem from "mem";
import mime from "mime";
import path from "path";

const getMemoizedVideoMetadataById = mem(
  async (id) => {
    const video = await Video.selectByKey({ id });
    const filename = video.filename.startsWith(path.sep)
      ? video.filename
      : path.join(config.videos.directory, video.filename);
    const contentType = mime.getType(filename);
    const { size } = await fs.stat(filename);
    return { contentType, filename, size };
  },
  {
    maxAge: 1000 * 60 * 60, // 1 hour
  }
);

export default getMemoizedVideoMetadataById;
