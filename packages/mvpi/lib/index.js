import config from "@mvpi/config";
import { koaErrorHandler } from "@mvpi/errors";
import createMediaService from "@mvpi/media-service";
import createMetadataService from "@mvpi/metadata-service";
import createRemoteService from "@mvpi/remote-service";
import http from "http";
import Koa from "koa";
import mount from "koa-mount";

import createUIService from "./createUIService.js";
import logger from "./logger.js";

const createMVPiService = async () => {
  logger.debug("creating MVPi service");
  const app = new Koa();

  // 1. create services
  const [
    mediaService,
    metadataService,
    mediaUI,
    metadataUI,
    remoteUI,
  ] = await Promise.all([
    createMediaService(),
    createMetadataService(),
    createUIService("@mvpi/media-ui"),
    createUIService("@mvpi/metadata-ui"),
    createUIService("@mvpi/remote-ui"),
  ]);

  // 2. mount services
  app.use(mount(config.media.service.prefix, mediaService));
  app.use(mount(config.media.ui.prefix, mediaUI));
  app.use(mount(config.metadata.service.prefix, metadataService));
  app.use(mount(config.metadata.ui.prefix, metadataUI));
  app.use(mount(config.remote.ui.prefix, remoteUI));

  app.use(async (ctx) => {
    if (ctx.path === "/") ctx.redirect(config.media.ui.prefix);
  });

  app.on("error", koaErrorHandler(logger));

  // 3. create aggregated server
  const server = await createRemoteService(http.createServer(app.callback()));

  // 4. start media sync
  // await metadataSync();

  return server;
};

export default createMVPiService;
