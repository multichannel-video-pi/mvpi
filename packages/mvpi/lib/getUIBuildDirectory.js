import { createRequire } from "module";
import path from "path";

const require = createRequire(import.meta.url);

const getUIBuildDirectory = (moduleName) => {
  return path.join(
    require.resolve(`${moduleName}/build/asset-manifest.json`),
    ".."
  );
};

export default getUIBuildDirectory;
