import compress from "@mvpi/compress";
import config from "@mvpi/config";
import { koaErrorMiddleware } from "@mvpi/errors";
import Koa from "koa";
import koaPino from "koa-pino-logger";
import send from "koa-send";
import serve from "koa-static";
import pino from "pino";

import getUIBuildDirectory from "./getUIBuildDirectory.js";

const createUIService = async (moduleName) => {
  const name = moduleName.replace(/^@mvpi\//, "");
  const logger = pino({ ...config.logger, name });
  logger.debug(`creating ${name} service`);

  const buildDirectory = getUIBuildDirectory(moduleName);
  await compress(buildDirectory);

  const app = new Koa();

  app.use(koaPino({ logger }));
  app.use(koaErrorMiddleware(logger));
  app.use(serve(buildDirectory));

  // send index.html when path does not match a static resource
  app.use(async (ctx) => {
    await send(ctx, "index.html", { root: buildDirectory });
  });

  return app;
};

export default createUIService;
