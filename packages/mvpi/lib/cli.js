#!/usr/bin/env node

import config from "@mvpi/config";

import createMVPiService from "./index.js";
import logger from "./logger.js";

(async () => {
  try {
    const server = await createMVPiService();
    server.listen(config.mvpi.service.port, () =>
      logger.info(`MVPi service listening on port ${config.mvpi.service.port}`)
    );
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
