import database from "@mvpi/database";
import { koaErrorHandler, koaErrorMiddleware } from "@mvpi/errors";
import { schema } from "@mvpi/models";
import { setUpdateSchedulesInterval } from "@mvpi/schedules";
import apollo from "apollo-server-koa";
import Koa from "koa";
import compress from "koa-compress";
import koaPino from "koa-pino-logger";
import zlib from "zlib";

import logger from "./logger.js";

const { ApolloServer } = apollo;

const createMetadataService = async (prefix) => {
  logger.debug("creating metadata service");
  await database();
  await setUpdateSchedulesInterval();

  const app = new Koa();
  const server = new ApolloServer({ schema });

  app.use(koaPino({ logger }));
  app.use(koaErrorMiddleware(logger));
  app.use(
    compress({
      br: {
        params: {
          [zlib.constants.BROTLI_PARAM_QUALITY]: 5,
        },
      },
    })
  );

  app.use(
    prefix
      ? server.getMiddleware({ path: `${prefix}/graphql` })
      : server.getMiddleware()
  );

  app.on("error", koaErrorHandler(logger));

  return app;
};

export default createMetadataService;
