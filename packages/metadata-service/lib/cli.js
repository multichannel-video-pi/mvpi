#!/usr/bin/env node

import config from "@mvpi/config";
import createMetadataService from "./index.js";
import logger from "./logger.js";

(async () => {
  try {
    const app = await createMetadataService(config.metadata.service.prefix);
    app.listen(config.metadata.service.port, () => {
      logger.info(
        `metadata service listening on port ${config.metadata.service.port}`
      );
    });
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
