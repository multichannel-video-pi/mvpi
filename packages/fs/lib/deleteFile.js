import { promises as fs } from "fs";
import logger from "./logger.js";

const deleteFile = async (path) => {
  logger.debug(`deleting file ${path}`);
  try {
    await fs.unlink(path);
  } catch (err) {
    if (err.code !== "ENOENT") throw err;
    logger.warn(`unable to delete file ${path} because it does not exist`);
  }
  return path;
};

export default deleteFile;
