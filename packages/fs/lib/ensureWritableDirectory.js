import { constants as fsConstants, promises as fs } from "fs";

import logger from "./logger.js";

const { F_OK, R_OK, W_OK, X_OK } = fsConstants;
const DEFAULT_MODE = 0o700;

const ensureWritableDirectory = async (dirname, mode = DEFAULT_MODE) => {
  // ensure directory exists
  try {
    await fs.access(dirname, F_OK);
  } catch (err) {
    logger.debug(`creating directory ${dirname}`);
    await fs.mkdir(dirname, { mode: mode, recursive: true });
  }

  // ensure directory has correct permissions
  try {
    await fs.access(dirname, F_OK | R_OK | W_OK | X_OK);
  } catch (err) {
    logger.debug("updating directory permissions");
    await fs.chmod(dirname, mode);
  }

  return null;
};

export default ensureWritableDirectory;
