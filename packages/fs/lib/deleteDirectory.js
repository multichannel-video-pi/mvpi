import { promises as fs } from "fs";
import logger from "./logger.js";

const deleteDirectory = async (path) => {
  logger.debug(`deleting directory ${path}`);
  try {
    await fs.rmdir(path);
  } catch (err) {
    if (err.code !== "ENOENT") throw err;
    logger.warn(`unable to delete directory ${path} because it does not exist`);
  }
  return path;
};

export default deleteDirectory;
