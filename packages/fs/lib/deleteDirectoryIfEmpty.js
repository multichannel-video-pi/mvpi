import deleteDirectory from "./deleteDirectory.js";
import logger from "./logger.js";

const deleteDirectoryIfEmpty = async (path) => {
  try {
    await deleteDirectory(path);
  } catch (err) {
    if (err.code !== "ENOTEMPTY") throw err;
    logger.warn(`unable to delete directory ${path} because it is not empty`);
  }
  return path;
};

export default deleteDirectoryIfEmpty;
