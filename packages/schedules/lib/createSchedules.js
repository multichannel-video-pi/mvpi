import config from "@mvpi/config";
import database, { insertInto } from "@mvpi/database";
import { getDenormalizedVideos } from "@mvpi/videos";
import throttlify from "throttlify";

import { SCHEDULES_ORDER, SCHEDULES_TABLE } from "./constants.js";
import generateTagSchedules from "./generateTagSchedules.js";
import { column, selectSchedules } from "./tableUtilities.js";

const createSchedules = async (params, info) => {
  const db = await database();
  const now = Date.now();
  const endTime = now + config.schedules.future;

  const [videos, schedules] = await Promise.all([
    getDenormalizedVideos(),
    selectSchedules({}, SCHEDULES_ORDER),
  ]);

  const tagIds = [
    ...new Set(videos.flatMap((video) => video.tags.map((tag) => tag.id))),
  ];

  await Promise.all(
    tagIds
      .flatMap((tagId) => {
        const tagSchedules = schedules.filter(
          (schedule) => schedule.tagId === tagId
        );
        const tagVideos = videos.filter((video) =>
          video.tags.find((tag) => tag.id === tagId)
        );

        return generateTagSchedules(tagId, endTime, tagVideos, tagSchedules);
      })
      .map(async (schedule) => {
        const insertQuery = insertInto(SCHEDULES_TABLE, schedule);
        return db.run(insertQuery);
      })
  );

  return selectSchedules(
    [[column`createdAt`, ">=", now]],
    SCHEDULES_ORDER,
    info
  );
};

export default throttlify(createSchedules, {
  concurrent: 1,
  duration: 15000,
  max: 1,
});
