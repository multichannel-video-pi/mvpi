import config from "@mvpi/config";

import { SCHEDULES_ORDER } from "./constants.js";
import { column, deleteSchedules } from "./tableUtilities.js";

const deleteExpiredSchedules = async (conditions, order, info) => {
  const expiredStartTime = Date.now() - config.schedules.past;
  return deleteSchedules(
    [[column`startTime`, "<", expiredStartTime]],
    SCHEDULES_ORDER,
    info
  );
};

export default deleteExpiredSchedules;
