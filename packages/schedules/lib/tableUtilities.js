import { createTableUtilities } from "@mvpi/database";
import { SCHEDULES_TABLE } from "./constants.js";

export const {
  delete: deleteSchedules,
  deleteByKey: deleteSchedule,
  insert: insertSchedule,
  select: selectSchedules,
  selectByKey: selectSchedule,
  update: updateSchedules,
  updateByKey: updateSchedule,
  column,
  getConditions,
  getOrder,
  getPrimaryKeyConditions,
} = createTableUtilities(SCHEDULES_TABLE);
