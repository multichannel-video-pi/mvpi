import assert from "assert";

import getVideo from "./getVideo.js";
import getVideoWeight from "./getVideoWeight.js";

const generateTagSchedules = (tagId, endTime, videos, schedules) => {
  assert(tagId, "tagId is required");
  assert(endTime > Date.now(), "endTime must be a future timestamp");
  assert(Array.isArray(videos), "videos must be an array");
  assert(Array.isArray(schedules), "schedules must be an array");

  const now = Date.now();
  const getTagVideo = getVideo(videos);
  const getTagVideoWeight = getVideoWeight(videos);

  while (
    (schedules[schedules.length - 1]?.startTime || now) +
      Math.ceil(
        (getTagVideo(schedules[schedules.length - 1])?.duration || 0) * 1000
      ) <
    endTime
  ) {
    const previous = schedules[schedules.length - 1];
    const previousStartTime = previous?.startTime || now;
    const previousVideo = getTagVideo(previous);
    const previousDuration = Math.ceil((previousVideo?.duration || 0) * 1000);
    const getNextVideoWeight = getTagVideoWeight(previousVideo);

    const weightedVideoIds = videos
      // .filter(video => video.id !== lastScheduled?.videoId)
      .reduce((videoIds, video) => {
        const ratingWeight = getNextVideoWeight(video);
        return [...videoIds, ...[...Array(ratingWeight)].map(() => video.id)];
      }, []);
    const randomVideoIdIndex = Math.floor(
      Math.random() * weightedVideoIds.length
    );

    const next = {
      startTime: previousStartTime + previousDuration,
      videoId: weightedVideoIds[randomVideoIdIndex],
      tagId,
    };

    schedules.push(next);
  }

  return schedules.filter((schedule) => !schedule.id);
};

export default generateTagSchedules;
