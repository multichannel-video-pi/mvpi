import { formatTitle } from "@mvpi/util";

const VIDEO_RATING_WEIGHT = {
  0: 100,
  1: 100,
  2: 100,
  3: 200,
  4: 300,
  5: 300,
};

const NEXT_IN_SERIES_MULTIPLIER = 10;

const getVideoWeight = (videos) => (previousVideo) => (video) => {
  const { televisionEpisode } = video;

  const rating =
    video.rating ||
    (Number.isFinite(video.voteAverage)
      ? Math.round(video.voteAverage / 2)
      : 0);

  // const collection = movie?.collection;
  const televisionShow = televisionEpisode?.televisionShow;

  // if (video._id === previousVideo?.next) {
  //   return VIDEO_RATING_WEIGHT[rating] * NEXT_IN_SERIES_MULTIPLIER;
  // }

  // if (collection) {}

  if (televisionShow) {
    const sortedEpisodeVideoIds = videos
      .filter(
        (v) => v.televisionEpisode?.televisionShow.id === televisionShow.id
      )
      .map((v) => [v.id, formatTitle(v).toLowerCase()])
      .sort(([, titleA], [, titleB]) => {
        if (titleA > titleB) return 1;
        if (titleA < titleB) return -1;
        return 0;
      })
      .map(([id]) => id);

    if (
      previousVideo?.televisionEpisode?.televisionShow.id !== televisionShow.id
    )
      return Math.ceil(
        VIDEO_RATING_WEIGHT[rating] / sortedEpisodeVideoIds.length
      );

    const previousIndex = sortedEpisodeVideoIds.findIndex(
      (id) => id === previousVideo?.id
    );
    const currentIndex = sortedEpisodeVideoIds.findIndex(
      (id) => id === video.id
    );

    if (previousIndex + 1 === currentIndex)
      return VIDEO_RATING_WEIGHT[rating] * NEXT_IN_SERIES_MULTIPLIER;

    return Math.ceil(
      VIDEO_RATING_WEIGHT[rating] / (sortedEpisodeVideoIds.length * 2)
    );
  }

  return VIDEO_RATING_WEIGHT[rating];
};

export default getVideoWeight;
