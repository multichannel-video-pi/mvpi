import config from "@mvpi/config";

import createSchedules from "./createSchedules.js";
import deleteExpiredSchedules from "./deleteExpiredSchedules.js";
import logger from "./logger.js";

const setUpdateSchedulesInterval = async () => {
  const updateInterval = config.schedules.updateInterval;
  await deleteExpiredSchedules();
  await createSchedules();
  logger.debug("updated schedules");

  const interval = setInterval(async () => {
    try {
      await deleteExpiredSchedules();
      await createSchedules();
      logger.debug("updated schedules");
    } catch (err) {
      logger.error("failed to update schedules");
      logger.error(err);
    }
  }, updateInterval);

  logger.debug(`scheduler started with ${updateInterval}ms update interval`);
  return interval;
};

export default setUpdateSchedulesInterval;
