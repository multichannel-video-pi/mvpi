const getVideo = (videos) => (schedule) =>
  videos.find((video) => video.id === schedule?.videoId);

export default getVideo;
