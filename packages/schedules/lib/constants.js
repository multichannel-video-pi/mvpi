export const SCHEDULES_TABLE = "Schedules";

export const SCHEDULES_ORDER = {
  "Schedules.tagId": "ASC",
  "Schedules.startTime": "ASC",
};
