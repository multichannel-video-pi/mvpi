import { promises as fs } from "fs";

import compressFile from "./compressFile.js";
import getFilesInDirectory from "./getFilesInDirectory.js";

const DEFAULT_THRESHOLD = 10240;

const compress = async (path, threshold = DEFAULT_THRESHOLD) => {
  const stats = await fs.stat(path);

  if (stats.isFile()) return compressFile(path);
  if (!stats.isDirectory()) throw new Error("path must be a file or directory");
  if (typeof threshold !== "number")
    throw new Error("threshold must be a number");

  const files = await getFilesInDirectory(path, threshold);
  return Promise.all(
    files
      .filter((file) => /\.(css|html|js|json|map)$/.test(file))
      .map(compressFile)
  );
};

export default compress;
