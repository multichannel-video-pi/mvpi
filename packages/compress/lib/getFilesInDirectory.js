import { promises as fs } from "fs";
import path from "path";

const getFilesInDirectory = async (dir, threshold) => {
  const items = await fs.readdir(dir);
  const itemsWithStats = await Promise.all(
    items.map(async (item) => {
      const key = path.join(dir, item);
      return [key, await fs.stat(key)];
    })
  );
  const files = await Promise.all(
    itemsWithStats
      .filter(([, stats]) => {
        if (stats.isDirectory()) return true;
        if (stats.isFile() && stats.size >= threshold) return true;
        return false;
      })
      .map(async ([item, stats]) => {
        if (stats.isDirectory()) return getFilesInDirectory(item, threshold);
        return item;
      })
  );
  return files.flatMap((file) => file);
};

export default getFilesInDirectory;
