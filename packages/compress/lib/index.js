export { default } from "./compress.js";
export { default as compressFile } from "./compressFile.js";
export { default as getFilesInDirectory } from "./getFilesInDirectory.js";
