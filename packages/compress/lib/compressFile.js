import { formatSize } from "@mvpi/util";
import fs from "fs";
import stream from "stream";
import { promisify } from "util";
import zlib from "zlib";

import logger from "./logger.js";

const { stat } = fs.promises;
const pipeline = promisify(stream.pipeline);

const compressFile = async (file) => {
  const start = Date.now();
  const brotliFile = `${file}.br`;
  const gzipFile = `${file}.gz`;

  const { size } = await stat(file);

  const read = fs.createReadStream(file);

  const brotli = zlib.createBrotliCompress({
    params: {
      [zlib.constants.BROTLI_PARAM_MODE]: zlib.constants.BROTLI_MODE_TEXT,
      [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
      [zlib.constants.BROTLI_PARAM_SIZE_HINT]: size,
    },
  });
  const gzip = zlib.createGzip();

  const brotliWrite = fs.createWriteStream(brotliFile);
  const gzipWrite = fs.createWriteStream(gzipFile);

  await Promise.all([
    pipeline(read, brotli, brotliWrite),
    pipeline(read, gzip, gzipWrite),
  ]);

  const [{ size: brotliSize }, { size: gzipSize }] = await Promise.all([
    stat(brotliFile),
    stat(gzipFile),
  ]);

  const formattedSize = formatSize(size);
  const formattedBrotliSize = formatSize(brotliSize);
  const formattedGzipSize = formatSize(gzipSize);

  const duration = Date.now() - start;
  logger.debug(
    `compressed ${file} in ${duration}ms -- uncompressed: ${formattedSize}, brotli: ${formattedBrotliSize}, gzip: ${formattedGzipSize}`
  );

  return { brotli: brotliFile, gzip: gzipFile, uncompressed: file };
};

export default compressFile;
