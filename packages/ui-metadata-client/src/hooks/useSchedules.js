import { useContext } from "react";

import SchedulesContext from "../contexts/SchedulesContext.js";

const useSchedules = () => useContext(SchedulesContext);

export default useSchedules;
