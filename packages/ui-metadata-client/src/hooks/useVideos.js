import { useContext } from "react";

import VideosContext from "../contexts/VideosContext.js";

const useVideos = () => useContext(VideosContext);

export default useVideos;
