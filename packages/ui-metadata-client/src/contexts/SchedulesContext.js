import { createContext } from "react";

const SchedulesContext = createContext();

export default SchedulesContext;
