import { useQuery, gql } from "@apollo/client";
import React from "react";

import VideosContext from "../contexts/VideosContext.js";

const FETCH_VIDEOS = gql`
  query fetchVideos {
    videos {
      id
      bitrate
      duration
      filename
      isActive
      overview
      rating
      releaseDate
      size
      title
      voteAverage
      voteCount

      movie {
        id
        backdropPath
        chronology
        posterPath
        tagline
        tmdbId

        collection {
          id
          backdropPath
          name
          overview
          posterPath
          tmdbId
        }
      }

      tags {
        id
        name
        overview
        tmdbId
      }

      televisionEpisode {
        id
        episodeNumber
        seasonNumber
        stillPath

        televisionShow {
          id
          backdropPath
          firstAirDate
          lastAirDate
          name
          overview
          posterPath
          rating
          tagline
          tmdbId
          voteAverage
          voteCount
        }
      }
    }
  }
`;

const VideosContextProvider = ({ children }) => {
  const { data, error, loading, refetch } = useQuery(FETCH_VIDEOS, {
    pollInterval: 300000,
  });

  const videos = data?.videos;
  return (
    <VideosContext.Provider value={{ error, loading, videos, refetch }}>
      {children}
    </VideosContext.Provider>
  );
};

export default VideosContextProvider;
