import React from "react";

import MetadataApolloProvider from "./MetadataApolloProvider.js";
import VideosContextProvider from "./VideosContextProvider.js";

const SchedulesProvider = ({ children, config }) => {
  return (
    <MetadataApolloProvider config={config}>
      <VideosContextProvider>{children}</VideosContextProvider>
    </MetadataApolloProvider>
  );
};

export default SchedulesProvider;
