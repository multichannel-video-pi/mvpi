import { ApolloProvider } from "@apollo/client";
import React, { useEffect, useMemo } from "react";

import createMetadataClient from "../clients/createMetadataClient.js";

const MetadataApolloProvider = ({ children, config }) => {
  const client = useMemo(() => createMetadataClient(config), [config]);

  useEffect(
    () => () => {
      client.stop();
    },
    [client]
  );

  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};

export default MetadataApolloProvider;
