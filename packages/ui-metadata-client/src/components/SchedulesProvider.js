import React from "react";

import MetadataApolloProvider from "./MetadataApolloProvider.js";
import SchedulesContextProvider from "./SchedulesContextProvider.js";

const SchedulesProvider = ({ children, config }) => {
  return (
    <MetadataApolloProvider config={config}>
      <SchedulesContextProvider>{children}</SchedulesContextProvider>
    </MetadataApolloProvider>
  );
};

export default SchedulesProvider;
