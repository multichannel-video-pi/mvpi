import { useQuery, gql } from "@apollo/client";
import React, { useMemo } from "react";

import SchedulesContext from "../contexts/SchedulesContext.js";

const FETCH_SCHEDULES = gql`
  query fetchSchedules {
    schedules(orderBy: { tagId: ASC, startTime: ASC }) {
      id
      startTime

      tag {
        id
        name
        overview
      }

      video {
        id
        bitrate
        duration
        filename
        isActive
        overview
        rating
        releaseDate
        size
        title
        voteAverage
        voteCount

        movie {
          id
          backdropPath
          chronology
          posterPath
          tagline
          tmdbId

          collection {
            id
            backdropPath
            name
            overview
            posterPath
            tmdbId
          }
        }

        tags {
          id
          name
          overview
          tmdbId
        }

        televisionEpisode {
          id
          episodeNumber
          seasonNumber
          stillPath

          televisionShow {
            id
            backdropPath
            firstAirDate
            lastAirDate
            name
            overview
            posterPath
            rating
            tagline
            tmdbId
            voteAverage
            voteCount
          }
        }
      }
    }
  }
`;

const SchedulesContextProvider = ({ children }) => {
  const { data, error, loading, refetch } = useQuery(FETCH_SCHEDULES, {
    pollInterval: 300000,
  });

  const schedules = data?.schedules;
  const channels = useMemo(
    () =>
      [...new Set(schedules?.map((schedule) => schedule.tag))].sort(
        (tagA, tagB) => {
          const nameA = tagA.name.toLowerCase();
          const nameB = tagB.name.toLowerCase();
          if (nameA > nameB) return 1;
          if (nameA < nameB) return -1;
          return 0;
        }
      ),
    [schedules]
  );

  return (
    <SchedulesContext.Provider
      value={{ channels, error, loading, refetch, schedules }}
    >
      {children}
    </SchedulesContext.Provider>
  );
};

export default SchedulesContextProvider;
