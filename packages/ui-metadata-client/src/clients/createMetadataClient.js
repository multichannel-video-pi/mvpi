import { ApolloClient, InMemoryCache } from "@apollo/client";
import { url } from "@mvpi/util";

const createMetadataClient = (config) => {
  return new ApolloClient({
    uri: url`${config.metadata.service}/graphql`,
    cache: new InMemoryCache({
      typePolicies: {
        Video: {
          fields: {
            tags: {
              merge: false,
            },
          },
        },
      },
    }),
  });
};

export default createMetadataClient;
