import { unflatten } from "@mvpi/util";
import { PARENT_ID } from "./constants.js";

const mergeAndUnflatten = (record, nested, keys) => {
  const merged = !keys
    ? record
    : {
        ...record,
        ...Object.fromEntries(
          keys.map((key) => [
            key,
            nested[key].filter((item) => item[PARENT_ID] === record.id),
          ])
        ),
      };
  return unflatten(merged, { filterEmptyValues: true });
};

export default mergeAndUnflatten;
