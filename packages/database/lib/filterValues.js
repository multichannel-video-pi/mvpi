import { READ_ONLY_COLUMNS } from "./constants.js";

const filterValues = (values) => {
  return Object.fromEntries(
    Object.entries(values)
      .filter(([column]) => !READ_ONLY_COLUMNS.includes(column))
      .filter(([, value]) => typeof value !== "undefined")
  );
};

export default filterValues;
