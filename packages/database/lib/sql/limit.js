import SQL from "sql-template-strings";

const limitSQL = (limit, offset) => {
  if (!limit) return "";
  return offset ? SQL` LIMIT ${limit} OFFSET ${offset}` : SQL` LIMIT ${limit}`;
};

export default limitSQL;
