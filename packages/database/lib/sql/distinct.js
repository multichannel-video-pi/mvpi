const distinctSQL = (distinct) => {
  return distinct ? "DISTINCT " : "";
};

export default distinctSQL;
