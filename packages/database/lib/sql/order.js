import { ValidationError } from "@mvpi/errors";
import { SORT_KEYWORDS } from "../constants.js";

const orderSQL = (order) => {
  const orderEntries = Object.entries(order);
  if (!orderEntries.length) return "";
  const clause = orderEntries
    .map(([column, sort]) => {
      if (!SORT_KEYWORDS.includes(sort))
        throw new ValidationError(
          `unknown or unsupported sort keyword: ${sort}`
        );
      return `${column} ${sort}`;
    })
    .join(", ");
  return ` ORDER BY ${clause}`;
};

export default orderSQL;
