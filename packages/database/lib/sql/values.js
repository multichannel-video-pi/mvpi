import SQL from "sql-template-strings";

const valuesSQL = (values) => {
  const valueEntries = Object.entries(values);
  const columns = valueEntries.map(([column]) => column);
  return valueEntries.reduce((accumulated, [, value], index, source) => {
    if (index + 1 === source.length) {
      return accumulated.append(SQL`${value})`);
    }
    return accumulated.append(SQL`${value}, `);
  }, SQL` (`.append(columns.join(", ")).append(SQL`) VALUES (`));
};

export default valuesSQL;
