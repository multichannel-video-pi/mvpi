import { ValidationError } from "@mvpi/errors";
import SQL from "sql-template-strings";

const conditionsSQL = (conditions) => {
  if (!conditions.length) return "";

  return conditions.reduce((sql, { column, compare, logic, value }, index) => {
    const columnSQL =
      index === 0
        ? sql.append(column).append(SQL` `)
        : sql
            .append(SQL` `)
            .append(logic)
            .append(SQL` `)
            .append(column)
            .append(SQL` `);

    switch (compare) {
      case "=":
      case "!=":
      case "<>":
      case "<":
      case ">":
      case "<=":
      case ">=":
      case "LIKE":
      case "NOT LIKE":
        return columnSQL.append(compare).append(SQL` ${value}`);
      case "BETWEEN":
      case "NOT BETWEEN": {
        const [lowValue, highValue] = value;
        return columnSQL
          .append(compare)
          .append(SQL` ${lowValue} AND ${highValue}`);
      }
      case "IN":
      case "NOT IN":
        return value.reduce((accumulated, v, i, source) => {
          if (i + 1 === source.length) {
            return accumulated.append(SQL`${v})`);
          }
          return accumulated.append(SQL`${v}, `);
        }, columnSQL.append(compare).append(SQL` (`));
      default:
        throw new ValidationError(`unknown comparison operator: ${compare}`);
    }
  }, SQL` WHERE `);
};

export default conditionsSQL;
