const fieldsSQL = (fields) => {
  const fieldEntries = Object.entries(fields);
  if (!fieldEntries.length) return "* FROM ";
  return fieldEntries
    .map(([alias, field]) => {
      if (alias === field || !field) return alias;
      return `${field} AS ${alias}`;
    })
    .join(", ")
    .concat(" FROM ");
};

export default fieldsSQL;
