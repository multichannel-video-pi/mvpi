import { ValidationError } from "@mvpi/errors";
import SQL from "sql-template-strings";

import filterValues from "../filterValues.js";

import conditionsSQL from "./conditions.js";
import setSQL from "./set.js";

const upsertSQL = (onConflict, queryValues) => {
  const { column, conditions, operation, values } = onConflict;
  if (!operation) return "";
  const constraint = Array.isArray(column) ? column.join(", ") : column;
  const sql = constraint
    ? SQL` ON CONFLICT (`.append(constraint).append(") DO ")
    : SQL` ON CONFLICT DO `;

  switch (operation) {
    case "NOTHING":
      return sql.append("NOTHING");
    case "UPDATE": {
      const updateConditions = conditions || {};
      const updateValues = filterValues(values || queryValues);
      return sql
        .append("UPDATE")
        .append(setSQL(updateValues))
        .append(conditionsSQL(updateConditions));
    }
    default:
      throw new ValidationError(
        `unknown or unsupported conflict operation: ${operation}`
      );
  }
};

export default upsertSQL;
