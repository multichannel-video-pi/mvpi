import { ValidationError } from "@mvpi/errors";
import { INNER_JOIN, LEFT_JOIN } from "../constants.js";

const joinsSQL = (joins) => {
  return joins
    .map((join) => {
      switch (join.type) {
        case INNER_JOIN:
          return ` INNER JOIN ${join.table} ON ${join.condition}`;
        case LEFT_JOIN:
          return ` LEFT JOIN ${join.table} ON ${join.condition}`;
        default:
          throw new ValidationError(
            `unknown or unsupported join type: ${join.type}`
          );
      }
    })
    .join("");
};

export default joinsSQL;
