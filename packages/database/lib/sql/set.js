import SQL from "sql-template-strings";

const setSQL = (values) => {
  return Object.entries(values).reduce(
    (accumulated, [column, value], index, source) => {
      if (index + 1 === source.length) {
        return accumulated.append(column).append(SQL` = ${value}`);
      }
      return accumulated.append(column).append(SQL` = ${value}, `);
    },
    SQL` SET `
  );
};

export default setSQL;
