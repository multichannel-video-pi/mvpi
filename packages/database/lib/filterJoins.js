const filterJoins = (joins, table) => {
  return joins.filter(([joinTable], index) => {
    if (!joinTable) return false;
    if (joinTable === table) return false;
    if (joins.findIndex(([jT]) => jT === joinTable) !== index) return false;
    return true;
  });
};

export default filterJoins;
