// parent identifier
export const PARENT_ID = "PARENT_ID";

// join directives
export const MANY_TO_MANY = "manyToMany";
export const ONE_TO_MANY = "oneToMany";
export const ONE_TO_ONE = "oneToOne";
export const JOIN_DIRECTIVES = [MANY_TO_MANY, ONE_TO_MANY, ONE_TO_ONE];

// query actions
export const DELETE = Symbol("DELETE");
export const INSERT = Symbol("INSERT");
export const SELECT = Symbol("SELECT");
export const UPDATE = Symbol("UPDATE");

// query joins
export const INNER_JOIN = Symbol("INNER_JOIN");
export const LEFT_JOIN = Symbol("LEFT_JOIN");

// query implementation details
export const READ_ONLY_COLUMNS = ["id", "createdAt", "updatedAt"];
export const SORT_KEYWORDS = ["ASC", "DESC"];
