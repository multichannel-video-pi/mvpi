import database from "./database.js";
import filterJoins from "./filterJoins.js";
import mergeAndUnflatten from "./mergeAndUnflatten.js";
import { selectFrom } from "./Query.js";

import {
  JOIN_DIRECTIVES,
  PARENT_ID,
  MANY_TO_MANY,
  ONE_TO_MANY,
  ONE_TO_ONE,
} from "./constants.js";

import getFieldsAndLeftJoins from "./getFieldsAndLeftJoins.js";

const getNestedQueryRecords = async (
  schema,
  selections,
  selectionsFieldType,
  selectionsTable,
  parentId,
  parentConditions,
  parentJoins = [],
  fieldPrefix = ""
) => {
  const schemaFields = schema.getType(selectionsFieldType).getFields();
  return (
    await Promise.all(
      selections
        .map((selection) => {
          const name = selection.name.value;
          if (!schemaFields[name]) return { name, selection };

          const schemaField = schemaFields[name].astNode;
          const joinDirective = schemaField.directives.find((directive) =>
            JOIN_DIRECTIVES.includes(directive.name.value)
          );
          return { name, schemaField, joinDirective, selection };
        })
        .filter(({ joinDirective }) => joinDirective)
        .map(async ({ name, schemaField, joinDirective, selection }) => {
          const joinSelections = selection.selectionSet.selections;
          const joinSelectionsFieldType =
            schemaField.type.type?.name.value || schemaField.type.name.value;
          const joinDirectiveArgs = Object.fromEntries(
            joinDirective.arguments.map((arg) => [
              arg.name.value,
              arg.value.value,
            ])
          );

          switch (joinDirective.name.value) {
            case MANY_TO_MANY: {
              const db = await database();
              const joinParentId = `${joinDirectiveArgs.table}.id`;
              const { fields, joins } = getFieldsAndLeftJoins(
                schema,
                joinSelections,
                joinSelectionsFieldType,
                joinDirectiveArgs.table,
                joinParentId
              );
              const nested = await getNestedQueryRecords(
                schema,
                joinSelections,
                joinSelectionsFieldType,
                joinDirectiveArgs.table,
                joinParentId,
                parentConditions,
                [
                  [joinDirectiveArgs.linkTable, joinDirectiveArgs.condition],
                  [selectionsTable, joinDirectiveArgs.linkCondition],
                  ...parentJoins,
                ]
              );
              const nestedKeys = Object.keys(nested);
              const combinedJoins = filterJoins(
                [
                  [selectionsTable, joinDirectiveArgs.linkCondition],
                  [joinDirectiveArgs.table, joinDirectiveArgs.condition],
                  ...parentJoins,
                  ...joins,
                ],
                joinDirectiveArgs.linkTable
              );
              const query = selectFrom(joinDirectiveArgs.linkTable)
                .fields(fields)
                .fields({ [PARENT_ID]: parentId })
                .leftJoins(combinedJoins)
                .where(parentConditions);
              const records = (await db.all(query)).map((record) =>
                mergeAndUnflatten(record, nested, nestedKeys)
              );
              return { [`${fieldPrefix}${name}`]: records };
            }

            case ONE_TO_MANY: {
              const db = await database();
              const joinParentId = `${joinDirectiveArgs.table}.id`;
              const { fields, joins } = getFieldsAndLeftJoins(
                schema,
                joinSelections,
                joinSelectionsFieldType,
                joinDirectiveArgs.table,
                joinParentId
              );
              const nested = await getNestedQueryRecords(
                schema,
                joinSelections,
                joinSelectionsFieldType,
                joinDirectiveArgs.table,
                joinParentId,
                parentConditions,
                [[selectionsTable, joinDirectiveArgs.condition], ...parentJoins]
              );
              const nestedKeys = Object.keys(nested);
              const combinedJoins = filterJoins(
                [
                  [selectionsTable, joinDirectiveArgs.condition],
                  ...parentJoins,
                  ...joins,
                ],
                joinDirectiveArgs.table
              );
              const query = selectFrom(joinDirectiveArgs.table)
                .fields(fields)
                .fields({ [PARENT_ID]: parentId })
                .leftJoins(combinedJoins)
                .where(parentConditions);
              const records = (await db.all(query)).map((record) =>
                mergeAndUnflatten(record, nested, nestedKeys)
              );
              return { [`${fieldPrefix}${name}`]: records };
            }

            case ONE_TO_ONE:
              return getNestedQueryRecords(
                schema,
                joinSelections,
                joinSelectionsFieldType,
                joinDirectiveArgs.table,
                parentId,
                parentConditions,
                [
                  [selectionsTable, joinDirectiveArgs.condition],
                  ...parentJoins,
                ],
                `${fieldPrefix}${name}__`
              );
            default:
              return null;
          }
        })
    )
  ).reduce((accumulated, nested) => ({ ...accumulated, ...nested }), {});
};

export default getNestedQueryRecords;
