import config from "@mvpi/config";
import { ensureWritableDirectory } from "@mvpi/fs";
import mem from "mem";
import path from "path";
import sqlite3 from "sqlite3";
import { open } from "sqlite";
import { fileURLToPath } from "url";

import logger from "./logger.js";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const migrationsPath = path.join(__dirname, "..", "migrations");

const database = mem(async () => {
  logger.debug(`validating writable database directory`);
  await ensureWritableDirectory(path.dirname(config.sqlite.filename));

  logger.debug(`connecting to sqlite database at ${config.sqlite.filename}`);
  const db = await open({
    filename: config.sqlite.filename,
    driver: sqlite3.Database,
  });
  logger.debug("connected to sqlite database");

  logger.debug("enabling foreign key constraints");
  await db.exec("PRAGMA foreign_keys = ON");

  logger.debug("executing sqlite database migrations");
  await db.migrate({ migrationsPath });
  logger.debug("sqlite database migrations complete");

  return db;
});

export default database;
