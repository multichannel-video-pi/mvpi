import Table from "./Table.js";

const createTableUtilities = (table, primaryKey) => {
  const Util = class extends Table {
    static primaryKey = primaryKey || "id";
    static table = table;
  };

  return {
    // Query Methods
    delete: Util.delete.bind(Util),
    deleteByKey: Util.deleteByKey.bind(Util),
    insert: Util.insert.bind(Util),
    select: Util.select.bind(Util),
    selectByKey: Util.selectByKey.bind(Util),
    update: Util.update.bind(Util),
    updateByKey: Util.updateByKey.bind(Util),

    // Column Methods
    column: Util.column.bind(Util),
    getConditions: Util.getConditions.bind(Util),
    getOrder: Util.getOrder.bind(Util),
    getPrimaryKeyConditions: Util.getPrimaryKeyConditions.bind(Util),

    // Table Metadata
    primaryKey: Util.primaryKey,
    table: Util.table,
  };
};

export default createTableUtilities;
