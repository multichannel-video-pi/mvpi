import filterJoins from "./filterJoins.js";

import {
  JOIN_DIRECTIVES,
  MANY_TO_MANY,
  ONE_TO_MANY,
  ONE_TO_ONE,
} from "./constants.js";

const getFieldsAndLeftJoins = (
  schema,
  selections,
  selectionsFieldType,
  selectionsTable,
  parentId,
  fieldPrefix = ""
) => {
  const schemaFields = schema.getType(selectionsFieldType).getFields();
  return selections.reduce(
    (accumulated, selection) => {
      const selectionName = selection.name.value;
      if (!schemaFields[selectionName]) return accumulated;

      const schemaField = schemaFields[selectionName].astNode;
      const joinDirective = schemaField.directives.find((directive) =>
        JOIN_DIRECTIVES.includes(directive.name.value)
      );
      const joinName = joinDirective?.name.value;
      switch (joinName) {
        case MANY_TO_MANY:
        case ONE_TO_MANY:
          return {
            fields: {
              ...accumulated.fields,
              id: parentId,
            },
            joins: accumulated.joins,
          };

        case ONE_TO_ONE: {
          const joinSelections = selection.selectionSet.selections;
          const joinSelectionsFieldType = schemaField.type.name.value;
          const joinDirectiveArgs = Object.fromEntries(
            joinDirective.arguments.map((arg) => [
              arg.name.value,
              arg.value.value,
            ])
          );
          const { fields, joins } = getFieldsAndLeftJoins(
            schema,
            joinSelections,
            joinSelectionsFieldType,
            joinDirectiveArgs.table,
            parentId,
            `${fieldPrefix}${selectionName}__`
          );
          return {
            fields: {
              ...accumulated.fields,
              ...fields,
            },
            joins: filterJoins(
              [
                ...accumulated.joins,
                [joinDirectiveArgs.table, joinDirectiveArgs.condition],
                ...joins,
              ],
              selectionsTable
            ),
          };
        }

        default:
          return {
            fields: {
              ...accumulated.fields,
              [`${fieldPrefix}${selectionName}`]: `${selectionsTable}.${selectionName}`,
            },
            joins: accumulated.joins,
          };
      }
    },
    { fields: {}, joins: [] }
  );
};

export default getFieldsAndLeftJoins;
