import { ValidationError } from "@mvpi/errors";
import SQL from "sql-template-strings";

import filterValues from "./filterValues.js";
import logger from "./logger.js";

import conditionsSQL from "./sql/conditions.js";
import distinctSQL from "./sql/distinct.js";
import fieldsSQL from "./sql/fields.js";
import joinsSQL from "./sql/joins.js";
import limitSQL from "./sql/limit.js";
import orderSQL from "./sql/order.js";
import setSQL from "./sql/set.js";
import upsertSQL from "./sql/upsert.js";
import valuesSQL from "./sql/values.js";

import {
  // query actions
  DELETE,
  INSERT,
  SELECT,
  UPDATE,

  // query joins
  INNER_JOIN,
  LEFT_JOIN,
} from "./constants.js";

class Query {
  #action = null;
  #conditions = [];
  #distinct = false;
  #fields = {};
  #joins = [];
  #limit = null;
  #offset = null;
  #onConflict = {};
  #order = {};
  #table = null;
  #values = {};

  constructor(table, options) {
    this.#action = options?.action;
    this.#distinct = options?.distinct === true;
    this.#fields = options?.fields || {};
    this.#table = table;
    this.#values = filterValues(options?.values || {});
  }

  fields(fields) {
    this.#fields = { ...this.#fields, ...fields };
    return this;
  }

  innerJoin(table, condition, alias) {
    return this.join(INNER_JOIN, table, condition, alias);
  }

  join(type, table, condition, alias) {
    this.#joins = [...this.#joins, { alias, condition, table, type }];
    return this;
  }

  leftJoin(table, condition, alias) {
    return this.join(LEFT_JOIN, table, condition, alias);
  }

  leftJoins(joins) {
    if (!joins) return this;
    return joins.reduce((query, joinArgs) => {
      query.join(LEFT_JOIN, ...joinArgs);
      return query;
    }, this);
  }

  limit(limit, offset) {
    if (offset) this.#offset = offset;
    this.#limit = limit;
    return this;
  }

  offset(offset) {
    this.#offset = offset;
    return this;
  }

  onConflict(column, operation, values, conditions) {
    this.#onConflict = { column, conditions, operation, values };
    return this;
  }

  orderBy(order) {
    this.#order = { ...this.#order, ...order };
    return this;
  }

  orWhere(...clause) {
    if (!clause[0]) return this;
    const CLAUSE_GROUP = Symbol(JSON.stringify(clause));

    if (Array.isArray(clause[0]))
      return clause[0].reduce((query, [column, compare, value], index) => {
        if (index === 0)
          return query.where(column, compare, value, "OR", CLAUSE_GROUP);
        return query.where(column, compare, value, "AND", CLAUSE_GROUP);
      }, this);

    if (typeof clause[0] === "object")
      return Object.entries(clause[0]).reduce(
        (query, [column, value], index) => {
          if (index === 0)
            return query.where(column, "=", value, "OR", CLAUSE_GROUP);
          return query.where(column, "=", value, "AND", CLAUSE_GROUP);
        },
        this
      );

    const [column, compare, value, group] = clause;
    return this.where(column, compare, value, "OR", group);
  }

  where(...clause) {
    if (!clause[0]) return this;
    const CLAUSE_GROUP = Symbol(JSON.stringify(clause));

    if (Array.isArray(clause[0]))
      return clause[0].reduce((query, [column, compare, value]) => {
        return query.where(column, compare, value, "AND", CLAUSE_GROUP);
      }, this);

    if (typeof clause[0] === "object")
      return Object.entries(clause[0]).reduce(
        (query, [column, value]) =>
          query.where(column, "=", value, "AND", CLAUSE_GROUP),
        this
      );

    const [column, compare, value, logic = "AND", group] = clause;

    this.#conditions = [
      ...this.#conditions,
      { column, compare, group, logic, value },
    ];

    return this;
  }

  whereBetween(column, lowValue, highValue, logic, group) {
    return this.where(column, "BETWEEN", [lowValue, highValue], logic, group);
  }

  whereIn(column, values, logic, group) {
    return this.where(column, "IN", values, logic, group);
  }

  get statement() {
    switch (this.#action) {
      case DELETE:
        return SQL`DELETE FROM `
          .append(this.#table)
          .append(conditionsSQL(this.#conditions));
      case INSERT:
        return SQL`INSERT INTO `
          .append(this.#table)
          .append(valuesSQL(this.#values))
          .append(upsertSQL(this.#onConflict, this.#values));
      case SELECT:
        return SQL`SELECT `
          .append(distinctSQL(this.#distinct))
          .append(fieldsSQL(this.#fields))
          .append(this.#table)
          .append(joinsSQL(this.#joins))
          .append(conditionsSQL(this.#conditions))
          .append(orderSQL(this.#order))
          .append(limitSQL(this.#limit, this.#offset));
      case UPDATE:
        return SQL`UPDATE `
          .append(this.#table)
          .append(setSQL(this.#values))
          .append(conditionsSQL(this.#conditions));
      default:
        throw new ValidationError(
          `unknown or undefined query action: ${this.#action}`
        );
    }
  }

  get sql() {
    const sql = this.statement.sql;
    logger.debug(sql);
    return sql;
  }

  get values() {
    return this.statement.values;
  }

  static deleteFrom(table, options) {
    return new Query(table, { ...options, action: DELETE });
  }

  static insertInto(table, values, options) {
    return new Query(table, { ...options, action: INSERT, values });
  }

  static selectDistinctFrom(table, options) {
    return new Query(table, { ...options, action: SELECT, distinct: true });
  }

  static selectFrom(table, options) {
    return new Query(table, { ...options, action: SELECT });
  }

  static update(table, values, options) {
    return new Query(table, { ...options, action: UPDATE, values });
  }
}

export const deleteFrom = Query.deleteFrom;
export const insertInto = Query.insertInto;
export const selectDistinctFrom = Query.selectDistinctFrom;
export const selectFrom = Query.selectFrom;
export const update = Query.update;
export default Query;
