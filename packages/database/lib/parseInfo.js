import getFieldsAndLeftJoins from "./getFieldsAndLeftJoins.js";
import getNestedQueryRecords from "./getNestedQueryRecords.js";

const parseInfo = async (info, conditions, table) => {
  if (!info) return {};
  const id = `${table}.id`;
  const schema = info.schema;
  const fieldName = info.fieldName;
  const fieldNodes = info.fieldNodes;
  const query = fieldNodes.find((field) => field.name.value === fieldName);
  const selections = query.selectionSet.selections;
  const selectionsFieldType =
    info.returnType.name || info.returnType.ofType.name;

  const { fields, joins } = getFieldsAndLeftJoins(
    schema,
    selections,
    selectionsFieldType,
    table,
    id
  );

  const nested = await getNestedQueryRecords(
    schema,
    selections,
    selectionsFieldType,
    table,
    id,
    conditions
  );
  const nestedKeys = Object.keys(nested);

  return { fields, joins, nested, nestedKeys };
};

export default parseInfo;
