import { NotFoundError } from "@mvpi/errors";
import { lowerCaseFirst } from "@mvpi/util";

import database from "./database.js";
import mergeAndUnflatten from "./mergeAndUnflatten.js";
import parseInfo from "./parseInfo.js";
import { deleteFrom, insertInto, selectFrom, update } from "./Query.js";

const UNCONDITIONAL_PARAMS = ["orderBy"];

class Table {
  static primaryKey = "id";
  static table = null;

  // Query Methods
  static async delete(conditions, order, info) {
    const db = await database();
    const records = await this.select(conditions, order, info);
    const query = deleteFrom(this.table).where(conditions);
    await db.run(query);
    return records;
  }

  static async deleteByKey(conditions, info) {
    const db = await database();
    const record = await this.selectByKey(conditions, info);
    const query = deleteFrom(this.table).where(conditions);
    await db.run(query);
    return record;
  }

  static async insert(values, info) {
    const db = await database();
    const query = insertInto(this.table, values);
    const { lastID } = await db.run(query);
    const conditions = this.getPrimaryKeyConditions({ ...values, id: lastID });
    return this.selectByKey(conditions, info);
  }

  static async select(conditions, order, info) {
    const db = await database();
    const { fields, joins, nested, nestedKeys } = await parseInfo(
      info,
      conditions,
      this.table
    );
    const query = selectFrom(this.table)
      .fields(fields)
      .leftJoins(joins)
      .where(conditions)
      .orderBy(order);
    const records = await db.all(query);
    return records.map((record) =>
      mergeAndUnflatten(record, nested, nestedKeys)
    );
  }

  static async selectByKey(conditions, info) {
    const db = await database();
    const { fields, joins, nested, nestedKeys } = await parseInfo(
      info,
      conditions,
      this.table
    );
    const query = selectFrom(this.table)
      .fields(fields)
      .leftJoins(joins)
      .where(conditions);
    const record = await db.get(query);
    if (!record) throw new NotFoundError(`record not found in ${this.table}`);
    return mergeAndUnflatten(record, nested, nestedKeys);
  }

  static async update(values, conditions, order, info) {
    const db = await database();
    const query = update(this.table, values).where(conditions);
    await db.run(query);
    return this.select(conditions, order, info);
  }

  static async updateByKey(values, info) {
    const db = await database();
    const conditions = this.getPrimaryKeyConditions(values);
    const query = update(this.table, values).where(conditions);
    await db.run(query);
    return this.selectByKey(conditions, info);
  }

  // Column Methods
  static column(strings, ...keys) {
    const field = strings.map((str, i) => `${str}${keys[i] || ""}`).join("");
    return `${this.table}.${field}`;
  }

  static getConditions(params) {
    return Object.fromEntries(
      Object.entries(params)
        .filter(([key]) => !UNCONDITIONAL_PARAMS.includes(key))
        .map(([column, value]) => [this.column`${column}`, value])
    );
  }

  static getOrder(params) {
    if (!params.orderBy) return {};
    return Object.fromEntries(
      Object.entries(params.orderBy).map(([column, value]) => [
        this.column`${column}`,
        value,
      ])
    );
  }

  static getPrimaryKeyConditions(values) {
    const keyArray = Array.isArray(this.primaryKey)
      ? this.primaryKey
      : [this.primaryKey];
    return Object.fromEntries(
      keyArray.map((column) => {
        const value = values[column];
        if (!value) throw new Error(`primary key "${column}" not found`);
        return [this.column`${column}`, value];
      })
    );
  }

  // GraphQL Resolvers
  static get resolvers() {
    const one = this.one || this.name;
    const many = this.many || this.table;
    return {
      Mutation: {
        [`create${one}`]: async (parent, params, ctx, info) =>
          this.insert(params, info),
        [`delete${one}`]: async (parent, params, ctx, info) =>
          this.deleteByKey(this.getConditions(params), info),
        [`update${one}`]: async (parent, params, ctx, info) =>
          this.updateByKey(params, info),
      },
      Query: {
        [lowerCaseFirst(one)]: async (parent, params, ctx, info) =>
          this.selectByKey(this.getConditions(params), info),
        [lowerCaseFirst(many)]: async (parent, params, ctx, info) =>
          this.select(this.getConditions(params), this.getOrder(params), info),
      },
    };
  }
}

export default Table;
