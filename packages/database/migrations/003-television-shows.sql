--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE TelevisionShows (
  id INTEGER PRIMARY KEY,
  backdropPath TEXT,
  firstAirDate TEXT,
  lastAirDate TEXT,
  name TEXT NOT NULL,
  overview TEXT,
  posterPath TEXT,
  rating INTEGER,
  tagline TEXT,
  tmdbId INTEGER UNIQUE,
  voteAverage REAL,
  voteCount INTEGER,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4))
);

CREATE TABLE TelevisionEpisodes (
  id INTEGER PRIMARY KEY,
  episodeNumber INTEGER,
  seasonNumber INTEGER,
  stillPath TEXT,
  televisionShowId INTEGER NOT NULL,
  videoId INTEGER NOT NULL UNIQUE,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT TelevisionEpisodes_uc_televisionShowId_seasonNumber_episodeNumber UNIQUE (televisionShowId, seasonNumber, episodeNumber),
  CONSTRAINT TelevisionEpisodes_fk_televisionShowId FOREIGN KEY (televisionShowId)
    REFERENCES TelevisionShows (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION,
  CONSTRAINT TelevisionEpisodes_fk_videoId FOREIGN KEY (videoId)
    REFERENCES Videos (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION
);

CREATE INDEX TelevisionShows_ix_name ON TelevisionShows (name);
CREATE INDEX TelevisionEpisodes_ix_televisionShowId ON TelevisionEpisodes (televisionShowId);
CREATE INDEX TelevisionEpisodes_ix_videoId ON TelevisionEpisodes (videoId);

CREATE TRIGGER TelevisionShows_tg_updatedAt
  AFTER UPDATE ON TelevisionShows FOR EACH ROW
BEGIN
  UPDATE TelevisionShows SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

CREATE TRIGGER TelevisionEpisodes_tg_updatedAt
  AFTER UPDATE ON TelevisionEpisodes FOR EACH ROW
BEGIN
  UPDATE TelevisionEpisodes SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TRIGGER TelevisionEpisodes_tg_updatedAt;
DROP TRIGGER TelevisionShows_tg_updatedAt;

DROP INDEX TelevisionEpisodes_ix_videoId;
DROP INDEX TelevisionEpisodes_ix_televisionShowId;
DROP INDEX TelevisionShows_ix_name;

DROP TABLE TelevisionEpisodes;
DROP TABLE TelevisionShows;
