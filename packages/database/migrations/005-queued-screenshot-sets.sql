--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE QueuedScreenshotSets (
  id INTEGER PRIMARY KEY,
  isCreating INTEGER NOT NULL DEFAULT 0,
  pid INTEGER,
  videoId INTEGER NOT NULL UNIQUE,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT QueuedScreenshotSets_ck_isCreating CHECK (isCreating IN (0, 1)),
  CONSTRAINT QueuedScreenshotSets_fk_videoId FOREIGN KEY (videoId)
    REFERENCES Videos (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION
);

CREATE INDEX QueuedScreenshotSets_ix_videoId ON QueuedScreenshotSets (videoId);

CREATE TRIGGER QueuedScreenshotSets_tg_updatedAt
  AFTER UPDATE ON QueuedScreenshotSets FOR EACH ROW
BEGIN
  UPDATE QueuedScreenshotSets SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TRIGGER QueuedScreenshotSets_tg_updatedAt;

DROP INDEX QueuedScreenshotSets_ix_videoId;

DROP TABLE QueuedScreenshotSets;
