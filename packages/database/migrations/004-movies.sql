--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE Collections (
  id INTEGER PRIMARY KEY,
  backdropPath TEXT,
  name TEXT NOT NULL,
  overview TEXT,
  posterPath TEXT,
  tmdbId INTEGER UNIQUE,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4))
);

CREATE TABLE Movies (
  id INTEGER PRIMARY KEY,
  backdropPath TEXT,
  chronology INTEGER,
  collectionId INTEGER,
  posterPath TEXT,
  tagline TEXT,
  tmdbId INTEGER UNIQUE,
  videoId INTEGER NOT NULL UNIQUE,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT Movies_uc_collectionId_chronology UNIQUE (collectionId, chronology),
  CONSTRAINT Movies_fk_collectionId FOREIGN KEY (collectionId)
    REFERENCES Collections (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION,
  CONSTRAINT Movies_fk_videoId FOREIGN KEY (videoId)
    REFERENCES Videos (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION
);

CREATE INDEX Collections_ix_name ON Collections (name);
CREATE INDEX Movies_ix_collectionId ON Movies (collectionId);
CREATE INDEX Movies_ix_videoId ON Movies (videoId);

CREATE TRIGGER Collections_tg_updatedAt
  AFTER UPDATE ON Collections FOR EACH ROW
BEGIN
  UPDATE Collections SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

CREATE TRIGGER Movies_tg_updatedAt
  AFTER UPDATE ON Movies FOR EACH ROW
BEGIN
  UPDATE Movies SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TRIGGER Movies_tg_updatedAt;
DROP TRIGGER Collections_tg_updatedAt;

DROP INDEX Movies_ix_videoId;
DROP INDEX Movies_ix_collectionId;
DROP INDEX Collections_ix_name;

DROP TABLE Movies;
DROP TABLE Collections;
