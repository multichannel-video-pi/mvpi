--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE Tags (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL UNIQUE,
  overview TEXT,
  tmdbId INTEGER UNIQUE,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4))
);

CREATE TABLE Videos (
  id INTEGER PRIMARY KEY,
  bitrate INTEGER NOT NULL,
  duration REAL NOT NULL,
  filename TEXT NOT NULL UNIQUE,
  isActive INTEGER NOT NULL DEFAULT 1,
  overview TEXT,
  rating INTEGER,
  releaseDate TEXT,
  size INTEGER NOT NULL,
  title TEXT,
  voteAverage REAL,
  voteCount INTEGER,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT Videos_ck_isActive CHECK (isActive IN (0, 1))
);

CREATE TABLE Screenshots (
  id INTEGER PRIMARY KEY,
  altText TEXT,
  filename TEXT NOT NULL UNIQUE,
  isActive INTEGER NOT NULL DEFAULT 1,
  videoId INTEGER NOT NULL,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT Screenshots_ck_isActive CHECK (isActive IN (0, 1)),
  CONSTRAINT Screenshots_fk_videoId FOREIGN KEY (videoId)
    REFERENCES Videos (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION
);

CREATE TABLE VideoTags (
  tagId INTEGER NOT NULL,
  videoId INTEGER NOT NULL,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT VideoTags_pk PRIMARY KEY (tagId, videoId),
  CONSTRAINT VideoTags_fk_tagId FOREIGN KEY (tagId)
    REFERENCES Tags (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION,
  CONSTRAINT VideoTags_fk_videoId FOREIGN KEY (videoId)
    REFERENCES Videos (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION
);

CREATE INDEX Screenshots_ix_videoId ON Screenshots (videoId);
CREATE INDEX VideoTags_ix_tagId ON VideoTags (tagId);
CREATE INDEX VideoTags_ix_videoId ON VideoTags (videoId);

CREATE TRIGGER Screenshots_tg_updatedAt
  AFTER UPDATE ON Screenshots FOR EACH ROW
BEGIN
  UPDATE Screenshots SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

CREATE TRIGGER Tags_tg_updatedAt
  AFTER UPDATE ON Tags FOR EACH ROW
BEGIN
  UPDATE Tags SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

CREATE TRIGGER Videos_tg_updatedAt
  AFTER UPDATE ON Videos FOR EACH ROW
BEGIN
  UPDATE Videos SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

CREATE TRIGGER VideoTags_tg_updatedAt
  AFTER UPDATE ON VideoTags FOR EACH ROW
BEGIN
  UPDATE VideoTags SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TRIGGER VideoTags_tg_updatedAt;
DROP TRIGGER Videos_tg_updatedAt;
DROP TRIGGER Tags_tg_updatedAt;
DROP TRIGGER Screenshots_tg_updatedAt;

DROP INDEX VideoTags_ix_videoId;
DROP INDEX VideoTags_ix_tagId;
DROP INDEX Screenshots_ix_videoId;

DROP TABLE VideoTags;
DROP TABLE Screenshots;
DROP TABLE Videos;
DROP TABLE Tags;
