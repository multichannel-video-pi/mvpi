--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE Schedules (
  id INTEGER PRIMARY KEY,
  startTime INTEGER NOT NULL,
  tagId INTEGER NOT NULL,
  videoId INTEGER NOT NULL,
  createdAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  updatedAt INTEGER NOT NULL DEFAULT (strftime('%s','now') || substr(strftime('%f','now'),4)),
  CONSTRAINT Schedules_fk_tagId FOREIGN KEY (tagId)
    REFERENCES Tags (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION,
  CONSTRAINT Schedules_fk_videoId FOREIGN KEY (videoId)
    REFERENCES Videos (id)
      ON DELETE CASCADE
      ON UPDATE NO ACTION
);

CREATE INDEX Schedules_ix_startTime ON Schedules (startTime);
CREATE INDEX Schedules_ix_tagId ON Schedules (tagId);
CREATE INDEX Schedules_ix_videoId ON Schedules (videoId);

CREATE TRIGGER Schedules_tg_updatedAt
  AFTER UPDATE ON Schedules FOR EACH ROW
BEGIN
  UPDATE Schedules SET updatedAt = (strftime('%s','now') || substr(strftime('%f','now'),4)) WHERE id = old.id;
END;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TRIGGER Schedules_tg_updatedAt;

DROP INDEX Schedules_ix_videoId;
DROP INDEX Schedules_ix_tagId;
DROP INDEX Schedules_ix_startTime;

DROP TABLE Schedules;
