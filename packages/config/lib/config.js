import { flatten, unflatten } from "@mvpi/util";
import dotenv from "dotenv";
import findUp from "find-up";
import fs from "fs";
import path from "path";
import process from "process";
import yaml from "js-yaml";
import { fileURLToPath } from "url";

import parseConfigValue from "./parseConfigValue.js";
import updateProductionServices from "./updateProductionServices.js";

const dirname = path.dirname(fileURLToPath(import.meta.url));
const configFilePath =
  process.env.CONFIG_FILE_PATH || path.join(dirname, "..", "config.yml");
const envFilePath =
  process.env.ENV_FILE_PATH || findUp.sync(".env", { cwd: dirname });

const configFile = yaml.load(fs.readFileSync(configFilePath, "utf8"));
const envFile = envFilePath
  ? dotenv.parse(fs.readFileSync(envFilePath, "utf8"))
  : {};

const defaultConfig =
  process.env.NODE_ENV === "production"
    ? updateProductionServices(configFile)
    : configFile;

const config = unflatten(
  {
    ...flatten(defaultConfig),
    ...envFile,
    ...process.env,
  },
  { valueParser: parseConfigValue }
);

export default config;
