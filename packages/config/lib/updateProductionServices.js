const updateProductionServices = (config) => {
  return {
    ...config,
    media: {
      ...config.media,
      service: {
        ...config.media.service,
        ...config.mvpi.service,
      },
      ui: {
        ...config.media.ui,
        ...config.mvpi.service,
      },
    },
    metadata: {
      ...config.metadata,
      service: {
        ...config.metadata.service,
        ...config.mvpi.service,
      },
      ui: {
        ...config.metadata.ui,
        ...config.mvpi.service,
      },
    },
    remote: {
      ...config.remote,
      service: {
        ...config.remote.service,
        ...config.mvpi.service,
        protocol: config.mvpi.service.protocol === "https" ? "wss" : "ws",
      },
      ui: {
        ...config.remote.ui,
        ...config.mvpi.service,
      },
    },
  };
};

export default updateProductionServices;
