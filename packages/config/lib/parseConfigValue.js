import { parseValue } from "@mvpi/util";
import os from "os";

const homeDirectory = os.homedir();

const parseConfigValue = (value) => {
  const parsedValue = parseValue(value);
  if (!homeDirectory || typeof parsedValue !== "string") return parsedValue;
  return parsedValue.replace(/^~(?=$|\/|\\)/, homeDirectory);
};

export default parseConfigValue;
