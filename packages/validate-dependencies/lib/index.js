import assert from "assert";
import { promises as fs } from "fs";

import getConsumerPackageNamesProvider from "./getConsumerPackageNamesProvider.js";
import getNestedJSDependenciesProvider from "./getNestedJSDependenciesProvider.js";
import getNonProjectPackageEntries from "./getNonProjectPackageEntries.js";
import getPackageEntries from "./getPackageEntries.js";
import ValidateDependenciesError from "./ValidateDependenciesError.js";

const validateDependencies = async (rootDir) => {
  const stats = await fs.stat(rootDir);
  assert(stats.isDirectory(), `${rootDir} is not a directory`);

  const pkgEntries = await getPackageEntries(rootDir);
  const nonProjectPkgEntries = await getNonProjectPackageEntries(pkgEntries);
  const pkgObj = Object.fromEntries([...pkgEntries, ...nonProjectPkgEntries]);

  const getConsumerPackageNames = getConsumerPackageNamesProvider(pkgObj);
  const getNestedJSDependencies = getNestedJSDependenciesProvider(pkgObj);

  const errors = pkgEntries.flatMap(
    ([
      pkgName,
      { dependencies, devDependencies, jsDependencies, peerDependencies },
    ]) => {
      // js dependency not found in package.json
      const jsDependencyErrors = jsDependencies
        .filter(
          (dependency) =>
            !dependencies.includes(dependency) &&
            !devDependencies.includes(dependency)
        )
        .map(
          (dependency) =>
            new Error(`${pkgName}: "${dependency}" not found in package.json`)
        );

      // package.json dependency not found in js
      const dependencyErrors = dependencies
        .filter(
          (dependency) =>
            !jsDependencies.includes(dependency) &&
            !getNestedJSDependencies(dependency, [
              ...dependencies,
              ...devDependencies,
            ]).includes(dependency)
        )
        .map(
          (dependency) =>
            new Error(`${pkgName}: dependency "${dependency}" not found in js`)
        );

      // package.json devDependency not found in js
      const devDependencyErrors = devDependencies
        .filter(
          (dependency) =>
            !jsDependencies.includes(dependency) &&
            !getNestedJSDependencies(dependency, [
              ...dependencies,
              ...devDependencies,
            ]).includes(dependency)
        )
        .map(
          (dependency) =>
            new Error(
              `${pkgName}: devDependency "${dependency}" not found in js`
            )
        );

      // package.json peerDependency not found in js
      const peerDependencyErrors = peerDependencies
        .filter(
          (dependency) =>
            !jsDependencies.includes(dependency) &&
            !getNestedJSDependencies(dependency, [
              ...dependencies,
              ...devDependencies,
            ]).includes(dependency)
        )
        .map(
          (dependency) =>
            new Error(
              `${pkgName}: peerDependency "${dependency}" not found in js`
            )
        );

      // package consumers do not provide peer dependencies
      const packageErrors = peerDependencies
        .flatMap((dependency) =>
          getConsumerPackageNames(
            pkgName,
            dependency
          ).map((consumerPkgName) => [
            dependency,
            consumerPkgName,
            pkgObj[consumerPkgName],
          ])
        )
        .filter(
          ([dependency, , consumerPkg]) =>
            !consumerPkg.dependencies.includes(dependency) &&
            !consumerPkg.devDependencies.includes(dependency)
        )
        .map(
          ([dependency, consumerPkgName]) =>
            new Error(
              `${pkgName}: peerDependency "${dependency}" not found in ${consumerPkgName} dependencies`
            )
        );

      // package has a circular dependency
      const circularDependencyErrors = [...dependencies, ...devDependencies]
        .map((dependency) => [dependency, pkgObj[dependency]])
        .filter(
          ([, pkg]) =>
            pkg?.dependencies.includes(pkgName) ||
            pkg?.devDependencies.includes(pkgName)
        )
        .map(
          ([dependency]) =>
            new Error(
              `${pkgName}: dependency "${dependency}" is a circular dependency of ${pkgName}`
            )
        );

      return [
        ...jsDependencyErrors,
        ...dependencyErrors,
        ...devDependencyErrors,
        ...peerDependencyErrors,
        ...packageErrors,
        ...circularDependencyErrors,
      ];
    }
  );

  if (errors.length) throw new ValidateDependenciesError(errors);
  return null;
};

export default validateDependencies;
