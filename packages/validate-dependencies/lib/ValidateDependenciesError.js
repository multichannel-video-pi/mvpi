class ValidateDependenciesError extends Error {
  #validationErrors;

  constructor(errors) {
    super(
      errors.length === 1 ? "1 error found" : `${errors.length} errors found`
    );

    this.name = "ValidateDependenciesError";
    this.#validationErrors = errors;
  }

  get validationErrors() {
    return this.#validationErrors;
  }
}

export default ValidateDependenciesError;
