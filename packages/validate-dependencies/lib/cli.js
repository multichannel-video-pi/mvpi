#!/usr/bin/env node

import c from "ansi-colors";
import os from "os";
import path from "path";
import validateDependencies from "./index.js";
import ValidateDependenciesError from "./ValidateDependenciesError.js";

(async () => {
  try {
    process.stdout.write(os.EOL);
    process.stdout.write("@mvpi/validate-dependencies: ");
    process.stdout.write(c.bold.cyan("validating project dependencies"));
    process.stdout.write(os.EOL);
    process.stdout.write(
      c.gray("------------------------------------------------------------")
    );
    process.stdout.write(os.EOL);

    const [relativePath = "/"] = process.argv.slice(2);
    const rootDir = path.join(process.cwd(), relativePath, "packages");
    await validateDependencies(rootDir);

    process.stdout.write("dependency validation complete: ");
    process.stdout.write(c.bold.green("0 errors found"));
    process.stdout.write(os.EOL);
    process.stdout.write(os.EOL);
    process.exit(0);
  } catch (err) {
    if (err instanceof ValidateDependenciesError) {
      err.validationErrors.forEach(({ message }) => {
        process.stderr.write(c.red(message));
        process.stderr.write(os.EOL);
      });
      process.stderr.write(os.EOL);
    }

    process.stderr.write("dependency validation complete: ");
    process.stderr.write(c.bold.red(err.message));
    process.stderr.write(os.EOL);
    process.stderr.write(os.EOL);
    process.exit(1);
  }
})();
