import { promises as fs } from "fs";
import path from "path";

import isIgnoredDirectory from "./isIgnoredDirectory.js";

const getFilesInDirectory = async (dir, endsWith) => {
  const items = await fs.readdir(dir);
  const itemsWithStats = await Promise.all(
    items.map(async (item) => {
      const key = path.join(dir, item);
      return [key, await fs.stat(key)];
    })
  );
  const files = await Promise.all(
    itemsWithStats
      .filter(([item, stats]) => {
        if (stats.isFile() && item.endsWith(endsWith)) return true;
        if (stats.isDirectory() && !isIgnoredDirectory(item)) return true;
        return false;
      })
      .map(async ([item, stats]) => {
        if (stats.isDirectory()) return getFilesInDirectory(item, endsWith);
        return item;
      })
  );
  return files.flatMap((file) => file);
};

export default getFilesInDirectory;
