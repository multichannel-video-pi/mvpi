import { promises as fs } from "fs";

import { IGNORED_JS_DEPENDENCIES } from "./constants.js";
import getFilesInDirectory from "./getFilesInDirectory.js";

const getJSDependencies = async (pkgRoot) => {
  const jsFiles = await getFilesInDirectory(pkgRoot, ".js");
  const modules = await Promise.all(
    jsFiles.map(async (jsFile) => fs.readFile(jsFile, "utf8"))
  );

  const jsDependencies = [
    ...new Set(
      modules
        .flatMap((js) => js.match(/(?<=(from |import[ (])")[^.].+?(?=")/gs))
        .filter((dep) => dep && !IGNORED_JS_DEPENDENCIES.includes(dep))
        .map((dep) =>
          dep.startsWith("@")
            ? dep.split("/").slice(0, 2).join("/")
            : dep.split("/")[0]
        )
    ),
  ];

  return jsDependencies.sort();
};

export default getJSDependencies;
