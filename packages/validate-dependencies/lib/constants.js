export const IGNORED_DIRECTORIES = ["build", "dist", "node_modules"];

export const IGNORED_JS_DEPENDENCIES = [
  "assert",
  "child_process",
  "events",
  "fs",
  "http",
  "module",
  "os",
  "path",
  "process",
  "stream",
  "url",
  "util",
  "zlib",
];

export const IGNORED_PACKAGE_DEPENDENCIES = [
  "@mvpi/media-ui",
  "@mvpi/metadata-ui",
  "@mvpi/react-scripts",
  "@mvpi/remote-ui",
  "pino-pretty",
  "react-scripts",
];
