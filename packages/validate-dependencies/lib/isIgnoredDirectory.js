import { IGNORED_DIRECTORIES } from "./constants.js";

const isIgnoredDirectory = (dir) =>
  !!IGNORED_DIRECTORIES.find((ignored) => dir.endsWith(ignored));

export default isIgnoredDirectory;
