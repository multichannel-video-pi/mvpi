const getNestedJSDependenciesProvider = (pkgOjb) => {
  const getNestedJSDependencies = (dependency, dependencies) => {
    return [
      ...new Set(
        dependencies.flatMap((pkgName) => {
          const pkg = pkgOjb[pkgName];
          if (!pkg?.peerDependencies.includes(dependency)) return [];
          const nested = getNestedJSDependencies(dependency, [
            ...pkg.dependencies,
            ...pkg.devDependencies,
          ]);
          return [...pkg.jsDependencies, ...nested];
        })
      ),
    ];
  };
  return getNestedJSDependencies;
};

export default getNestedJSDependenciesProvider;
