import { promises as fs } from "fs";

import { IGNORED_PACKAGE_DEPENDENCIES } from "./constants.js";

const getPackageDependencies = async (pkg) => {
  const pkgJSON = JSON.parse(await fs.readFile(pkg, "utf8"));
  const name = pkgJSON.name;

  const dependencies = Object.keys(pkgJSON.dependencies || {})
    .filter((dependency) => !IGNORED_PACKAGE_DEPENDENCIES.includes(dependency))
    .sort();
  const devDependencies = Object.keys(pkgJSON.devDependencies || {})
    .filter((dependency) => !IGNORED_PACKAGE_DEPENDENCIES.includes(dependency))
    .sort();
  const peerDependencies = Object.keys(pkgJSON.peerDependencies || {})
    .filter((dependency) => !IGNORED_PACKAGE_DEPENDENCIES.includes(dependency))
    .sort();

  return { dependencies, devDependencies, name, peerDependencies };
};

export default getPackageDependencies;
