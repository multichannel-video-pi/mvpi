import path from "path";

import getESLintJSDependencies from "./getESLintJSDependencies.js";
import getFilesInDirectory from "./getFilesInDirectory.js";
import getJSDependencies from "./getJSDependencies.js";
import getPackageDependencies from "./getPackageDependencies.js";

const getPackageEntries = async (dir) => {
  const pkgs = await getFilesInDirectory(dir, "package.json");
  return Promise.all(
    pkgs.map(async (pkg) => {
      const pkgRoot = path.dirname(pkg);
      const {
        dependencies,
        devDependencies,
        name,
        peerDependencies,
      } = await getPackageDependencies(pkg);

      if (name.endsWith("eslint-config")) {
        const jsDependencies = await getESLintJSDependencies(pkgRoot);
        return [
          name,
          {
            dependencies,
            devDependencies,
            jsDependencies,
            peerDependencies,
          },
        ];
      }

      const jsDependencies = await getJSDependencies(pkgRoot);
      return [
        name,
        {
          dependencies,
          devDependencies,
          jsDependencies,
          peerDependencies,
        },
      ];
    })
  );
};

export default getPackageEntries;
