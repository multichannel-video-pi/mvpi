const getConsumerPackageNamesProvider = (pkgOjb) => {
  const pkgEntries = Object.entries(pkgOjb);
  const getConsumerPackageNames = (pkgName, dependency) => {
    return [
      ...new Set(
        pkgEntries
          .filter(
            ([, { dependencies, devDependencies }]) =>
              dependencies.includes(pkgName) ||
              devDependencies.includes(pkgName)
          )
          .flatMap(([consumerPkgName, { peerDependencies }]) => {
            if (peerDependencies.includes(dependency))
              return [
                consumerPkgName,
                ...getConsumerPackageNames(consumerPkgName, dependency),
              ];
            return consumerPkgName;
          })
      ),
    ];
  };
  return getConsumerPackageNames;
};

export default getConsumerPackageNamesProvider;
