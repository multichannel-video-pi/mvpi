import { createRequire } from "module";

import getPackageDependencies from "./getPackageDependencies.js";

const require = createRequire(import.meta.url);

const getNonProjectPackageEntries = async (pkgEntries) => {
  const pkgObj = Object.fromEntries(pkgEntries);
  const pkgDependencies = [
    ...new Set(
      pkgEntries.flatMap(
        ([, { dependencies, devDependencies, peerDependencies }]) => [
          ...dependencies,
          ...devDependencies,
          ...peerDependencies,
        ]
      )
    ),
  ];

  return Promise.all(
    pkgDependencies
      .filter((pkgName) => !pkgObj[pkgName])
      .map(async (pkgName) => {
        try {
          const pkg = require.resolve(`${pkgName}/package.json`);
          const { peerDependencies } = await getPackageDependencies(pkg);
          return [
            pkgName,
            {
              dependencies: [],
              devDependencies: [],
              jsDependencies: peerDependencies,
              peerDependencies,
            },
          ];
        } catch {
          return [
            pkgName,
            {
              dependencies: [],
              devDependencies: [],
              jsDependencies: [],
              peerDependencies: [],
            },
          ];
        }
      })
  );
};

export default getNonProjectPackageEntries;
