import { promises as fs } from "fs";
import getFilesInDirectory from "./getFilesInDirectory.js";

const getESLintJSDependencies = async (pkgRoot) => {
  const configurationFiles = await getFilesInDirectory(pkgRoot, ".json");
  const configurations = await Promise.all(
    configurationFiles.map(async (file) =>
      JSON.parse(await fs.readFile(file, "utf8"))
    )
  );

  const jsDependencies = [
    ...new Set(
      configurations.flatMap((config) => {
        const parser = config.parser;
        const plugins =
          config.plugins?.map((plugin) => `eslint-plugin-${plugin}`) || [];
        return parser ? [parser, ...plugins] : plugins;
      })
    ),
  ];

  return jsDependencies.sort();
};

export default getESLintJSDependencies;
