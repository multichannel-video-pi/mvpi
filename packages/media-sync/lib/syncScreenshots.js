import config from "@mvpi/config";
import database from "@mvpi/database";
import { Screenshot, Video } from "@mvpi/models";
import { promises as fs } from "fs";
import glob from "glob";
import path from "path";
import { promisify } from "util";

import logger from "./logger.js";

const syncScreenshots = async () => {
  await database();

  logger.debug("retrieving screenshot metadata records");
  const screenshots = await Screenshot.select();

  logger.debug("removing active screenshot metadata with missing files");
  await Promise.all(
    screenshots
      .filter((screenshot) => screenshot.isActive)
      .map(async (screenshot) => {
        try {
          await fs.stat(screenshot.filename);
        } catch (err) {
          if (err.code === "ENOENT")
            return Screenshot.deleteByKey(
              Screenshot.getPrimaryKeyConditions(screenshot)
            );
        }
        return null;
      })
  );

  logger.debug(
    `retrieving screenshot files in ${config.screenshots.directory}`
  );
  const directoryFilenames = await promisify(glob)(
    `${config.screenshots.directory}/**/*.{png}`
  );

  logger.debug("removing orphaned screenshots");
  const metadataFilenames = screenshots.map(
    (screenshot) => screenshot.filename
  );
  await Promise.all(
    directoryFilenames
      .filter((filename) => !metadataFilenames.includes(filename))
      .map(async (filename) => {
        try {
          logger.debug(`removing file ${filename}`);
          await fs.unlink(filename);
        } catch {
          logger.warn(`failed to remove orphaned screenshot ${filename}`);
        }
      })
  );

  logger.debug("removing empty directories");
  await Promise.all(
    [
      ...new Set([
        ...directoryFilenames.map((filename) => path.dirname(filename)),
        ...metadataFilenames.map((filename) => path.dirname(filename)),
      ]),
    ].map(async (directory) => {
      try {
        const items = await fs.readdir(directory);
        if (!items.length) {
          logger.debug(`removing directory ${directory}`);
          await fs.rmdir(directory);
        }
      } catch (err) {
        logger.warn(`failed to read and/or remove directory ${directory}`);
      }
    })
  );

  logger.debug("scheduling video screenshot creation");
  const screenshotVideoIds = await Screenshot.getScreenshotVideoIds();
  const videos = await Video.select();

  return Promise.all(
    videos
      .filter(
        (video) => video.isActive && !screenshotVideoIds.includes(video.id)
      )
      .map(async (video) => Screenshot.createScreenshots({ videoId: video.id }))
  );
};

export default syncScreenshots;
