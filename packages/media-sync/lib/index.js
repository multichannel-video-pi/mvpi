import logger from "./logger.js";
import syncVideos from "./syncVideos.js";

const mediaSync = async () => {
  const createdVideos = await syncVideos();

  logger.info(`media sync complete:

${createdVideos.length} video records created
`);
};

export default mediaSync;
