#!/usr/bin/env node

import mediaSync from "./index.js";
import logger from "./logger.js";

(async () => {
  try {
    await mediaSync();
    process.exit(0);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
