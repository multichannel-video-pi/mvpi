import config from "@mvpi/config";
import database from "@mvpi/database";
import { Video } from "@mvpi/models";
import { promises as fs } from "fs";
import glob from "glob";
import { promisify } from "util";

import logger from "./logger.js";

const syncVideos = async () => {
  await database();

  logger.debug("retrieving video metadata records");
  const videos = await Video.select();

  logger.debug("updating video metadata with missing files");
  await Promise.all(
    videos.map(async (video) => {
      try {
        const filename = Video.getAbsolutePathToVideo(video.filename);
        await fs.stat(filename);
      } catch (err) {
        if (err.code === "ENOENT")
          return Video.updateByKey({ ...video, isActive: 0 });
      }
      return null;
    })
  );

  logger.debug(`retrieving video files in ${config.videos.directory}`);
  const directoryFilenames = await promisify(glob)(
    `${config.videos.directory}/**/*.{mp4,m4v}`
  );

  logger.debug("adding new video metadata records");
  const metadataFilenames = videos.map((video) =>
    Video.getAbsolutePathToVideo(video.filename)
  );
  return Promise.all(
    directoryFilenames
      .filter((filename) => !metadataFilenames.includes(filename))
      .map(async (filename) => Video.createVideo({ filename }))
  );
};

export default syncVideos;
