const data = {
  connections: [],
};

export const addConnection = (connection) => {
  return (data.connections = [
    ...data.connections.filter(
      (previousConnection) =>
        previousConnection.connected && previousConnection !== connection
    ),
    connection,
  ]);
};

export const listConnections = () => data.connections;

export const removeConnection = (connection) => {
  return (data.connections = [
    ...data.connections.filter(
      (previousConnection) =>
        previousConnection.connected && previousConnection !== connection
    ),
  ]);
};

export default data;
