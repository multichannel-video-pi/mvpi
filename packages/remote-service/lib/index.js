import config from "@mvpi/config";
import websocket from "websocket";

import { removeConnection } from "./connections.js";
import logger from "./logger.js";
import messageReducer from "./message.js";

const WebSocketServer = websocket.server;

const createRemoteService = async (httpServer) => {
  logger.debug("creating remote service");
  const webSocketServer = new WebSocketServer({
    autoAcceptConnections: false,
    httpServer,
  });

  webSocketServer.on("request", async (req) => {
    try {
      const connection = req.accept(
        config.remote.service.applicationProtocol,
        req.origin
      );
      connection.on("message", (message) =>
        messageReducer(message, connection)
      );
      connection.on("close", () => removeConnection(connection));
    } catch (err) {
      logger.error(err);
      req.reject(500, "Internal Server Error");
    }
  });

  return httpServer;
};

export default createRemoteService;
