import { listConnections } from "./connections.js";

const dispatch = (action) => {
  listConnections()
    .filter((connection) => connection.connected)
    .forEach((connection) => connection.sendUTF(action));
};

export default dispatch;
