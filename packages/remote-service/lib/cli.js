#!/usr/bin/env node

import config from "@mvpi/config";
import http from "http";

import createRemoteService from "./index.js";
import logger from "./logger.js";

(async () => {
  try {
    const app = await createRemoteService(
      http.createServer((req, res) => {
        logger.info(`${req.method} ${req.url}`);
        res.writeHead(404);
        res.end();
      })
    );

    app.listen(config.remote.service.port, () => {
      logger.info(
        `remote service listening on port ${config.remote.service.port}`
      );
    });
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
