import {
  REMOTE_CLIENT_DISPATCH,
  REMOTE_CLIENT_SUBSCRIBE,
  REMOTE_CLIENT_UNSUBSCRIBE,
} from "@mvpi/remote-client/lib/actions.js";

import { addConnection, removeConnection } from "./connections.js";
import dispatch from "./dispatch.js";
import logger from "./logger.js";

const parseActionFromUTF8Data = (utf8Data) => {
  try {
    return JSON.parse(utf8Data);
  } catch (err) {
    logger.error(err);
    logger.error(`Failed to parse action from: ${utf8Data}`);
    return {};
  }
};

const reducer = (message, connection) => {
  const action = parseActionFromUTF8Data(message?.utf8Data);

  switch (action.type) {
    case REMOTE_CLIENT_DISPATCH:
      return dispatch(action.payload);
    case REMOTE_CLIENT_SUBSCRIBE:
      return addConnection(connection);
    case REMOTE_CLIENT_UNSUBSCRIBE:
      return removeConnection(connection);
    default:
      return null;
  }
};

export default reducer;
