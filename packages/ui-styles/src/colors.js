export const TRUE_BLACK = "hsl(0, 0%, 0%)";
export const BLACK = "hsl(0, 0%, 4%)";
export const BLACK_BIS = "hsl(0, 0%, 7%)";
export const BLACK_TER = "hsl(0, 0%, 14%)";
export const GREY_DARKER = "hsl(0, 0%, 21%)";
export const GREY_DARK = "hsl(0, 0%, 29%)";
export const GREY = "hsl(0, 0%, 48%)";
export const GREY_LIGHT = "hsl(0, 0%, 71%)";
export const GREY_LIGHTER = "hsl(0, 0%, 86%)";
export const WHITE_TER = "hsl(0, 0%, 96%)";
export const WHITE_BIS = "hsl(0, 0%, 98%)";
export const WHITE = "hsl(0, 0%, 100%)";

export const BLUE_DARK = "hsl(205, 60%, 16%)";
export const BLUE = "hsl(205, 74%, 40%)";
export const BLUE_A25 = "hsla(205, 74%, 40%, 0.25)";
export const BLUE_LIGHT = "hsl(206, 100%, 63%)";
export const BLUE_LIGHTER = "hsl(206, 100%, 90%)";
export const BLUE_LIGHTEST = "hsl(207, 100%, 96%)";

export const PURPLE = "hsl(237, 50%, 31%)";

export const RED_DARK = "hsl(1, 100%, 34%)";
export const RED = "hsl(1, 100%, 68%)";
export const RED_LIGHT = "hsl(1, 100%, 85%)";

export const GREEN_DARK = "hsl(60, 60%, 42%)";
export const GREEN = "hsl(60, 74%, 46%)";

export const YELLOW = "hsl(48, 100%, 67%)";

export const BROWN = "hsl(35, 90%, 37%)";
export const TAN = "hsl(36, 100%, 63%)";

export const BACKGROUND_MASK = "hsla(0, 0%, 10%, 0.75)";
export const TRANSPARENT = "hsla(0, 0%, 0%, 0)";
