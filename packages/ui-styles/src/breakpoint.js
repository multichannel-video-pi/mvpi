export const breakpoint = (width) => `@media (min-width: ${width}px)`;

export const mobileWide = breakpoint(576);
export const tablet = breakpoint(768);
export const tabletWide = breakpoint(992);
export const desktop = breakpoint(1200);

export default breakpoint;
