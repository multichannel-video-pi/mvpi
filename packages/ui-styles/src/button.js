import { css } from "@emotion/react";

import {
  BLUE,
  BLUE_LIGHT,
  BROWN,
  GREEN,
  GREEN_DARK,
  GREY_LIGHTER,
  RED,
  RED_DARK,
  TAN,
  TRANSPARENT,
  WHITE_BIS,
} from "./colors.js";

export const MIN_BUTTON_HEIGHT = "2.375rem";

export const buttonStyles = css`
  position: relative;
  border: 2px solid ${BLUE_LIGHT};
  padding: 0.25rem 0.75rem;
  min-height: ${MIN_BUTTON_HEIGHT};
  background-color: ${BLUE};
  font-size: 1rem;
  letter-spacing: 1px;
  color: ${WHITE_BIS};
  cursor: pointer;
  transition: all 100ms;

  &:hover,
  &:focus {
    border-style: solid;
    background-color: ${BLUE_LIGHT};
  }

  &:active {
    border-color: ${BLUE};
    background-color: ${BLUE};
  }

  &:focus {
    outline: 0;
  }

  &[disabled] {
    border-color: ${GREY_LIGHTER};
    background-color: ${GREY_LIGHTER};
    color: ${WHITE_BIS};
    cursor: not-allowed;
  }

  &::before {
    content: "";
    position: absolute;
    left: 0;
    width: 100%;
    text-align: center;
    color: ${WHITE_BIS};
  }
`;

export const buttonDangerStyles = css`
  border-color: ${RED};
  background-color: ${RED_DARK};

  &:hover,
  &:focus {
    background-color: ${RED};
  }

  &:active {
    border-color: ${RED_DARK};
    background-color: ${RED_DARK};
  }

  &[disabled] {
    border-color: ${GREY_LIGHTER};
    background-color: ${GREY_LIGHTER};
  }
`;

export const buttonLoadingStyles = css`
  color: ${TRANSPARENT};

  &[disabled] {
    color: ${TRANSPARENT};
  }
`;

export const buttonSuccessStyles = css`
  border-color: ${GREEN};
  background-color: ${GREEN_DARK};

  &:hover,
  &:focus {
    background-color: ${GREEN};
  }

  &:active {
    border-color: ${GREEN_DARK};
    background-color: ${GREEN_DARK};
  }

  &[disabled] {
    border-color: ${GREY_LIGHTER};
    background-color: ${GREY_LIGHTER};
  }
`;

export const buttonWarningStyles = css`
  border-color: ${TAN};
  background-color: ${BROWN};

  &:hover,
  &:focus {
    background-color: ${TAN};
  }

  &:active {
    border-color: ${BROWN};
    background-color: ${BROWN};
  }

  &[disabled] {
    border-color: ${GREY_LIGHTER};
    background-color: ${GREY_LIGHTER};
  }
`;

export default buttonStyles;
