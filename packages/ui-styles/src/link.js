import { css } from "@emotion/react";

import { BLUE, RED } from "./colors.js";

export const linkStyles = css`
  color: ${BLUE};
  text-decoration: none;

  &:active {
    color: ${RED};
  }

  &:hover {
    text-decoration: underline;
  }
`;

export default linkStyles;
