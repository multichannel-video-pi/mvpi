import { css } from "@emotion/react";

import { CODE_FONT, DEFAULT_FONT } from "./fonts.js";

const globalStyles = css`
  html {
    box-sizing: border-box;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  html,
  body {
    font-size: 16px;
  }

  body {
    font-family: ${DEFAULT_FONT};
    letter-spacing: 1px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: ${CODE_FONT};
  }
`;

export default globalStyles;
