export const CODE_FONT = `source-code-pro, Menlo, Monaco, Consolas, "Courier New", monospace`;

export const DEFAULT_FONT = `-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif`;
