import { GraphQLError, GraphQLScalarType, Kind } from "graphql";

export const foreignKeyValue = (outputValue) => {
  if (typeof outputValue === "string") {
    return outputValue;
  }

  if (Number.isInteger(outputValue)) {
    return String(outputValue);
  }

  throw new GraphQLError(`ForeignKey cannot represent value: ${outputValue}`);
};

export const parseForeignKeyLiteral = (ast) => {
  if (ast.kind !== Kind.STRING && ast.kind !== Kind.INT) {
    throw new GraphQLError(
      `ForeignKey cannot represent a non-string and non-integer value: ${ast.value}`
    );
  }

  return ast.value;
};

const ForeignKey = new GraphQLScalarType({
  name: "ForeignKey",
  description:
    'The custom `ForeignKey` scalar type represents an attribute that defines a relationship, or part of a relationship, to an `ID` or primary key of another type. Like the ID type, the ForeignKey type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as a ForeignKey.',
  serialize: foreignKeyValue,
  parseValue: foreignKeyValue,
  parseLiteral: parseForeignKeyLiteral,
});

export default ForeignKey;
