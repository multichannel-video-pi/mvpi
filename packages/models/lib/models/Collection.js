import { Table } from "@mvpi/database";
import gql from "graphql-tag";

class Collection extends Table {
  static table = "Collections";
  static typeDefs = gql`
    type Collection {
      id: ID!
      backdropPath: String
      name: String
      overview: String
      posterPath: String
      tmdbId: Int
      createdAt: Float
      updatedAt: Float

      movies: [Movie]
        @oneToMany(
          table: "Movies"
          condition: "Movies.CollectionId = Collections.id"
        )
    }

    extend type Mutation {
      createCollection(
        backdropPath: String
        name: String!
        overview: String
        posterPath: String
        tmdbId: Int
      ): Collection
      deleteCollection(id: ID!): Collection
      updateCollection(
        id: ID!
        backdropPath: String
        name: String
        overview: String
        posterPath: String
        tmdbId: Int
      ): Collection
    }

    extend type Query {
      collection(id: ID!): Collection
      collections: [Collection]
    }
  `;
}

export default Collection;
