import { Table } from "@mvpi/database";
import gql from "graphql-tag";

class TelevisionShow extends Table {
  static table = "TelevisionShows";
  static typeDefs = gql`
    type TelevisionShow {
      id: ID!
      backdropPath: String
      firstAirDate: String
      lastAirDate: String
      name: String
      overview: String
      posterPath: String
      rating: Int
      tagline: String
      tmdbId: Int
      voteAverage: Float
      voteCount: Int
      createdAt: Float
      updatedAt: Float

      televisionEpisodes: [TelevisionEpisode]
        @oneToMany(
          table: "TelevisionEpisodes"
          condition: "TelevisionEpisodes.televisionShowId = TelevisionShows.id"
        )
    }

    extend type Mutation {
      createTelevisionShow(
        backdropPath: String
        firstAirDate: String
        lastAirDate: String
        name: String!
        overview: String
        posterPath: String
        rating: Int
        tagline: String
        tmdbId: Int
      ): TelevisionShow
      deleteTelevisionShow(id: ID!): TelevisionShow
      updateTelevisionShow(
        id: ID!
        backdropPath: String
        firstAirDate: String
        lastAirDate: String
        name: String
        overview: String
        posterPath: String
        rating: Int
        tagline: String
        tmdbId: Int
      ): TelevisionShow
    }

    extend type Query {
      televisionShow(id: ID!): TelevisionShow
      televisionShows: [TelevisionShow]
    }
  `;
}

export default TelevisionShow;
