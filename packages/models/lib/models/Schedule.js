import { Table } from "@mvpi/database";
import { createSchedules, deleteExpiredSchedules } from "@mvpi/schedules";
import gql from "graphql-tag";
import _ from "lodash";

class Schedule extends Table {
  static table = "Schedules";
  static typeDefs = gql`
    input SchedulesOrder {
      tagId: Sort
      startTime: Sort
    }

    type Schedule {
      id: ID!
      startTime: Float
      tagId: ForeignKey
      videoId: ForeignKey
      createdAt: Float
      updatedAt: Float

      tag: Tag @oneToOne(table: "Tags", condition: "Schedules.tagId = Tags.id")

      video: Video
        @oneToOne(table: "Videos", condition: "Schedules.videoId = Videos.id")
    }

    extend type Mutation {
      createSchedule(
        startTime: Float!
        tagId: ForeignKey!
        videoId: ForeignKey!
      ): Schedule
      createSchedules: [Schedule]
      deleteExpiredSchedules: [Schedule]
      deleteSchedule(id: ID!): Schedule
      deleteSchedules(tagId: ForeignKey): [Schedule]
      updateSchedule(
        id: ID!
        startTime: Float
        tagId: ForeignKey
        videoId: ForeignKey
      ): Schedule
    }

    extend type Query {
      schedule(id: ID!): Schedule
      schedules(
        tagId: ForeignKey
        videoId: ForeignKey
        orderBy: SchedulesOrder
      ): [Schedule]
    }
  `;

  static get resolvers() {
    return _.merge(super.resolvers, {
      Mutation: {
        createSchedules: async (parent, params, ctx, info) =>
          Schedule.createSchedules(params, info),
        deleteExpiredSchedules: async (parent, params, ctx, info) =>
          Schedule.deleteExpiredSchedules(
            Schedule.getConditions(params),
            Schedule.getOrder(params),
            info
          ),
        deleteSchedules: async (parent, params, ctx, info) =>
          Schedule.delete(
            Schedule.getConditions(params),
            Schedule.getOrder(params),
            info
          ),
      },
    });
  }

  static createSchedules = createSchedules;
  static deleteExpiredSchedules = deleteExpiredSchedules;
}

export default Schedule;
