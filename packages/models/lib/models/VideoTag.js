import { Table } from "@mvpi/database";
import gql from "graphql-tag";
import _ from "lodash";

class VideoTag extends Table {
  static table = "VideoTags";
  static primaryKey = ["tagId", "videoId"];
  static typeDefs = gql`
    type VideoTag {
      tagId: ID!
      videoId: ID!
      createdAt: Float
      updatedAt: Float
    }

    extend type Mutation {
      createVideoTag(tagId: ID!, videoId: ID!): VideoTag
      createVideoTags(tagIds: [ID]!, videoIds: [ID]!): [VideoTag]
      updateVideoTag(tagId: ID!, videoId: ID!): VideoTag
      deleteVideoTag(tagId: ID!, videoId: ID!): VideoTag
      deleteVideoTags(tagIds: [ID]!, videoIds: [ID]!): [VideoTag]
    }

    extend type Query {
      videoTag(tagId: ID!, videoId: ID!): VideoTag
      videoTags(tagId: ID, videoId: ID): [VideoTag]
    }
  `;

  static get resolvers() {
    return _.merge(super.resolvers, {
      Mutation: {
        createVideoTags: async (parent, params, ctx, info) =>
          VideoTag.createVideoTags(params, info),
        deleteVideoTags: async (parent, params, ctx, info) =>
          VideoTag.deleteVideoTags(params, info),
      },
    });
  }

  static async createVideoTags({ tagIds, videoIds }, info) {
    return Promise.all(
      videoIds.flatMap((videoId) =>
        tagIds.map(async (tagId) => VideoTag.insert({ tagId, videoId }, info))
      )
    );
  }

  static async deleteVideoTags({ tagIds, videoIds }, info) {
    return Promise.all(
      videoIds.flatMap((videoId) =>
        tagIds.map(async (tagId) =>
          VideoTag.deleteByKey({ tagId, videoId }, info)
        )
      )
    );
  }
}

export default VideoTag;
