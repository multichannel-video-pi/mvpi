import { Table } from "@mvpi/database";
import gql from "graphql-tag";

class QueuedScreenshotSet extends Table {
  static table = "QueuedScreenshotSets";
  static typeDefs = gql`
    type QueuedScreenshotSet {
      id: ID!
      isCreating: Boolean
      pid: Int
      videoId: ForeignKey
      createdAt: Float
      updatedAt: Float
    }

    extend type Mutation {
      createQueuedScreenshotSet(videoId: ForeignKey!): QueuedScreenshotSet
      deleteQueuedScreenshotSet(id: ID!): QueuedScreenshotSet
      updateQueuedScreenshotSet(
        id: ID!
        isCreating: Boolean
        pid: Int
        videoId: ForeignKey
      ): QueuedScreenshotSet
    }

    extend type Query {
      queuedScreenshotSet(id: ID!): QueuedScreenshotSet
      queuedScreenshotSets: [QueuedScreenshotSet]
    }
  `;
}

export default QueuedScreenshotSet;
