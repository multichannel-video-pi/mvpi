import { Table } from "@mvpi/database";
import gql from "graphql-tag";

class Tag extends Table {
  static table = "Tags";
  static typeDefs = gql`
    type Tag {
      id: ID!
      name: String
      overview: String
      tmdbId: Int
      createdAt: Float
      updatedAt: Float
    }

    extend type Mutation {
      createTag(name: String!, overview: String, tmdbId: Int): Tag
      deleteTag(id: ID!): Tag
      updateTag(id: ID!, name: String, overview: String, tmdbId: Int): Tag
    }

    extend type Query {
      tag(id: ID!): Tag
      tags: [Tag]
    }
  `;
}

export default Tag;
