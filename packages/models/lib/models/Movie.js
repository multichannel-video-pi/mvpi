import { Table } from "@mvpi/database";
import gql from "graphql-tag";
import _ from "lodash";

class Movie extends Table {
  static table = "Movies";
  static typeDefs = gql`
    type Movie {
      id: ID!
      backdropPath: String
      chronology: Int
      collectionId: ForeignKey
      posterPath: String
      tagline: String
      tmdbId: Int
      videoId: ForeignKey
      createdAt: Float
      updatedAt: Float

      collection: Collection
        @oneToOne(
          table: "Collections"
          condition: "Movies.collectionId = Collections.id"
        )

      video: Video
        @oneToOne(table: "Videos", condition: "Movies.videoId = Videos.id")
    }

    extend type Mutation {
      createMovie(
        backdropPath: String
        chronology: Int
        collectionId: ForeignKey
        posterPath: String
        tagline: String
        tmdbId: Int
        videoId: ForeignKey!
      ): Movie
      createMovies(collectionId: ForeignKey, videoIds: [ForeignKey]!): [Movie]
      deleteMovie(id: ID!): Movie
      deleteMovies(ids: [ID]!): [Movie]
      updateMovie(
        id: ID!
        backdropPath: String
        chronology: Int
        collectionId: ForeignKey
        posterPath: String
        tagline: String
        tmdbId: Int
        videoId: ForeignKey
      ): Movie
      updateMovies(
        ids: [ID]!
        chronology: Int
        collectionId: ForeignKey
      ): [Movie]
    }

    extend type Query {
      movie(id: ID!): Movie
      movies: [Movie]
    }
  `;

  static get resolvers() {
    return _.merge(super.resolvers, {
      Mutation: {
        createMovies: async (parent, params, ctx, info) =>
          Movie.createMovies(params, info),
        deleteMovies: async (parent, params, ctx, info) =>
          Movie.deleteMovies(params, info),
        updateMovies: async (parent, params, ctx, info) =>
          Movie.updateMovies(params, info),
      },
    });
  }

  static async createMovies({ videoIds, ...values }, info) {
    return Promise.all(
      videoIds.map((videoId) => Movie.insert({ videoId, ...values }, info))
    );
  }

  static async deleteMovies({ ids }, info) {
    return Promise.all(ids.map((id) => Movie.deleteByKey({ id }, info)));
  }

  static async updateMovies({ ids, ...values }, info) {
    return Promise.all(
      ids.map((id) => Movie.updateByKey({ id, ...values }, info))
    );
  }
}

export default Movie;
