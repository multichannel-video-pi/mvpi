import { Table } from "@mvpi/database";
import gql from "graphql-tag";
import _ from "lodash";

import {
  createScreenshots,
  deleteScreenshots,
  getScreenshotVideoIds,
} from "@mvpi/screenshots";

class Screenshot extends Table {
  static table = "Screenshots";
  static typeDefs = gql`
    type Screenshot {
      id: ID!
      altText: String
      filename: String
      isActive: Boolean
      videoId: ForeignKey
      createdAt: Float
      updatedAt: Float
    }

    extend type Mutation {
      createScreenshot(
        altText: String
        filename: String!
        videoId: ForeignKey!
      ): Screenshot
      deleteScreenshot(id: ID!): Screenshot
      deleteScreenshots(videoId: ForeignKey!): [Screenshot]
      updateScreenshot(
        id: ID!
        altText: String
        filename: String
        videoId: ForeignKey
      ): Screenshot
    }

    extend type Query {
      screenshot(id: ID!): Screenshot
      screenshots(videoId: ForeignKey): [Screenshot]
    }
  `;

  static get resolvers() {
    return _.merge(super.resolvers, {
      Mutation: {
        deleteScreenshots: async (parent, params, ctx, info) =>
          Screenshot.deleteScreenshots(
            Screenshot.getConditions(params),
            Screenshot.getOrder(params),
            info
          ),
      },
    });
  }

  static createScreenshots = createScreenshots;
  static deleteScreenshots = deleteScreenshots;
  static getScreenshotVideoIds = getScreenshotVideoIds;
}

export default Screenshot;
