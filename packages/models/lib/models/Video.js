import { Table } from "@mvpi/database";
import gql from "graphql-tag";
import _ from "lodash";

import {
  createVideo,
  getAbsolutePathToVideo,
  getDenormalizedVideos,
  getRelativePathToVideo,
  updateVideos,
} from "@mvpi/videos";

class Video extends Table {
  static table = "Videos";
  static typeDefs = gql`
    type Video {
      id: ID!
      bitrate: Int
      duration: Float
      filename: String
      isActive: Boolean
      overview: String
      rating: Int
      releaseDate: String
      size: Float
      title: String
      voteAverage: Float
      voteCount: Int
      createdAt: Float
      updatedAt: Float

      movie: Movie
        @oneToOne(table: "Movies", condition: "Movies.videoId = Videos.id")

      screenshots: [Screenshot]
        @oneToMany(
          table: "Screenshots"
          condition: "Screenshots.videoId = Videos.id"
        )

      tags: [Tag]
        @manyToMany(
          linkTable: "VideoTags"
          linkCondition: "VideoTags.videoId = Videos.id"
          table: "Tags"
          condition: "VideoTags.tagId = Tags.id"
        )

      televisionEpisode: TelevisionEpisode
        @oneToOne(
          table: "TelevisionEpisodes"
          condition: "TelevisionEpisodes.videoId = Videos.id"
        )
    }

    extend type Mutation {
      createVideo(
        filename: String!
        isActive: Boolean
        overview: String
        rating: Int
        releaseDate: String
        title: String
      ): Video
      deleteVideo(id: ID!): Video
      updateVideo(
        id: ID!
        isActive: Boolean
        overview: String
        rating: Int
        releaseDate: String
        title: String
      ): Video
      updateVideos(ids: [ID]!, isActive: Boolean, rating: Int): [Video]
    }

    extend type Query {
      video(id: ID!): Video
      videos: [Video]
    }
  `;

  static get resolvers() {
    return _.merge(super.resolvers, {
      Mutation: {
        createVideo: async (parent, params, ctx, info) =>
          Video.createVideo(params, info),
        updateVideos: async (parent, params, ctx, info) =>
          Video.updateVideos(params, null, null, info),
      },
    });
  }

  static createVideo = createVideo;
  static getAbsolutePathToVideo = getAbsolutePathToVideo;
  static getDenormalizedVideos = getDenormalizedVideos;
  static getRelativePathToVideo = getRelativePathToVideo;
  static updateVideos = updateVideos;
}

export default Video;
