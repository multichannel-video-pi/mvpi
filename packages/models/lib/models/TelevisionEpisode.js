import { Table } from "@mvpi/database";
import gql from "graphql-tag";
import _ from "lodash";

class TelevisionEpisode extends Table {
  static table = "TelevisionEpisodes";
  static typeDefs = gql`
    type TelevisionEpisode {
      id: ID!
      episodeNumber: Int
      seasonNumber: Int
      stillPath: String
      televisionShowId: ForeignKey
      videoId: ForeignKey
      createdAt: Float
      updatedAt: Float

      televisionShow: TelevisionShow
        @oneToOne(
          table: "TelevisionShows"
          condition: "TelevisionEpisodes.televisionShowId = TelevisionShows.id"
        )

      video: Video
        @oneToOne(
          table: "Videos"
          condition: "TelevisionEpisodes.videoId = Videos.id"
        )
    }

    extend type Mutation {
      createTelevisionEpisode(
        episodeNumber: Int
        seasonNumber: Int
        stillPath: String
        televisionShowId: ForeignKey!
        videoId: ForeignKey!
      ): TelevisionEpisode
      createTelevisionEpisodes(
        seasonNumber: Int
        televisionShowId: ForeignKey!
        videoIds: [ForeignKey]!
      ): [TelevisionEpisode]
      deleteTelevisionEpisode(id: ID!): TelevisionEpisode
      deleteTelevisionEpisodes(ids: [ID]!): [TelevisionEpisode]
      updateTelevisionEpisode(
        id: ID!
        episodeNumber: Int
        seasonNumber: Int
        stillPath: String
        televisionShowId: ForeignKey
        videoId: ForeignKey
      ): TelevisionEpisode
      updateTelevisionEpisodes(
        ids: [ID]!
        episodeNumber: Int
        seasonNumber: Int
        televisionShowId: ForeignKey
      ): [TelevisionEpisode]
    }

    extend type Query {
      televisionEpisode(id: ID!): TelevisionEpisode
      televisionEpisodes: [TelevisionEpisode]
    }
  `;

  static get resolvers() {
    return _.merge(super.resolvers, {
      Mutation: {
        createTelevisionEpisodes: async (parent, params, ctx, info) =>
          TelevisionEpisode.createTelevisionEpisodes(params, info),
        deleteTelevisionEpisodes: async (parent, params, ctx, info) =>
          TelevisionEpisode.deleteTelevisionEpisodes(params, info),
        updateTelevisionEpisodes: async (parent, params, ctx, info) =>
          TelevisionEpisode.updateTelevisionEpisodes(params, info),
      },
    });
  }

  static async createTelevisionEpisodes({ videoIds, ...values }, info) {
    return Promise.all(
      videoIds.map((videoId) =>
        TelevisionEpisode.insert({ videoId, ...values }, info)
      )
    );
  }

  static async deleteTelevisionEpisodes({ ids }, info) {
    return Promise.all(
      ids.map((id) => TelevisionEpisode.deleteByKey({ id }, info))
    );
  }

  static async updateTelevisionEpisodes({ ids, ...values }, info) {
    return Promise.all(
      ids.map((id) => TelevisionEpisode.updateByKey({ id, ...values }, info))
    );
  }
}

export default TelevisionEpisode;
