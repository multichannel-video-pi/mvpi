import schema from "./schema.js";

export { default as Collection } from "./models/Collection.js";
export { default as Movie } from "./models/Movie.js";
export { default as Schedule } from "./models/Schedule.js";
export { default as Screenshot } from "./models/Screenshot.js";
export { default as Tag } from "./models/Tag.js";
export { default as TelevisionEpisode } from "./models/TelevisionEpisode.js";
export { default as TelevisionShow } from "./models/TelevisionShow.js";
export { default as Video } from "./models/Video.js";
export { default as VideoTag } from "./models/VideoTag.js";

export { schema };
export default schema;
