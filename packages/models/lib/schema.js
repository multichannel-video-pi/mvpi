import gql from "graphql-tag";
import tools from "graphql-tools";
import _ from "lodash";

import Collection from "./models/Collection.js";
import Movie from "./models/Movie.js";
import QueuedScreenshotSet from "./models/QueuedScreenshotSet.js";
import Schedule from "./models/Schedule.js";
import Screenshot from "./models/Screenshot.js";
import Tag from "./models/Tag.js";
import TelevisionEpisode from "./models/TelevisionEpisode.js";
import TelevisionShow from "./models/TelevisionShow.js";
import Video from "./models/Video.js";
import VideoTag from "./models/VideoTag.js";

import ForeignKey from "./scalars/ForeignKey.js";

const { makeExecutableSchema } = tools;

const resolvers = { ForeignKey };
const typeDefs = gql`
  directive @manyToMany(
    linkTable: String!
    linkCondition: String!
    table: String!
    condition: String!
  ) on FIELD_DEFINITION

  directive @oneToMany(table: String!, condition: String!) on FIELD_DEFINITION
  directive @oneToOne(table: String!, condition: String!) on FIELD_DEFINITION

  enum Sort {
    ASC
    DESC
  }

  scalar ForeignKey

  type Query
  type Mutation
`;

const schema = makeExecutableSchema({
  typeDefs: [
    typeDefs,
    Collection.typeDefs,
    Movie.typeDefs,
    QueuedScreenshotSet.typeDefs,
    Schedule.typeDefs,
    Screenshot.typeDefs,
    Tag.typeDefs,
    TelevisionEpisode.typeDefs,
    TelevisionShow.typeDefs,
    Video.typeDefs,
    VideoTag.typeDefs,
  ],
  resolvers: _.merge(
    resolvers,
    Collection.resolvers,
    Movie.resolvers,
    QueuedScreenshotSet.resolvers,
    Schedule.resolvers,
    Screenshot.resolvers,
    Tag.resolvers,
    TelevisionEpisode.resolvers,
    TelevisionShow.resolvers,
    Video.resolvers,
    VideoTag.resolvers
  ),
});

export default schema;
