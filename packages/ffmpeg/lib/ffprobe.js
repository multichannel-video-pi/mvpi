import ffmpeg from "fluent-ffmpeg";
import { promisify } from "util";

import logger from "./logger.js";

const ffprobe = async (filename) => {
  logger.debug(`executing ffmpeg ffprobe for ${filename}`);
  const command = ffmpeg(filename);
  return promisify(command.ffprobe).call(command);
};

export default ffprobe;
