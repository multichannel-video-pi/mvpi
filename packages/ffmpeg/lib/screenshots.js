import { strict as assert } from "assert";
import ffmpeg from "fluent-ffmpeg";
import path from "path";

import logger from "./logger.js";

const screenshots = async (filename, opts) => {
  logger.debug(`creating ffmpeg screenshots for ${filename}`);
  assert(opts?.count, "opts.count is required");
  assert(opts?.filename, "opts.filename is required");
  assert(opts?.folder, "opts.folder is required");

  const stream = ffmpeg(filename).screenshots(opts);

  const [filenames] = await Promise.all([
    new Promise((resolve, reject) => {
      stream.on("error", reject).on("filenames", resolve);
    }),
    new Promise((resolve, reject) => {
      stream.on("error", reject).on("end", resolve);
    }),
  ]);

  return filenames.map((basename) => path.join(opts.folder, basename));
};

export default screenshots;
