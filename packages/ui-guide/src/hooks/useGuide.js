import { useContext } from "react";
import GuideContext from "../contexts/GuideContext.js";

const useGuide = () => useContext(GuideContext);

export default useGuide;
