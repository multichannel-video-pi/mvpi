/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { BACKGROUND_MASK, WHITE } from "@mvpi/ui-styles";
import { addMinutes } from "date-fns";
import { useMemo } from "react";

import GuideContext from "../contexts/GuideContext.js";
import GuideHeader from "./GuideHeader.js";
import GuideGrid from "./GuideGrid.js";

const Guide = ({ channel, schedules, time }) => {
  const { startTime, endTime } = useMemo(() => {
    const timeDate = new Date(time);
    return {
      startTime: timeDate,
      endTime: addMinutes(timeDate, 150),
    };
  }, [time]);

  const tags = useMemo(() => {
    const sortedTags = [
      ...new Set(schedules?.map((schedule) => schedule.tag)),
    ].sort((a, b) => {
      const nameA = a.name.toLowerCase();
      const nameB = b.name.toLowerCase();
      if (nameA > nameB) return 1;
      if (nameA < nameB) return -1;
      return 0;
    });

    const channelIndex = sortedTags.findIndex((tag) => channel === tag.id);
    if (channelIndex === -1) return sortedTags;
    return [
      ...sortedTags.slice(channelIndex),
      ...sortedTags.slice(0, channelIndex),
    ];
  }, [channel, schedules]);

  return (
    <GuideContext.Provider value={{ channel, endTime, schedules, startTime }}>
      <article
        css={css`
          position: absolute;
          overflow: hidden;
          z-index: 1;
          width: 100vw;
          height: 100vh;
          padding: 1rem;
          background-color: ${BACKGROUND_MASK};
          color: ${WHITE};
        `}
      >
        <GuideHeader />
        <GuideGrid tags={tags} />
      </article>
    </GuideContext.Provider>
  );
};

export default Guide;
