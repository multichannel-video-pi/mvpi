/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { BLUE, BLUE_A25, GREY } from "@mvpi/ui-styles";
import { formatShortTitle } from "@mvpi/util";
import { useMemo } from "react";

import useGuide from "../hooks/useGuide.js";

const ScheduleCell = ({
  cellCount,
  isSelectedChannel,
  isSelectedSchedule,
  schedule,
}) => {
  const { endTime, startTime } = useGuide();

  const title = useMemo(() => formatShortTitle(schedule.video), [schedule]);
  const flexGrow = useMemo(() => {
    const videoDuration = schedule.video.duration * 1000;
    const videoStartTime = schedule.startTime;
    const videoEndTime = videoStartTime + videoDuration;

    const startCrop =
      videoStartTime < startTime ? startTime - videoStartTime : 0;
    const endCrop = videoEndTime > endTime ? videoEndTime - endTime : 0;

    const displayedDuration = videoDuration - startCrop - endCrop;

    return (displayedDuration / (endTime - startTime)) * cellCount;
  }, [cellCount, endTime, schedule, startTime]);

  const backgroundColor = useMemo(() => {
    if (isSelectedSchedule) return BLUE;
    if (isSelectedChannel) return BLUE_A25;
    return null;
  }, [isSelectedChannel, isSelectedSchedule]);

  return (
    <li
      css={css`
        position: relative;
        flex-grow: ${flexGrow};
        border: ${isSelectedChannel
          ? `1px solid ${BLUE}`
          : `1px solid ${GREY}`};
        height: 3rem;
        overflow: hidden;
        ${backgroundColor && `background-color: ${backgroundColor};`}
      `}
    >
      <span
        css={css`
          position: absolute;
          display: block;
          width: 100%;
          max-width: 100%;
          padding: 1rem;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
        `}
      >
        {title}
      </span>
    </li>
  );
};

export default ScheduleCell;
