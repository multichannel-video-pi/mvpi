/** @jsx jsx */
import { css, jsx } from "@emotion/react";

const TimeHeader = () => {
  return (
    <header>
      <h1
        css={css`
          margin: 0 0 2rem;
          font-size: 3rem;
          line-height: 1.125;
        `}
      >
        MVPi Guide
      </h1>
    </header>
  );
};

export default TimeHeader;
