/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { BLUE, BLUE_DARK } from "@mvpi/ui-styles";
import { formatTitle } from "@mvpi/util";
import { format } from "date-fns";
import { useMemo } from "react";

const ScheduleDetails = ({ schedule }) => {
  const title = useMemo(() => formatTitle(schedule.video), [schedule]);

  const timeRange = useMemo(() => {
    const start = format(schedule.startTime, "h:mm a");
    const end = format(
      schedule.startTime + schedule.video.duration * 1000,
      "h:mm a"
    );
    return `${start} - ${end}`;
  }, [schedule]);

  return (
    <div
      css={css`
        position: relative;
        padding: 1rem;
        border: 1px solid ${BLUE};
        background-color: ${BLUE_DARK};
      `}
    >
      <h4
        css={css`
          margin-bottom: 0.5rem;
          font-size: 1.5rem;
        `}
      >
        {title}
      </h4>
      <span>{timeRange}</span>
      {!!schedule.video.overview && (
        <p
          css={css`
            margin-top: 0.5rem;
            line-height: 1.5;
          `}
        >
          {schedule.video.overview}
        </p>
      )}
    </div>
  );
};

export default ScheduleDetails;
