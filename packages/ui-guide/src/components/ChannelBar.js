/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { Fragment, useMemo } from "react";

import useGuide from "../hooks/useGuide.js";
import ScheduleCell from "./ScheduleCell.js";
import ScheduleDetails from "./ScheduleDetails.js";

const ChannelBar = ({ isSelectedChannel, row, tag }) => {
  const { endTime, schedules, startTime } = useGuide();

  const tagSchedules = useMemo(
    () =>
      schedules.filter((schedule) => {
        if (schedule.tag.id !== tag.id) return false;
        if (schedule.startTime >= endTime) return false;
        if (schedule.startTime + schedule.video.duration * 1000 <= startTime)
          return false;
        return true;
      }),
    [endTime, schedules, startTime, tag]
  );

  return (
    <Fragment>
      <h3
        css={css`
          grid-column: 1;
          grid-row: ${row};
          padding: 1rem;
          text-align: right;
          font-weight: 300;
        `}
      >
        {tag.name}
      </h3>
      <div
        css={css`
          grid-column: 2 / 7;
          grid-row: ${row};
        `}
      >
        <ul
          css={css`
            display: flex;
          `}
        >
          {tagSchedules.map((schedule, index) => (
            <ScheduleCell
              cellCount={tagSchedules.length}
              isSelectedChannel={isSelectedChannel}
              isSelectedSchedule={isSelectedChannel && index === 0}
              key={schedule.id}
              schedule={schedule}
            />
          ))}
        </ul>
        {isSelectedChannel && <ScheduleDetails schedule={tagSchedules[0]} />}
      </div>
    </Fragment>
  );
};

export default ChannelBar;
