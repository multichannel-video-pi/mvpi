/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { GREY } from "@mvpi/ui-styles";
import { addHours, format } from "date-fns";
import { useMemo } from "react";

import useGuide from "../hooks/useGuide.js";
import ChannelBar from "./ChannelBar.js";
import GuideCurrentTime from "./GuideCurrentTime.js";

const GuideGrid = ({ tags }) => {
  const { channel, startTime } = useGuide();

  const {
    formattedStartDate,
    formattedStartTime,
    formattedMiddleTime,
    formattedEndTime,
  } = useMemo(() => {
    return {
      formattedStartDate: format(startTime, "E, MMM dd"),
      formattedStartTime: format(startTime, "h:mm a"),
      formattedMiddleTime: format(addHours(startTime, 1), "h:mm a"),
      formattedEndTime: format(addHours(startTime, 2), "h:mm a"),
    };
  }, [startTime]);

  return (
    <section
      css={css`
        display: grid;
        grid-template-columns: auto 1fr 1fr 1fr 1fr 1fr;
      `}
    >
      <h2
        css={css`
          grid-column: 1;
          grid-row: 1;
          padding: 1rem;
          text-align: right;
          font-weight: 700;
        `}
      >
        {formattedStartDate}
      </h2>
      <span
        css={css`
          grid-column: 2 / 4;
          grid-row: 1;
          border-left: 1px solid ${GREY};
          padding: 1rem;
          font-weight: 300;
        `}
      >
        {formattedStartTime}
      </span>
      <span
        css={css`
          grid-column: 4 / 6;
          grid-row: 1;
          border-left: 1px solid ${GREY};
          padding: 1rem;
          font-weight: 300;
        `}
      >
        {formattedMiddleTime}
      </span>
      <span
        css={css`
          grid-column: 6 / 7;
          grid-row: 1;
          border-left: 1px solid ${GREY};
          padding: 1rem;
          font-weight: 300;
        `}
      >
        {formattedEndTime}
      </span>
      <GuideCurrentTime tagCount={tags.length} />
      {tags.map((tag, index) => (
        <ChannelBar
          isSelectedChannel={channel === tag.id}
          key={tag.id}
          row={index + 2}
          tag={tag}
        />
      ))}
    </section>
  );
};

export default GuideGrid;
