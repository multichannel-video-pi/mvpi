/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { BLUE } from "@mvpi/ui-styles";
import { useMemo } from "react";

import useGuide from "../hooks/useGuide.js";

const GuideCurrentTime = ({ tagCount }) => {
  const { endTime, startTime } = useGuide();
  const now = Date.now();

  const leftPercentage = useMemo(() => {
    if (now > endTime || now < startTime) return null;
    const total = endTime - startTime;
    const difference = now - startTime;
    const percentage = (difference / total) * 100;
    return `${percentage}%`;
  }, [endTime, now, startTime]);

  if (!leftPercentage) return null;

  const row = `2 / ${tagCount + 2}`;

  return (
    <div
      css={css`
        position: relative;
        grid-column: 2 / 7;
        grid-row: ${row};
      `}
    >
      <div
        css={css`
          position: absolute;
          left: ${leftPercentage};
          background-color: ${BLUE};
          width: 2px;
          height: 100%;
        `}
      ></div>
    </div>
  );
};

export default GuideCurrentTime;
