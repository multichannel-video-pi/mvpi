/**
 * sequential executer of an async function on every item in an array
 *
 * @param {Array} array - array of items
 * @param {function} asyncFn - asnychronous function that will be called for each item in array
 * @return {Array} result of asnychronous function on each item in array
 */
const serialAsyncMap = async (array, asyncFn) => {
  const result = [];
  for (const item of array) {
    result.push(await asyncFn(item));
  }
  return result;
};

export default serialAsyncMap;
