import path from "path";

const formatTitle = (video) => {
  const { movie, televisionEpisode } = video;
  const collection = movie && movie.collection;
  const televisionShow = televisionEpisode && televisionEpisode.televisionShow;

  if (!video.title)
    return path.basename(video.filename, path.extname(video.filename));

  if (televisionShow) {
    if (!televisionEpisode.seasonNumber || !televisionEpisode.episodeNumber)
      return `${televisionShow.name} - ${video.title}`;

    const season =
      `${televisionEpisode.seasonNumber}`.length === 1
        ? `0${televisionEpisode.seasonNumber}`
        : `${televisionEpisode.seasonNumber}`;
    const episode =
      `${televisionEpisode.episodeNumber}`.length === 1
        ? `0${televisionEpisode.episodeNumber}`
        : `${televisionEpisode.episodeNumber}`;

    return `${televisionShow.name} - [S${season}E${episode}] ${video.title}`;
  }

  if (collection) {
    return `${video.title} [${collection.name}]`;
  }

  return video.title;
};

export default formatTitle;
