const url = ([illegalString, ...strings], config, ...keys) => {
  if (typeof config !== "object")
    throw new Error("first key must be service configuration object");
  if (!config.hostname)
    throw new Error("service configuration object must include hostname");
  if (illegalString)
    throw new Error("string must not precede service configuration object");

  const hostname = config.hostname;
  const port = config.port || 80;
  const prefix = config.prefix || "";
  const protocol = config.protocol ? config.protocol.replace(/:$/, "") : "http";

  const route = strings.map((str, i) => `${str}${keys[i] || ""}`).join("");
  if (route && !/^\//.test(route))
    throw new Error(`route must start with "/": ${route}`);

  if (port === 80) return `${protocol}://${hostname}${prefix}${route}`;
  return `${protocol}://${hostname}:${port}${prefix}${route}`;
};

export default url;
