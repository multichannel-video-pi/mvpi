export const KEY_PREFIX = "MVPi/";

const usedKeys = [];

const key = (strings = [], ...keys) => {
  const interpolatedKey = strings
    .map((str, i) => `${str}${keys[i] || ""}`)
    .join("");
  if (usedKeys.includes(interpolatedKey))
    throw new Error(`key "${interpolatedKey}" is used more than once`);
  usedKeys.push(interpolatedKey);
  return `${KEY_PREFIX}${interpolatedKey}`;
};

export default key;
