const EMPTY_VALUES = [undefined, null, ""];

/**
 * convert an expanded object into a flat object
 *
 * @param {object} object - expanded object
 * @param {object} [opts] - options
 * @param {string} [opts.delimiter="__"] - delimter used in flattened object keys
 * @param {boolean} [opts.filterEmptyValues=false] - remove empty object values (undefined, null, "") from object
 * @param {boolean} [opts.prefix=""] - object key prefix
 * @param {boolean} [opts.stringifyValues=false] - stringify object values using JSON.stringify()
 * @return {object} expanded object
 */
const flatten = (
  object,
  {
    delimiter = "__",
    filterEmptyValues = false,
    prefix = "",
    stringifyValues = false,
  } = {}
) => {
  const objectEntries = filterEmptyValues
    ? Object.entries(object).filter(
        ([, value]) => !EMPTY_VALUES.includes(value)
      )
    : Object.entries(object);

  return objectEntries.reduce((flatObject, [key, value]) => {
    const prefixedKey = prefix ? `${prefix}${delimiter}${key}` : key;
    if (value instanceof Object && value.constructor === Object)
      return {
        ...flatObject,
        ...flatten(value, {
          delimiter,
          filterEmptyValues,
          prefix: prefixedKey,
          stringifyValues,
        }),
      };
    if (!stringifyValues) return { ...flatObject, [prefixedKey]: value };
    return { ...flatObject, [prefixedKey]: JSON.stringify(value) };
  }, {});
};

export default flatten;
