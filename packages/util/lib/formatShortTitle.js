import path from "path";

const formatShortTitle = (video) => {
  const { televisionEpisode } = video;
  const televisionShow = televisionEpisode && televisionEpisode.televisionShow;

  if (!video.title)
    return path.basename(video.filename, path.extname(video.filename));

  if (televisionShow) return televisionShow.name;

  return video.title;
};

export default formatShortTitle;
