const getOffsetDate = (offset) =>
  offset ? new Date(Date.now() + offset) : new Date();

export default getOffsetDate;
