const lowerCaseFirst = (str) => `${str.charAt(0).toLowerCase()}${str.slice(1)}`;

export default lowerCaseFirst;
