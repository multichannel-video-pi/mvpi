const EMPTY_VALUES = [undefined, null, ""];

/**
 * convert a flat object into an expanded object
 *
 * @param {object} flatObject - flat object
 * @param {object} [opts] - options
 * @param {string} [opts.delimiter="__"] - delimter used in flattened object keys
 * @param {boolean} [opts.filterEmptyValues=false] - remove empty object values (undefined, null, "") from object
 * @param {function} [opts.valueParser] - function to parse values (JSON.parse or similar)
 * @return {object} expanded object
 */
const unflatten = (
  flatObject,
  { delimiter = "__", filterEmptyValues = false, valueParser } = {}
) => {
  const flatObjectEntries = filterEmptyValues
    ? Object.entries(flatObject).filter(
        ([, value]) => !EMPTY_VALUES.includes(value)
      )
    : Object.entries(flatObject);

  const delimiterShiftedObject = flatObjectEntries.reduce(
    (obj, [flatKey, value]) => {
      const [key, ...nestedKeys] = flatKey.split(delimiter);
      if (!nestedKeys.length) return { ...obj, [key]: value };
      return {
        ...obj,
        [key]: { ...obj[key], [nestedKeys.join(delimiter)]: value },
      };
    },
    {}
  );

  return Object.fromEntries(
    Object.entries(delimiterShiftedObject).map(([key, value]) => {
      if (value instanceof Object && value.constructor === Object)
        return [
          key,
          unflatten(value, { delimiter, filterEmptyValues, valueParser }),
        ];
      if (typeof valueParser !== "function") return [key, value];
      return [key, valueParser(value)];
    })
  );
};

export default unflatten;
