const parseValue = (value, errorValue = value) => {
  try {
    return JSON.parse(value);
  } catch {
    return errorValue;
  }
};

export default parseValue;
