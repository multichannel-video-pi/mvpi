import path from "path";

const sanitizeFilename = (filename) =>
  path.basename(filename, path.extname(filename));

export default sanitizeFilename;
