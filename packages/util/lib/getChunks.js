const getChunks = (sourceArray = [], chunkSize = 25) => {
  return sourceArray.reduce((chunks, sourceArrayItem, sourceArrayIndex) => {
    const chunkArrayIndex = Math.floor(sourceArrayIndex / chunkSize);
    chunks[chunkArrayIndex] = Array.isArray(chunks[chunkArrayIndex])
      ? [...chunks[chunkArrayIndex], sourceArrayItem]
      : [sourceArrayItem];
    return chunks;
  }, []);
};

export default getChunks;
