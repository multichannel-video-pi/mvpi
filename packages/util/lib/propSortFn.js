const propSortFn = (prop, type = String) => {
  switch (type) {
    case Date:
    case Number:
      return (a, b) => {
        const propA = a[prop] || 0;
        const propB = b[prop] || 0;
        if (propA > propB) return 1;
        if (propA < propB) return -1;
        return 0;
      };
    case String:
      return (a, b) => {
        const propA = (a[prop] || "").toLowerCase();
        const propB = (b[prop] || "").toLowerCase();
        if (propA > propB) return 1;
        if (propA < propB) return -1;
        return 0;
      };
    default:
      return () => 0;
  }
};

export default propSortFn;
