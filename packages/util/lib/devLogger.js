export const noopLogger = Object.fromEntries(
  Object.entries(console)
    .filter(([, val]) => typeof val === "function")
    .map(([key]) => [key, () => null])
);

const logger = process.env.NODE_ENV === "production" ? noopLogger : console;

export default logger;
