/**
 * get the sum of an array of numbers (faster than using [].reduce())
 *
 * @param {number[]} values - array of numbers
 * @return {object} sum of values
 */
const sum = (values) => {
  let total = 0;
  let i = values.length;
  while (i--) total += values[i];
  return total;
};

export default sum;
