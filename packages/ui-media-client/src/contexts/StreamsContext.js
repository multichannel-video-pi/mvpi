import { createContext } from "react";

const StreamsContext = createContext();

export default StreamsContext;
