import { useStoredState, useVisibilityState } from "@mvpi/ui-hooks";
import { key } from "@mvpi/util";
import React, { useEffect, useState } from "react";

import {
  RemoteContextProvider,
  useRemoteControlDispatch,
} from "@mvpi/ui-remote-client";

import StreamsContext from "../contexts/StreamsContext.js";
import useChannelState from "../hooks/useChannelState.js";
import useGuideTimeState from "../hooks/useGuideTimeState.js";
import useStreamsState from "../hooks/useStreamsState.js";
import useVolumeState from "../hooks/useVolumeState.js";

const STREAMS_GUIDE_CHANNEL = key`STREAMS_GUIDE_CHANNEL`;
const STREAMS_PLAYER_CHANNEL = key`STREAMS_PLAYER_CHANNEL`;
const STREAMS_PLAYER_MUTE = key`STREAMS_PLAYER_MUTE`;
const STREAMS_PLAYER_VOLUME = key`STREAMS_PLAYER_VOLUME`;

const StreamsContextProvider = ({ children, config }) => {
  const [mute, setMute] = useStoredState(STREAMS_PLAYER_MUTE, false);
  const [offset, setOffset] = useState(0);
  const [power, setPower] = useState(false);

  const {
    channel: guideChannel,
    setChannel: setGuideChannel,
    setChannelBack: setGuideChannelBack,
    setChannelForward: setGuideChannelForward,
  } = useChannelState(STREAMS_GUIDE_CHANNEL);

  const {
    channel: playerChannel,
    setChannel: setPlayerChannel,
    setChannelBack: setPlayerChannelBack,
    setChannelForward: setPlayerChannelForward,
  } = useChannelState(STREAMS_PLAYER_CHANNEL);

  const [visible, setVisibility] = useVisibilityState({
    guide: false,
    infoBar: false,
    remote: false,
  });

  const {
    guideTime,
    setGuideTime,
    setGuideTimeBack,
    setGuideTimeForward,
    setGuideTimeForChannel,
  } = useGuideTimeState({ guideChannel });

  const {
    player,
    playerError,
    playerRef,
    selected,
    startTime,
    streamVideo,
    streamScheduledVideo,
  } = useStreamsState({ config, playerChannel, setVisibility });

  const { setVolume, setVolumeDown, setVolumeUp, volume } = useVolumeState(
    STREAMS_PLAYER_VOLUME
  );

  const dispatch = useRemoteControlDispatch({
    setGuideChannelBack,
    setGuideChannelForward,
    setGuideTimeBack,
    setGuideTimeForward,
    setMute,
    setPlayerChannelBack,
    setPlayerChannelForward,
    setPower,
    setVisibility,
    setVolumeDown,
    setVolumeUp,
    visible,
  });

  useEffect(() => {
    if (player) player.volume = mute ? 0 : volume;
  }, [mute, player, volume]);

  useEffect(() => {
    if (!visible.guide && playerChannel) {
      setGuideChannel(playerChannel);
      setGuideTimeForChannel(playerChannel);
    }
  }, [playerChannel, setGuideChannel, setGuideTimeForChannel, visible.guide]);

  return (
    <RemoteContextProvider config={config} dispatch={dispatch}>
      <StreamsContext.Provider
        value={{
          guideChannel,
          guideTime,
          mute,
          offset,
          player,
          playerChannel,
          playerError,
          playerRef,
          power,
          selected,
          setGuideChannel,
          setGuideChannelBack,
          setGuideChannelForward,
          setGuideTime,
          setGuideTimeBack,
          setGuideTimeForward,
          setMute,
          setOffset,
          setPlayerChannel,
          setPlayerChannelBack,
          setPlayerChannelForward,
          setPower,
          setVisibility,
          setVolume,
          setVolumeDown,
          setVolumeUp,
          startTime,
          streamScheduledVideo,
          streamVideo,
          visible,
          volume,
        }}
      >
        {children}
      </StreamsContext.Provider>
    </RemoteContextProvider>
  );
};

export default StreamsContextProvider;
