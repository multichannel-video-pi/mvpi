import React from "react";

import {
  MetadataApolloProvider,
  SchedulesContextProvider,
} from "@mvpi/ui-metadata-client";

import StreamsContextProvider from "./StreamsContextProvider.js";

const StreamsProvider = ({ children, config }) => {
  return (
    <MetadataApolloProvider config={config}>
      <SchedulesContextProvider>
        <StreamsContextProvider config={config}>
          {children}
        </StreamsContextProvider>
      </SchedulesContextProvider>
    </MetadataApolloProvider>
  );
};

export default StreamsProvider;
