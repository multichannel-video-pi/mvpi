import { useSchedules } from "@mvpi/ui-metadata-client";
import { useCallback, useState } from "react";

const useGuideTimeState = ({ guideChannel }) => {
  const { schedules } = useSchedules();
  const [guideTime, setGuideTime] = useState(Date.now());

  const setGuideTimeBack = useCallback(() => {
    return setGuideTime((prevGuideTime) => {
      if (!guideChannel) return prevGuideTime;
      const prevScheduleIndex = schedules?.findIndex(
        (schedule) =>
          schedule.tag.id === guideChannel &&
          schedule.startTime + schedule.video.duration * 1000 > prevGuideTime
      );
      if (prevScheduleIndex === -1) return prevGuideTime;
      return schedules[prevScheduleIndex - 1]?.startTime || prevGuideTime;
    });
  }, [guideChannel, schedules]);

  const setGuideTimeForward = useCallback(() => {
    return setGuideTime((prevGuideTime) => {
      if (!guideChannel) return prevGuideTime;
      const prevScheduleIndex = schedules?.findIndex(
        (schedule) =>
          schedule.tag.id === guideChannel &&
          schedule.startTime + schedule.video.duration * 1000 > prevGuideTime
      );
      if (prevScheduleIndex === -1) return prevGuideTime;
      return schedules[prevScheduleIndex + 1]?.startTime || prevGuideTime;
    });
  }, [guideChannel, schedules]);

  const setGuideTimeForChannel = useCallback(
    (channel) => {
      const now = Date.now();
      if (!channel) return now;
      const channelSchedule = schedules?.find(
        (schedule) =>
          schedule.tag.id === channel &&
          schedule.startTime + schedule.video.duration * 1000 > now
      );
      if (!channelSchedule) return now;

      const time = new Date();
      const year = time.getFullYear();
      const month = time.getMonth();
      const day = time.getDate();
      const hours = time.getHours();
      const minutes = time.getMinutes() >= 30 ? 30 : 0;
      const roundedTime = new Date(year, month, day, hours, minutes).getTime();

      return setGuideTime(
        channelSchedule.startTime > roundedTime
          ? channelSchedule.startTime
          : roundedTime
      );
    },
    [schedules]
  );

  return {
    guideTime,
    setGuideTime,
    setGuideTimeBack,
    setGuideTimeForward,
    setGuideTimeForChannel,
  };
};

export default useGuideTimeState;
