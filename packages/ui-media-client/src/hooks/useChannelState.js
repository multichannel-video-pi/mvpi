import { useStoredState } from "@mvpi/ui-hooks";
import { useSchedules } from "@mvpi/ui-metadata-client";
import { useCallback, useEffect } from "react";

const useChannelState = (key) => {
  const { channels } = useSchedules();
  const [channel, setChannel] = useStoredState(key, null);

  const setChannelBack = useCallback(
    () =>
      setChannel((prevChannel) => {
        if (!channels.length) return null;
        const prevIndex = channels.findIndex(({ id }) => prevChannel === id);
        const index = prevIndex <= 0 ? channels.length - 1 : prevIndex - 1;
        return channels[index].id;
      }),
    [channels, setChannel]
  );

  const setChannelForward = useCallback(
    () =>
      setChannel((prevChannel) => {
        if (!channels.length) return null;
        const prevIndex = channels.findIndex(({ id }) => prevChannel === id);
        const index = prevIndex === channels.length - 1 ? 0 : prevIndex + 1;
        return channels[index].id;
      }),
    [channels, setChannel]
  );

  useEffect(() => {
    if (!channel && channels.length) setChannel(channels[0].id);
  }, [channel, channels, setChannel]);

  return { channel, setChannel, setChannelBack, setChannelForward };
};

export default useChannelState;
