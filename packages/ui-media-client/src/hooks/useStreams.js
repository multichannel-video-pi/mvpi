import { useContext } from "react";
import StreamsContext from "../contexts/StreamsContext.js";

const useStreams = () => useContext(StreamsContext);

export default useStreams;
