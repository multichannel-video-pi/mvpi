import { useSchedules } from "@mvpi/ui-metadata-client";
import { url } from "@mvpi/util";
import { useCallback, useEffect, useState } from "react";

import usePlayerRef from "./usePlayerRef.js";

const useStreamsState = ({ config, playerChannel, setVisibility }) => {
  const { schedules } = useSchedules();
  const [playerError, setPlayerError] = useState(null);
  const [playerRef, player] = usePlayerRef();
  const [selected, setSelected] = useState(null);
  const [startTime, setStartTime] = useState(null);

  const streamVideo = useCallback(
    (videoId, videoStartTime) => {
      if (!player) return null;
      const now = Date.now();
      const currentTime = videoStartTime ? (now - videoStartTime) / 1000 : 0;

      setSelected(videoId);
      setStartTime(videoStartTime || now);
      setVisibility({ view: "infoBar", visible: true });

      player.src = url`${config.media.service}/videos/${videoId}`;
      player.currentTime = currentTime;
      return player.play().catch((err) => setPlayerError(err));
    },
    [config, player, setVisibility]
  );

  const streamScheduledVideo = useCallback(() => {
    if (!playerChannel) return null;
    const now = Date.now();
    const next = schedules?.find(
      (schedule) =>
        schedule.tag.id === playerChannel &&
        schedule.startTime + schedule.video.duration * 1000 > now
    );
    if (!next) return null;
    return streamVideo(next.video.id, next.startTime);
  }, [playerChannel, schedules, streamVideo]);

  useEffect(() => {
    if (!player) return () => null;
    const el = player;
    const handlePlayerEnded = () => streamScheduledVideo();
    const handlePlayerError = () => setPlayerError(new Error("video error"));
    el.addEventListener("ended", handlePlayerEnded, true);
    el.addEventListener("error", handlePlayerError, true);

    return () => {
      el.removeEventListener("ended", handlePlayerEnded, true);
      el.removeEventListener("error", handlePlayerError, true);
    };
  }, [player, streamScheduledVideo]);

  useEffect(() => {
    if (player && playerChannel) streamScheduledVideo();
  }, [player, playerChannel, streamScheduledVideo]);

  return {
    player,
    playerError,
    playerRef,
    selected,
    startTime,
    streamVideo,
    streamScheduledVideo,
  };
};

export default useStreamsState;
