import { useStoredState } from "@mvpi/ui-hooks";
import { useCallback } from "react";

const useVolumeState = (key) => {
  const [volume, setVolume] = useStoredState(key, 1);

  const setVolumeDown = useCallback(
    () =>
      setVolume((prevVolume) =>
        prevVolume <= 0 ? prevVolume : (prevVolume * 10 - 1) / 10
      ),
    [setVolume]
  );

  const setVolumeUp = useCallback(
    () =>
      setVolume((prevVolume) =>
        prevVolume >= 1 ? prevVolume : (prevVolume * 10 + 1) / 10
      ),
    [setVolume]
  );

  return { setVolume, setVolumeDown, setVolumeUp, volume };
};

export default useVolumeState;
