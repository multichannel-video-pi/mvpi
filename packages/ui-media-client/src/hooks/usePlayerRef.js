import { useCallback, useEffect, useState } from "react";

const usePlayerRef = () => {
  const [current, setCurrent] = useState(null);

  const playerRef = useCallback((node) => {
    setCurrent(node);
  }, []);

  useEffect(() => {
    if (!current) return () => null;

    // remove src attribute and load() empty <video> to prevent memory leak
    // see: https://html.spec.whatwg.org/multipage/media.html#best-practices-for-authors-using-media-elements
    return () => {
      current.removeAttribute("src");
      current.load();
    };
  }, [current]);

  return [playerRef, current];
};

export default usePlayerRef;
