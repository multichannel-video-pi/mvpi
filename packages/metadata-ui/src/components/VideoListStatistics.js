/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { WHITE_TER, tablet } from "@mvpi/ui-styles";

import {
  DescriptionList,
  FormattedDuration,
  FormattedSize,
  Grid,
  GridItem,
  Title,
} from "@mvpi/ui-helpers";

import {
  useVideoListCount,
  useVideoListDuration,
  useVideoListSize,
} from "@mvpi/ui-hooks";

const VideoListStatistics = ({ videos = [] }) => {
  const count = useVideoListCount(videos);
  const duration = useVideoListDuration(videos);
  const size = useVideoListSize(videos);

  return (
    <div
      css={css`
        margin: 0 0 1rem;
        padding: 1rem;
        background-color: ${WHITE_TER};

        ${tablet} {
          padding: 1rem 2rem;
        }
      `}
    >
      <Title type="2" size="6">
        Statistics
      </Title>
      <Grid
        columnGap="0.25rem"
        template={`
          [count-start] "count" auto [count-end]
          [duration-start] "duration" auto [duration-end]
          [size-start] "size" auto [size-end]
          / auto
        `}
        templateTablet={`
          [count-size-start] "count size" auto [count-size-end]
          [duration-start] "duration duration" auto [duration-end]
          / 1fr 1fr
        `}
        templateDesktop={`
          [count-size-duration-start] "count size duration" auto [count-size-duration-end]
          / 1fr 1fr 1fr
        `}
      >
        <GridItem area="count">
          <DescriptionList
            entries={[
              ["Videos", count.video],
              ["Tags", count.tag],
              ["Television Shows", count.televisionShow],
              ["Collections", count.collection],
            ]}
          />
        </GridItem>
        <GridItem area="size">
          <DescriptionList
            entries={[
              ["Total Size", <FormattedSize size={size.total} />],
              ["Average Size", <FormattedSize size={size.average} />],
              ["Median Size", <FormattedSize size={size.median} />],
            ]}
          />
        </GridItem>
        <GridItem area="duration">
          <DescriptionList
            entries={[
              [
                "Total Duration",
                <FormattedDuration duration={duration.total} />,
              ],
              [
                "Average Duration",
                <FormattedDuration duration={duration.average} />,
              ],
              [
                "Median Duration",
                <FormattedDuration duration={duration.median} />,
              ],
            ]}
          />
        </GridItem>
      </Grid>
    </div>
  );
};

export default VideoListStatistics;
