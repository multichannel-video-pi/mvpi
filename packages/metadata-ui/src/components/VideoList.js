/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useVideos } from "@mvpi/ui-metadata-client";
import { GREY_LIGHTER } from "@mvpi/ui-styles";
import { formatTitle, sanitizeFilename } from "@mvpi/util";
import { useFormikContext } from "formik";
import { useMemo } from "react";

import { Grid, GridItem, Input, Label } from "@mvpi/ui-helpers";

import VideoListCollectionSelect from "./VideoListCollectionSelect.js";
import VideoListItem from "./VideoListItem.js";
import VideoListRatingSelect from "./VideoListRatingSelect.js";
import VideoListSelectButtons from "./VideoListSelectButtons.js";
import VideoListStatistics from "./VideoListStatistics.js";
import VideoListTagSelect from "./VideoListTagSelect.js";
import VideoListTelevisionShowSelect from "./VideoListTelevisionShowSelect.js";

const VideoList = () => {
  const { values } = useFormikContext();
  const { videos } = useVideos();

  const filteredVideos = useMemo(() => {
    if (!Array.isArray(videos)) return [];
    const search = values.search?.toLowerCase() || "";

    return videos
      .filter((video) => {
        // always show selected
        if (values.selected.includes(video.id)) return true;

        const filename = sanitizeFilename(video.filename).toLowerCase();
        const collection = video.movie?.collection?.name.toLowerCase() || "";
        const show =
          video.televisionEpisode?.televisionShow?.name.toLowerCase() || "";
        const title = video.title?.toLowerCase() || "";
        const tags = video.tags?.map((tag) => tag.name.toLowerCase()) || [];

        if (filename.includes(search)) return true;
        if (collection.includes(search)) return true;
        if (show.includes(search)) return true;
        if (title.includes(search)) return true;
        if (tags.some((tag) => tag.includes(search))) return true;

        return false;
      })
      .map((video) => [video, formatTitle(video).toLowerCase()])
      .sort(([, titleA], [, titleB]) => {
        if (titleA > titleB) return 1;
        if (titleA < titleB) return -1;
        return 0;
      })
      .map(([video]) => video);
  }, [values.search, values.selected, videos]);

  return (
    <Grid
      columnGap="0.25rem"
      rowGap="1rem"
      template={`
        [statistics-start] "statistics" auto [statistics-end]
        [rating-start] "rating" auto [rating-end]
        [tag-start] "tag" auto [tag-end]
        [show-start] "show" auto [show-end]
        [collection-start] "collection" auto [collection-end]
        [search-start] "search" auto [search-end]
        [select-start] "select" auto [select-end]
        [content-start] "list" auto [content-end]
        / auto
      `}
      templateTablet={`
        [statistics-start] "statistics statistics statistics statistics statistics statistics" auto [statistics-end]
        [rating-tag-start] "rating rating rating tag tag tag" auto [rating-tag-end]
        [show-collection-start] "show show show collection collection collection" auto [show-collection-end]
        [search-select-start] "search search search search select select" auto [search-select-end]
        [content-start] "list list list list list list" auto [content-end]
        / 1fr 1fr 1fr 1fr 1fr 1fr
      `}
      templateDesktop={`
        [statistics-start] "statistics statistics statistics statistics" auto [statistics-end]
        [rating-tag-show-collection-start] "rating tag show collection" auto [rating-tag-show-collection-end]
        [search-select-start] "search search search select" auto [search-select-end]
        [content-start] "list list list list" auto [content-end]
        / 1fr 1fr 1fr 1fr
      `}
    >
      <GridItem area="statistics">
        <VideoListStatistics videos={filteredVideos} />
      </GridItem>

      <GridItem area="rating">
        <VideoListRatingSelect />
      </GridItem>

      <GridItem area="tag">
        <VideoListTagSelect />
      </GridItem>

      <GridItem area="show">
        <VideoListTelevisionShowSelect />
      </GridItem>

      <GridItem area="collection">
        <VideoListCollectionSelect />
      </GridItem>

      <GridItem area="search">
        <Label>
          Filter Videos
          <Input
            css={css`
              margin-bottom: 0;
            `}
            name="search"
            placeholder="Search Videos..."
            type="text"
          />
        </Label>
      </GridItem>

      <GridItem align="end" area="select">
        <VideoListSelectButtons videos={filteredVideos} />
      </GridItem>

      <GridItem area="list">
        <ul
          css={css`
            border-top: 1px solid ${GREY_LIGHTER};
            padding: 1rem 0;
          `}
        >
          {filteredVideos.map((video) => (
            <VideoListItem key={video.id} video={video} />
          ))}
        </ul>
      </GridItem>
    </Grid>
  );
};

export default VideoList;
