/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Paragraph, Section, Title } from "@mvpi/ui-helpers";
import { devLogger as logger } from "@mvpi/util";
import { useHistory } from "react-router-dom";

const DELETE_COLLECTION = gql`
  mutation deleteCollection($id: ID!) {
    deleteCollection(id: $id) {
      id
      name
      overview
    }
  }
`;

const CollectionRemoval = ({ collection }) => {
  const history = useHistory();

  const [deleteCollection, { error, loading }] = useMutation(
    DELETE_COLLECTION,
    {
      update: (cache, { data }) => {
        cache.evict({
          id: cache.identify(data.deleteCollection),
        });
      },
    }
  );

  return (
    <Section>
      <Title type="2" size="6">
        Removal
      </Title>
      {error && (
        <Paragraph isDanger>
          Failed to remove collection: {error.message}
        </Paragraph>
      )}
      <Paragraph isDanger>
        Removal is permanent, and cannot be reversed.
      </Paragraph>
      <Paragraph>
        If you delete this collection, it will destroy all associations it has
        with other records.
      </Paragraph>
      <div
        css={css`
          text-align: right;
        `}
      >
        <Button
          isDanger
          isLoading={loading}
          onClick={async () => {
            try {
              await deleteCollection({
                variables: {
                  id: collection.id,
                },
              });
              history.push("/");
            } catch (err) {
              logger.error(err);
            }
          }}
        >
          Delete Collection
        </Button>
      </div>
    </Section>
  );
};

export default CollectionRemoval;
