/** @jsxImportSource @emotion/react */
import { useQuery, gql } from "@apollo/client";
import { css } from "@emotion/react";
import config from "@mvpi/ui-config";
import { formatTitle, url } from "@mvpi/util";
import { useMemo } from "react";
import { Redirect, Route, Switch, useParams } from "react-router-dom";

import {
  DescriptionList,
  ImageGrid,
  FormattedDuration,
  FormattedSize,
  NoData,
  Section,
  Tab,
  Tabs,
  Title,
} from "@mvpi/ui-helpers";

import MovieForm from "./MovieForm.js";
import TelevisionEpisodeForm from "./TelevisionEpisodeForm.js";
import VideoForm from "./VideoForm.js";

const FETCH_VIDEO = gql`
  query fetchVideo($id: ID!) {
    video(id: $id) {
      id
      bitrate
      duration
      filename
      isActive
      overview
      rating
      releaseDate
      size
      title
      voteAverage
      voteCount

      movie {
        id
        backdropPath
        chronology
        posterPath
        tagline
        tmdbId

        collection {
          id
          backdropPath
          name
          overview
          posterPath
          tmdbId
        }
      }

      tags {
        id
        name
        overview
      }

      televisionEpisode {
        id
        episodeNumber
        seasonNumber
        stillPath

        televisionShow {
          id
          backdropPath
          firstAirDate
          lastAirDate
          name
          overview
          posterPath
          rating
          tagline
          tmdbId
          voteAverage
          voteCount
        }
      }
    }
  }
`;

const VideoDetails = () => {
  const { id } = useParams();

  const { data, error, loading, refetch } = useQuery(FETCH_VIDEO, {
    pollInterval: 300000,
    variables: { id },
  });

  const video = data?.video;
  const screenshots = useMemo(
    () =>
      Array.isArray(video?.screenshots)
        ? [...video.screenshots].map(
            (screenshot) =>
              url`${config.media.service}/screenshots/${screenshot.id}`
          )
        : [],
    [video]
  );

  if (!video)
    return (
      <NoData
        error={error}
        loading={loading}
        loadingText="Fetching Video..."
        reload={() => refetch()}
      />
    );

  const displayTitle = formatTitle(video);

  return (
    <article>
      <Section>
        <Title size="5">{displayTitle}</Title>
        <Tabs>
          <Tab to={`/videos/${video.id}`}>Video</Tab>
          <Tab to={`/videos/${video.id}/television-episode`}>
            Television Episode
          </Tab>
          <Tab to={`/videos/${video.id}/movie`}>Movie</Tab>
        </Tabs>
      </Section>
      <Section>
        <Switch>
          <Route exact path={`/videos/${video.id}`}>
            <VideoForm video={video} />
          </Route>
          <Route exact path={`/videos/${video.id}/television-episode`}>
            <TelevisionEpisodeForm video={video} />
          </Route>
          <Route exact path={`/videos/${video.id}/movie`}>
            <MovieForm video={video} />
          </Route>
          <Redirect to={`/videos/${video.id}`} />
        </Switch>
      </Section>
      <Section>
        <Title type="2" size="6">
          Statistics
        </Title>
        <DescriptionList
          entries={[
            ["Filename", video.filename],
            ["Duration", <FormattedDuration duration={video.duration} />],
            ["Size", <FormattedSize size={video.size} />],
            ["Bitrate", video.bitrate],
          ]}
        />
      </Section>
      <Section>
        <Title type="2" size="6">
          Video
        </Title>
        <video
          controls
          css={css`
            width: 100%;
          `}
        >
          <source
            src={url`${config.media.service}/videos/${video.id}`}
            type="video/mp4"
          />
        </video>
      </Section>
      <Section>
        <Title type="2" size="6">
          Screenshots
        </Title>
        <ImageGrid
          altText={`${displayTitle} Screenshots`}
          images={screenshots}
        />
      </Section>
    </article>
  );
};

export default VideoDetails;
