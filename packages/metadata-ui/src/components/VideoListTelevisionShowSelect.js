/** @jsxImportSource @emotion/react */
import { useMutation, gql } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Label, LinkButton } from "@mvpi/ui-helpers";
import { useVideos } from "@mvpi/ui-metadata-client";
import { devLogger as logger } from "@mvpi/util";
import { useFormikContext } from "formik";

import TelevisionShowSelect from "../helpers/TelevisionShowSelect.js";

const CREATE_AND_UPDATE_TELEVISION_EPISODES = gql`
  mutation createAndUpdateTelevisionEpisodes(
    $televisionShowId: ForeignKey!
    $createVideoIds: [ForeignKey]!
    $updateIds: [ID]!
  ) {
    createTelevisionEpisodes(
      televisionShowId: $televisionShowId
      videoIds: $createVideoIds
    ) {
      id
    }
    updateTelevisionEpisodes(
      ids: $updateIds
      televisionShowId: $televisionShowId
      episodeNumber: null
      seasonNumber: null
    ) {
      id
    }
  }
`;

const DELETE_TELEVISION_EPISODES = gql`
  mutation deleteTelevisionEpisodes($ids: [ID]!) {
    deleteTelevisionEpisodes(ids: $ids) {
      id
    }
  }
`;

const VideoListTelevisionShowSelect = () => {
  const { values } = useFormikContext();
  const { refetch, videos } = useVideos();

  const [
    createAndUpdateTelevisionEpisodes,
    { loading: isCreating },
  ] = useMutation(CREATE_AND_UPDATE_TELEVISION_EPISODES);
  const [deleteTelevisionEpisodes, { loading: isDeleting }] = useMutation(
    DELETE_TELEVISION_EPISODES
  );

  const isLoading = isCreating || isDeleting;
  const disabled =
    isLoading || !values.televisionShow || !values.selected.length;

  return (
    <div>
      <Label>
        Television Show
        <TelevisionShowSelect
          isClearable
          isCreatable
          name="televisionShow"
          placeholder="Select Television Show..."
        />
      </Label>
      <LinkButton
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={!values.televisionShow}
        to={`/television-shows/${values.televisionShow}`}
      >
        Edit Show
      </LinkButton>
      <Button
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={disabled}
        isLoading={isLoading}
        onClick={async () => {
          try {
            const televisionShowId = values.televisionShow;
            const createVideoIds = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) && !video.televisionEpisode
              )
              .map((video) => video.id);
            const updateIds = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) &&
                  video.televisionEpisode &&
                  video.televisionEpisode.televisionShow.id !== televisionShowId
              )
              .map((video) => video.televisionEpisode.id);

            await createAndUpdateTelevisionEpisodes({
              variables: { televisionShowId, createVideoIds, updateIds },
            });
            await refetch();
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Add Show to Selected
      </Button>
      <Button
        css={css`
          width: 100%;
        `}
        disabled={disabled}
        isLoading={isLoading}
        onClick={async () => {
          try {
            const televisionShowId = values.televisionShow;
            const ids = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) &&
                  video.televisionEpisode?.televisionShow.id ===
                    televisionShowId
              )
              .map((video) => video.televisionEpisode.id);

            await deleteTelevisionEpisodes({ variables: { ids } });
            await refetch();
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Remove Show from Selected
      </Button>
    </div>
  );
};

export default VideoListTelevisionShowSelect;
