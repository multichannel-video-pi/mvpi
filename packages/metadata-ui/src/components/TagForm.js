/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { devLogger as logger } from "@mvpi/util";
import { Formik, Form } from "formik";

import {
  Input,
  FormButtons,
  Label,
  Paragraph,
  Textarea,
} from "@mvpi/ui-helpers";

const UPDATE_TAG = gql`
  mutation updateTag($id: ID!, $name: String, $overview: String) {
    updateTag(id: $id, name: $name, overview: $overview) {
      id
      name
      overview
    }
  }
`;

const TagForm = ({ tag }) => {
  const [updateTag, { error, loading }] = useMutation(UPDATE_TAG);

  const initialValues = {
    name: tag.name || "",
    overview: tag.overview || "",
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values) => {
        try {
          await updateTag({
            variables: {
              id: tag.id,
              name: values.name || null,
              overview: values.overview || null,
            },
          });
        } catch (err) {
          logger.error(err);
        }
      }}
    >
      <Form>
        {error && (
          <Paragraph isDanger>Failed to update tag: {error.message}</Paragraph>
        )}
        <Label>
          Name
          <Input name="name" type="text" />
        </Label>
        <Label>
          Overview
          <Textarea name="overview" />
        </Label>
        <FormButtons isDanger={error} isLoading={loading} submit="Update" />
      </Form>
    </Formik>
  );
};

export default TagForm;
