/** @jsxImportSource @emotion/react */
import { useMutation, gql } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Label, StarsInput } from "@mvpi/ui-helpers";
import { devLogger as logger } from "@mvpi/util";
import { useFormikContext } from "formik";

const ADD_RATING_TO_SELECTED = gql`
  mutation AddRatingToSelected($ids: [ID]!, $rating: Int!) {
    updateVideos(ids: $ids, rating: $rating) {
      id
      rating
    }
  }
`;

const VideoListRatingSelect = () => {
  const { setFieldValue, values } = useFormikContext();
  const [addRatingToSelected, { loading }] = useMutation(
    ADD_RATING_TO_SELECTED
  );

  const disabled = loading || !values.rating || !values.selected.length;

  return (
    <div>
      <Label>Rating</Label>
      <StarsInput name="rating" size="2.375rem" />
      <Button
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={!values.rating}
        onClick={() => setFieldValue("rating", "")}
      >
        Clear Rating
      </Button>
      <Button
        css={css`
          width: 100%;
        `}
        disabled={disabled}
        isLoading={loading}
        onClick={async () => {
          try {
            await addRatingToSelected({
              variables: { ids: values.selected, rating: values.rating },
            });
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Add Rating to Selected
      </Button>
    </div>
  );
};

export default VideoListRatingSelect;
