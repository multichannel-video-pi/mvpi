/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Paragraph, Section, Title } from "@mvpi/ui-helpers";
import { devLogger as logger } from "@mvpi/util";
import { useHistory } from "react-router-dom";

const DELETE_TELEVISION_SHOW = gql`
  mutation deleteTelevisionShow($id: ID!) {
    deleteTelevisionShow(id: $id) {
      id
      name
      overview
    }
  }
`;

const TelevisionShowRemoval = ({ televisionShow }) => {
  const history = useHistory();

  const [deleteTelevisionShow, { error, loading }] = useMutation(
    DELETE_TELEVISION_SHOW,
    {
      update: (cache, { data }) => {
        cache.evict({
          id: cache.identify(data.deleteTelevisionShow),
        });
      },
    }
  );

  return (
    <Section>
      <Title type="2" size="6">
        Removal
      </Title>
      {error && (
        <Paragraph isDanger>
          Failed to remove television show: {error.message}
        </Paragraph>
      )}
      <Paragraph isDanger>
        Removal is permanent, and cannot be reversed.
      </Paragraph>
      <Paragraph>
        If you delete this television show, it will destroy all associations it
        has with other records.
      </Paragraph>
      <div
        css={css`
          text-align: right;
        `}
      >
        <Button
          isDanger
          isLoading={loading}
          onClick={async () => {
            try {
              await deleteTelevisionShow({
                variables: {
                  id: televisionShow.id,
                },
              });
              history.push("/");
            } catch (err) {
              logger.error(err);
            }
          }}
        >
          Delete Television Show
        </Button>
      </div>
    </Section>
  );
};

export default TelevisionShowRemoval;
