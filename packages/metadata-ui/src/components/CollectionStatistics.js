import { useVideos } from "@mvpi/ui-metadata-client";
import React, { useMemo } from "react";

import {
  DescriptionList,
  FormattedDuration,
  FormattedSize,
  Section,
  Title,
} from "@mvpi/ui-helpers";

import {
  useVideoListCount,
  useVideoListDuration,
  useVideoListSize,
} from "@mvpi/ui-hooks";

const CollectionStatistics = ({ collection }) => {
  const { videos } = useVideos();

  const movies = useMemo(
    () =>
      videos.filter((video) => video.movie?.collection?.id === collection.id),
    [collection, videos]
  );

  const count = useVideoListCount(movies);
  const duration = useVideoListDuration(movies);
  const size = useVideoListSize(movies);

  return (
    <Section>
      <Title type="2" size="6">
        Statistics
      </Title>
      <DescriptionList
        entries={[
          ["Videos", count.video],
          ["Tags", count.tag],
          ["Total Size", <FormattedSize size={size.total} />],
          ["Average Size", <FormattedSize size={size.average} />],
          ["Median Size", <FormattedSize size={size.median} />],
          ["Total Duration", <FormattedDuration duration={duration.total} />],
          [
            "Average Duration",
            <FormattedDuration duration={duration.average} />,
          ],
          ["Median Duration", <FormattedDuration duration={duration.median} />],
        ]}
      />
    </Section>
  );
};

export default CollectionStatistics;
