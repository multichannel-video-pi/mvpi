import { Checkbox } from "@mvpi/ui-helpers";
import { formatTitle } from "@mvpi/util";
import React from "react";
import { Link } from "react-router-dom";

const VideoListItem = ({ video }) => (
  <li>
    <Checkbox name="selected" value={video.id}>
      {formatTitle(video)}
    </Checkbox>{" "}
    <Link to={`/videos/${video.id}`}>Details</Link>
  </li>
);

export default VideoListItem;
