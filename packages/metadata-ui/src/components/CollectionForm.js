/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { devLogger as logger } from "@mvpi/util";
import { Formik, Form } from "formik";

import {
  Input,
  FormButtons,
  Label,
  Paragraph,
  Textarea,
} from "@mvpi/ui-helpers";

const UPDATE_COLLECTION = gql`
  mutation updateCollection($id: ID!, $name: String, $overview: String) {
    updateCollection(id: $id, name: $name, overview: $overview) {
      id
      name
      overview
    }
  }
`;

const CollectionForm = ({ collection }) => {
  const [updateCollection, { error, loading }] = useMutation(UPDATE_COLLECTION);

  const initialValues = {
    name: collection.name || "",
    overview: collection.overview || "",
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values) => {
        try {
          await updateCollection({
            variables: {
              id: collection.id,
              name: values.name || null,
              overview: values.overview || null,
            },
          });
        } catch (err) {
          logger.error(err);
        }
      }}
    >
      <Form>
        {error && (
          <Paragraph isDanger>
            Failed to update collection: {error.message}
          </Paragraph>
        )}
        <Label>
          Name
          <Input name="name" type="text" />
        </Label>
        <Label>
          Overview
          <Textarea name="overview" />
        </Label>
        <FormButtons isDanger={error} isLoading={loading} submit="Update" />
      </Form>
    </Formik>
  );
};

export default CollectionForm;
