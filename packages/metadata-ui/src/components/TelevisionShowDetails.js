/** @jsxImportSource @emotion/react */
import { gql, useQuery } from "@apollo/client";
import { NoData, Section, Title } from "@mvpi/ui-helpers";
import { useParams } from "react-router-dom";

import TelevisionShowForm from "./TelevisionShowForm.js";
import TelevisionShowRemoval from "./TelevisionShowRemoval.js";
import TelevisionShowStatistics from "./TelevisionShowStatistics.js";

const FETCH_TELEVISION_SHOW = gql`
  query fetchTelevisionShow($id: ID!) {
    televisionShow(id: $id) {
      id
      name
      overview
    }
  }
`;

const TelevisionShowDetails = () => {
  const { id } = useParams();
  const { data, error, loading, refetch } = useQuery(FETCH_TELEVISION_SHOW, {
    pollInterval: 300000,
    variables: { id },
  });

  const televisionShow = data?.televisionShow;

  if (!televisionShow)
    return (
      <NoData
        error={error}
        loading={loading}
        loadingText="Fetching Television Show..."
        reload={() => refetch()}
      />
    );

  return (
    <article>
      <Section>
        <Title size="5">Television Show: {televisionShow.name}</Title>
        <TelevisionShowForm televisionShow={televisionShow} />
      </Section>
      <TelevisionShowStatistics televisionShow={televisionShow} />
      <TelevisionShowRemoval televisionShow={televisionShow} />
    </article>
  );
};

export default TelevisionShowDetails;
