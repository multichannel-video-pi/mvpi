/** @jsxImportSource @emotion/react */
import { useMutation, gql } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Label, LinkButton } from "@mvpi/ui-helpers";
import { useVideos } from "@mvpi/ui-metadata-client";
import { devLogger as logger } from "@mvpi/util";
import { useFormikContext } from "formik";

import CollectionSelect from "../helpers/CollectionSelect.js";

const CREATE_AND_UPDATE_COLLECTION_MOVIES = gql`
  mutation createAndUpdateCollectionMovies(
    $createVideoIds: [ForeignKey]!
    $collectionId: ForeignKey!
    $updateIds: [ID]!
  ) {
    createMovies(collectionId: $collectionId, videoIds: $createVideoIds) {
      id
    }
    updateMovies(
      ids: $updateIds
      chronology: null
      collectionId: $collectionId
    ) {
      id
    }
  }
`;

const DELETE_COLLECTION_MOVIES = gql`
  mutation deleteCollectionMovies($ids: [ID]!) {
    updateMovies(ids: $ids, chronology: null, collectionId: null) {
      id
    }
  }
`;

const VideoListCollectionSelect = () => {
  const { values } = useFormikContext();
  const { refetch, videos } = useVideos();

  const [
    createAndUpdateCollectionMovies,
    { loading: isCreating },
  ] = useMutation(CREATE_AND_UPDATE_COLLECTION_MOVIES);
  const [deleteCollectionMovies, { loading: isDeleting }] = useMutation(
    DELETE_COLLECTION_MOVIES
  );

  const isLoading = isCreating || isDeleting;
  const disabled = isLoading || !values.collection || !values.selected.length;

  return (
    <div>
      <Label>
        Collection
        <CollectionSelect
          isClearable
          isCreatable
          name="collection"
          placeholder="Select Collection..."
        />
      </Label>
      <LinkButton
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={!values.collection}
        to={`/collections/${values.collection}`}
      >
        Edit Collection
      </LinkButton>
      <Button
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={disabled}
        isLoading={isLoading}
        onClick={async () => {
          try {
            const collectionId = values.collection;
            const createVideoIds = videos
              .filter(
                (video) => values.selected.includes(video.id) && !video.movie
              )
              .map((video) => video.id);
            const updateIds = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) &&
                  video.movie &&
                  video.movie.collection?.id !== collectionId
              )
              .map((video) => video.movie.id);

            await createAndUpdateCollectionMovies({
              variables: { createVideoIds, collectionId, updateIds },
            });
            await refetch();
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Add Collection to Selected
      </Button>
      <Button
        css={css`
          width: 100%;
        `}
        disabled={disabled}
        isLoading={isLoading}
        onClick={async () => {
          try {
            const collectionId = values.collection;
            const ids = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) &&
                  video.movie?.collection?.id === collectionId
              )
              .map((video) => video.movie.id);

            await deleteCollectionMovies({ variables: { ids } });
            await refetch();
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Remove Collection from Selected
      </Button>
    </div>
  );
};

export default VideoListCollectionSelect;
