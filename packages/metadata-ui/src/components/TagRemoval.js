/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Paragraph, Section, Title } from "@mvpi/ui-helpers";
import { devLogger as logger } from "@mvpi/util";
import { useHistory } from "react-router-dom";

const DELETE_TAG = gql`
  mutation deleteTag($id: ID!) {
    deleteTag(id: $id) {
      id
      name
      overview
    }
  }
`;

const TagRemoval = ({ tag }) => {
  const history = useHistory();

  const [deleteTag, { error, loading }] = useMutation(DELETE_TAG, {
    update: (cache, { data }) => {
      cache.evict({
        id: cache.identify(data.deleteTag),
      });
    },
  });

  return (
    <Section>
      <Title type="2" size="6">
        Removal
      </Title>
      {error && (
        <Paragraph isDanger>Failed to remove tag: {error.message}</Paragraph>
      )}
      <Paragraph isDanger>
        Removal is permanent, and cannot be reversed.
      </Paragraph>
      <Paragraph>
        If you delete this tag, it will destroy all associations it has with
        other records.
      </Paragraph>
      <div
        css={css`
          text-align: right;
        `}
      >
        <Button
          isDanger
          isLoading={loading}
          onClick={async () => {
            try {
              await deleteTag({
                variables: {
                  id: tag.id,
                },
              });
              history.push("/");
            } catch (err) {
              logger.error(err);
            }
          }}
        >
          Delete Tag
        </Button>
      </div>
    </Section>
  );
};

export default TagRemoval;
