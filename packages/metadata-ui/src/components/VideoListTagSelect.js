/** @jsxImportSource @emotion/react */
import { useMutation, gql } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Label, LinkButton } from "@mvpi/ui-helpers";
import { useVideos } from "@mvpi/ui-metadata-client";
import { devLogger as logger } from "@mvpi/util";
import { useFormikContext } from "formik";

import TagSelect from "../helpers/TagSelect.js";

const CREATE_VIDEO_TAGS = gql`
  mutation createVideoTags($videoIds: [ID]!, $tagId: ID!) {
    createVideoTags(tagIds: [$tagId], videoIds: $videoIds) {
      tagId
      videoId
    }
  }
`;

const DELETE_VIDEO_TAGS = gql`
  mutation deleteVideoTags($videoIds: [ID]!, $tagId: ID!) {
    deleteVideoTags(tagIds: [$tagId], videoIds: $videoIds) {
      tagId
      videoId
    }
  }
`;

const VideoListTagSelect = () => {
  const { values } = useFormikContext();
  const { refetch, videos } = useVideos();

  const [createVideoTags, { loading: isCreating }] = useMutation(
    CREATE_VIDEO_TAGS
  );
  const [deleteVideoTags, { loading: isDeleting }] = useMutation(
    DELETE_VIDEO_TAGS
  );

  const isLoading = isCreating || isDeleting;
  const disabled = isLoading || !values.tag || !values.selected.length;

  return (
    <div>
      <Label>
        Tag
        <TagSelect
          isClearable
          isCreatable
          name="tag"
          placeholder="Select Tag..."
        />
      </Label>
      <LinkButton
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={!values.tag}
        to={`/tags/${values.tag}`}
      >
        Edit Tag
      </LinkButton>
      <Button
        css={css`
          width: 100%;
          margin-bottom: 0.25rem;
        `}
        disabled={disabled}
        isLoading={isLoading}
        onClick={async () => {
          try {
            const tagId = values.tag;
            const videoIds = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) &&
                  !video.tags.find((tag) => tag.id === tagId)
              )
              .map((video) => video.id);

            await createVideoTags({ variables: { tagId, videoIds } });
            await refetch();
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Add Tag to Selected
      </Button>
      <Button
        css={css`
          width: 100%;
        `}
        disabled={disabled}
        isLoading={isLoading}
        onClick={async () => {
          try {
            const tagId = values.tag;
            const videoIds = videos
              .filter(
                (video) =>
                  values.selected.includes(video.id) &&
                  video.tags.find((tag) => tag.id === tagId)
              )
              .map((video) => video.id);

            await deleteVideoTags({ variables: { tagId, videoIds } });
            await refetch();
          } catch (err) {
            logger.error(err);
          }
        }}
      >
        Remove Tag from Selected
      </Button>
    </div>
  );
};

export default VideoListTagSelect;
