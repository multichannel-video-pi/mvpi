/** @jsxImportSource @emotion/react */
import { gql, useQuery } from "@apollo/client";
import { NoData, Section, Title } from "@mvpi/ui-helpers";
import { useParams } from "react-router-dom";

import TagForm from "./TagForm.js";
import TagRemoval from "./TagRemoval.js";
import TagStatistics from "./TagStatistics.js";

const FETCH_TAG = gql`
  query fetchTag($id: ID!) {
    tag(id: $id) {
      id
      name
      overview
    }
  }
`;

const TagDetails = () => {
  const { id } = useParams();
  const { data, error, loading, refetch } = useQuery(FETCH_TAG, {
    pollInterval: 300000,
    variables: { id },
  });

  const tag = data?.tag;

  if (!tag)
    return (
      <NoData
        error={error}
        loading={loading}
        loadingText="Fetching Tag..."
        reload={() => refetch()}
      />
    );

  return (
    <article>
      <Section>
        <Title size="5">Tag: {tag.name}</Title>
        <TagForm tag={tag} />
      </Section>
      <TagStatistics tag={tag} />
      <TagRemoval tag={tag} />
    </article>
  );
};

export default TagDetails;
