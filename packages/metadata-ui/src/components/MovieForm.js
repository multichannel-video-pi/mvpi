import { gql, useMutation } from "@apollo/client";
import React from "react";
import { Formik, Form } from "formik";
import { FormButtons, Input, Label } from "@mvpi/ui-helpers";
import { devLogger as logger } from "@mvpi/util";

import CollectionSelect from "../helpers/CollectionSelect.js";

const CREATE_MOVIE = gql`
  mutation createMovie(
    $chronology: Int
    $collectionId: ForeignKey
    $videoId: ForeignKey!
  ) {
    createMovie(
      chronology: $chronology
      collectionId: $collectionId
      videoId: $videoId
    ) {
      video {
        id
        movie {
          id
          chronology
          collection {
            id
            name
            overview
          }
        }
      }
    }
  }
`;

const UPDATE_MOVIE = gql`
  mutation updateMovie($id: ID!, $collectionId: ForeignKey, $chronology: Int) {
    updateMovie(id: $id, chronology: $chronology, collectionId: $collectionId) {
      id
      chronology
      collection {
        id
        name
        overview
      }
    }
  }
`;

const MovieForm = ({ video }) => {
  const [
    createMovie,
    { error: createError, loading: createLoading },
  ] = useMutation(CREATE_MOVIE);

  const [
    updateMovie,
    { error: updateError, loading: updateLoading },
  ] = useMutation(UPDATE_MOVIE);

  const error = createError || updateError;
  const loading = createLoading || updateLoading;
  const movie = video.movie;

  const initialValues = {
    chronology: Number.isInteger(movie?.chronology) ? movie.chronology : "",
    collectionId: movie?.collection?.id || "",
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values) => {
        try {
          if (!movie)
            await createMovie({
              variables: {
                chronology: Number.isInteger(values.chronology)
                  ? values.chronology
                  : null,
                collectionId: values.collectionId || null,
                videoId: video.id,
              },
            });
          else
            await updateMovie({
              variables: {
                id: movie.id,
                chronology: Number.isInteger(values.chronology)
                  ? values.chronology
                  : null,
                collectionId: values.collectionId || null,
              },
            });
        } catch (err) {
          logger.error(err);
        }
      }}
    >
      <Form>
        <Label>
          Collection
          <CollectionSelect
            isClearable
            isCreatable
            name="collectionId"
            placeholder="Select collection..."
          />
        </Label>
        <Label>
          Chronology
          <Input min="0" name="chronology" type="number" />
        </Label>
        <FormButtons isDanger={error} isLoading={loading} submit="Update" />
      </Form>
    </Formik>
  );
};

export default MovieForm;
