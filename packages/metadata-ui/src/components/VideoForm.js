/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { devLogger as logger, propSortFn } from "@mvpi/util";
import { Formik, Form } from "formik";
import { useMemo } from "react";

import {
  Input,
  FormButtons,
  Label,
  StarsInput,
  Textarea,
} from "@mvpi/ui-helpers";

import TagSelect from "../helpers/TagSelect.js";

const UPDATE_VIDEO = gql`
  mutation updateVideo(
    $id: ID!
    $overview: String
    $rating: Int
    $releaseDate: String
    $title: String
  ) {
    updateVideo(
      id: $id
      overview: $overview
      rating: $rating
      releaseDate: $releaseDate
      title: $title
    ) {
      id
      overview
      rating
      releaseDate
      title
      tags {
        id
        name
        overview
      }
    }
  }
`;

const UPDATE_VIDEO_TAGS = gql`
  mutation updateVideoTags(
    $videoId: ID!
    $createTagIds: [ID]!
    $deleteTagIds: [ID]!
  ) {
    createVideoTags(tagIds: $createTagIds, videoIds: [$videoId]) {
      tagId
      videoId
    }
    deleteVideoTags(tagIds: $deleteTagIds, videoIds: [$videoId]) {
      tagId
      videoId
    }
  }
`;

const VideoForm = ({ video }) => {
  const [
    updateVideo,
    { error: updateVideoError, loading: updateVideoLoading },
  ] = useMutation(UPDATE_VIDEO);

  const [
    updateVideoTags,
    { error: updateVideoTagsError, loading: updateVideoTagsLoading },
  ] = useMutation(UPDATE_VIDEO_TAGS);

  const error = updateVideoError || updateVideoTagsError;
  const loading = updateVideoLoading || updateVideoTagsLoading;

  const tags = useMemo(
    () =>
      Array.isArray(video?.tags)
        ? [...video.tags].sort(propSortFn("name")).map((tag) => tag.id)
        : [],
    [video]
  );

  const initialValues = {
    overview: video.overview || "",
    rating: video.rating || "",
    releaseDate: video.releaseDate || "",
    title: video.title || "",
    tags,
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values) => {
        try {
          const createTagIds = values.tags.filter((tag) => !tags.includes(tag));
          const deleteTagIds = tags.filter((tag) => !values.tags.includes(tag));

          await updateVideoTags({
            variables: {
              videoId: video.id,
              createTagIds,
              deleteTagIds,
            },
          });

          await updateVideo({
            variables: {
              id: video.id,
              overview: values.overview || null,
              rating: values.rating || null,
              releaseDate: values.releaseDate || null,
              title: values.title || null,
            },
          });
        } catch (err) {
          logger.error(err);
        }
      }}
    >
      <Form>
        <StarsInput inline name="rating" size="1.5rem" />
        <br />
        <Label>
          Title
          <Input name="title" type="text" />
        </Label>
        <Label>
          Overview
          <Textarea name="overview" />
        </Label>
        <Label>
          Tags
          <TagSelect
            isClearable
            isCreatable
            name="tags"
            placeholder="Select tags..."
          />
        </Label>
        <Label>
          Release Date
          <Input name="releaseDate" type="text" />
        </Label>
        <FormButtons isDanger={error} isLoading={loading} submit="Update" />
      </Form>
    </Formik>
  );
};

export default VideoForm;
