/** @jsxImportSource @emotion/react */
import { gql, useQuery } from "@apollo/client";
import { NoData, Section, Title } from "@mvpi/ui-helpers";
import { useParams } from "react-router-dom";

import CollectionForm from "./CollectionForm.js";
import CollectionRemoval from "./CollectionRemoval.js";
import CollectionStatistics from "./CollectionStatistics.js";

const FETCH_COLLECTION = gql`
  query fetchCollection($id: ID!) {
    collection(id: $id) {
      id
      name
      overview
    }
  }
`;

const CollectionDetails = () => {
  const { id } = useParams();
  const { data, error, loading, refetch } = useQuery(FETCH_COLLECTION, {
    pollInterval: 300000,
    variables: { id },
  });

  const collection = data?.collection;

  if (!collection)
    return (
      <NoData
        error={error}
        loading={loading}
        loadingText="Fetching Collection..."
        reload={() => refetch()}
      />
    );

  return (
    <article>
      <Section>
        <Title size="5">Collection: {collection.name}</Title>
        <CollectionForm collection={collection} />
      </Section>
      <CollectionStatistics collection={collection} />
      <CollectionRemoval collection={collection} />
    </article>
  );
};

export default CollectionDetails;
