/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { Button } from "@mvpi/ui-helpers";
import { useFormikContext } from "formik";
import { useMemo } from "react";

const VideoListSelectButtons = ({ videos }) => {
  const { setFieldValue, values } = useFormikContext();

  const videoIds = useMemo(() => videos.map((video) => video.id), [videos]);

  const allSelected = useMemo(() => {
    return videoIds.every((id) => values.selected.includes(id));
  }, [videoIds, values.selected]);

  const noneSelected = useMemo(() => {
    return !values.selected.length;
  }, [values.selected]);

  return (
    <div
      css={css`
        display: flex;
      `}
    >
      <Button
        css={css`
          flex-grow: 1;
          margin-right: 0.125rem;
        `}
        disabled={allSelected}
        onClick={() => setFieldValue("selected", videoIds)}
      >
        Select All
      </Button>{" "}
      <Button
        css={css`
          flex-grow: 1;
          margin-left: 0.125rem;
        `}
        disabled={noneSelected}
        onClick={() => setFieldValue("selected", [])}
      >
        Unselect All
      </Button>
    </div>
  );
};

export default VideoListSelectButtons;
