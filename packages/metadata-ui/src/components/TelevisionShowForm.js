/** @jsxImportSource @emotion/react */
import { gql, useMutation } from "@apollo/client";
import { devLogger as logger } from "@mvpi/util";
import { Formik, Form } from "formik";

import {
  Input,
  FormButtons,
  Label,
  Paragraph,
  Textarea,
} from "@mvpi/ui-helpers";

const UPDATE_TELEVISION_SHOW = gql`
  mutation updateTelevisionShow($id: ID!, $name: String, $overview: String) {
    updateTelevisionShow(id: $id, name: $name, overview: $overview) {
      id
      name
      overview
    }
  }
`;

const TelevisionShowForm = ({ televisionShow }) => {
  const [updateTelevisionShow, { error, loading }] = useMutation(
    UPDATE_TELEVISION_SHOW
  );

  const initialValues = {
    name: televisionShow.name || "",
    overview: televisionShow.overview || "",
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values) => {
        try {
          await updateTelevisionShow({
            variables: {
              id: televisionShow.id,
              name: values.name || null,
              overview: values.overview || null,
            },
          });
        } catch (err) {
          logger.error(err);
        }
      }}
    >
      <Form>
        {error && (
          <Paragraph isDanger>
            Failed to update television show: {error.message}
          </Paragraph>
        )}
        <Label>
          Name
          <Input name="name" type="text" />
        </Label>
        <Label>
          Overview
          <Textarea name="overview" />
        </Label>
        <FormButtons isDanger={error} isLoading={loading} submit="Update" />
      </Form>
    </Formik>
  );
};

export default TelevisionShowForm;
