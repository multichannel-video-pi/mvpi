import { useVideos } from "@mvpi/ui-metadata-client";
import React, { useMemo } from "react";

import {
  DescriptionList,
  FormattedDuration,
  FormattedSize,
  Section,
  Title,
} from "@mvpi/ui-helpers";

import {
  useVideoListCount,
  useVideoListDuration,
  useVideoListSize,
} from "@mvpi/ui-hooks";

const TagStatistics = ({ tag }) => {
  const { videos } = useVideos();

  const tagVideos = useMemo(
    () =>
      videos.filter((video) => video.tags.map(({ id }) => id).includes(tag.id)),
    [tag, videos]
  );

  const count = useVideoListCount(tagVideos);
  const duration = useVideoListDuration(tagVideos);
  const size = useVideoListSize(tagVideos);

  return (
    <Section>
      <Title type="2" size="6">
        Statistics
      </Title>
      <DescriptionList
        entries={[
          ["Videos", count.video],
          ["Television Shows", count.televisionShow],
          ["Collections", count.collection],
          ["Total Size", <FormattedSize size={size.total} />],
          ["Average Size", <FormattedSize size={size.average} />],
          ["Median Size", <FormattedSize size={size.median} />],
          ["Total Duration", <FormattedDuration duration={duration.total} />],
          [
            "Average Duration",
            <FormattedDuration duration={duration.average} />,
          ],
          ["Median Duration", <FormattedDuration duration={duration.median} />],
        ]}
      />
    </Section>
  );
};

export default TagStatistics;
