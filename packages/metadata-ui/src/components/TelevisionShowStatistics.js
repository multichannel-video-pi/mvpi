import { useVideos } from "@mvpi/ui-metadata-client";
import React, { useMemo } from "react";

import {
  DescriptionList,
  FormattedDuration,
  FormattedSize,
  Section,
  Title,
} from "@mvpi/ui-helpers";

import {
  useVideoListCount,
  useVideoListDuration,
  useVideoListSize,
} from "@mvpi/ui-hooks";

const TelevisionShowStatistics = ({ televisionShow }) => {
  const { videos } = useVideos();

  const televisionEpisodes = useMemo(
    () =>
      videos.filter(
        (video) =>
          video.televisionEpisode?.televisionShow.id === televisionShow.id
      ),
    [televisionShow, videos]
  );

  const count = useVideoListCount(televisionEpisodes);
  const duration = useVideoListDuration(televisionEpisodes);
  const size = useVideoListSize(televisionEpisodes);

  return (
    <Section>
      <Title type="2" size="6">
        Statistics
      </Title>
      <DescriptionList
        entries={[
          ["Videos", count.video],
          ["Tags", count.tag],
          ["Total Size", <FormattedSize size={size.total} />],
          ["Average Size", <FormattedSize size={size.average} />],
          ["Median Size", <FormattedSize size={size.median} />],
          ["Total Duration", <FormattedDuration duration={duration.total} />],
          [
            "Average Duration",
            <FormattedDuration duration={duration.average} />,
          ],
          ["Median Duration", <FormattedDuration duration={duration.median} />],
        ]}
      />
    </Section>
  );
};

export default TelevisionShowStatistics;
