import { gql, useMutation } from "@apollo/client";
import React from "react";
import { Formik, Form } from "formik";
import { FormButtons, Input, Label } from "@mvpi/ui-helpers";
import { devLogger as logger } from "@mvpi/util";

import TelevisionShowSelect from "../helpers/TelevisionShowSelect.js";

const CREATE_TELEVISION_EPISODE = gql`
  mutation createTelevisionEpisode(
    $episodeNumber: Int
    $seasonNumber: Int
    $televisionShowId: ForeignKey!
    $videoId: ForeignKey!
  ) {
    createTelevisionEpisode(
      episodeNumber: $episodeNumber
      seasonNumber: $seasonNumber
      televisionShowId: $televisionShowId
      videoId: $videoId
    ) {
      video {
        id
        televisionEpisode {
          id
          episodeNumber
          seasonNumber
          televisionShow {
            id
            name
            overview
          }
        }
      }
    }
  }
`;

const DELETE_TELEVISION_EPISODE = gql`
  mutation deleteTelevisionEpisode($id: ID!) {
    deleteTelevisionEpisode(id: $id) {
      id
      episodeNumber
      seasonNumber
      televisionShow {
        id
        name
        overview
      }
    }
  }
`;

const UPDATE_TELEVISION_EPISODE = gql`
  mutation updateTelevisionEpisode(
    $id: ID!
    $episodeNumber: Int
    $seasonNumber: Int
    $televisionShowId: ForeignKey!
  ) {
    updateTelevisionEpisode(
      id: $id
      episodeNumber: $episodeNumber
      seasonNumber: $seasonNumber
      televisionShowId: $televisionShowId
    ) {
      id
      episodeNumber
      seasonNumber
      televisionShow {
        id
        name
        overview
      }
    }
  }
`;

const TelevisionEpisodeForm = ({ video }) => {
  const [
    createTelevisionEpisode,
    { error: createError, loading: createLoading },
  ] = useMutation(CREATE_TELEVISION_EPISODE);

  const [
    deleteTelevisionEpisode,
    { error: deleteError, loading: deleteLoading },
  ] = useMutation(DELETE_TELEVISION_EPISODE, {
    update: (cache, { data }) => {
      cache.evict({
        id: cache.identify(data.deleteTelevisionEpisode),
      });
    },
  });

  const [
    updateTelevisionEpisode,
    { error: updateError, loading: updateLoading },
  ] = useMutation(UPDATE_TELEVISION_EPISODE);

  const error = createError || deleteError || updateError;
  const loading = createLoading || deleteLoading || updateLoading;
  const televisionEpisode = video.televisionEpisode;

  const initialValues = {
    episodeNumber: televisionEpisode?.episodeNumber || "",
    seasonNumber: televisionEpisode?.seasonNumber || "",
    televisionShowId: televisionEpisode?.televisionShow.id || "",
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values) => {
        try {
          if (!televisionEpisode)
            await createTelevisionEpisode({
              variables: {
                episodeNumber: values.episodeNumber || null,
                seasonNumber: values.seasonNumber || null,
                televisionShowId: values.televisionShowId || null,
                videoId: video.id,
              },
            });
          else if (!values.televisionShowId)
            await deleteTelevisionEpisode({
              variables: { id: televisionEpisode.id },
            });
          else
            await updateTelevisionEpisode({
              variables: {
                id: televisionEpisode.id,
                episodeNumber: values.episodeNumber || null,
                seasonNumber: values.seasonNumber || null,
                televisionShowId: values.televisionShowId || null,
              },
            });
        } catch (err) {
          logger.error(err);
        }
      }}
    >
      <Form>
        <Label>
          Television Show
          <TelevisionShowSelect
            isClearable
            isCreatable
            name="televisionShowId"
            placeholder="Select television show..."
          />
        </Label>
        <Label>
          Season
          <Input min="0" name="seasonNumber" type="number" />
        </Label>
        <Label>
          Episode
          <Input min="0" name="episodeNumber" type="number" />
        </Label>
        <FormButtons isDanger={error} isLoading={loading} submit="Update" />
      </Form>
    </Formik>
  );
};

export default TelevisionEpisodeForm;
