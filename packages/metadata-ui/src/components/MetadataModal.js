import { Modal } from "@mvpi/ui-helpers";
import React from "react";
import { Redirect, Route, Switch, useHistory } from "react-router-dom";

import CollectionDetails from "./CollectionDetails.js";
import TagDetails from "./TagDetails.js";
import TelevisionShowDetails from "./TelevisionShowDetails.js";
import VideoDetails from "./VideoDetails.js";

const MetadataModal = () => {
  const history = useHistory();

  return (
    <Modal onBackgroundClick={() => history.push("/")}>
      <Switch>
        <Route exact path="/collections/:id">
          <CollectionDetails />
        </Route>
        <Route exact path="/tags/:id">
          <TagDetails />
        </Route>
        <Route exact path="/television-shows/:id">
          <TelevisionShowDetails />
        </Route>
        <Route path="/videos/:id">
          <VideoDetails />
        </Route>
        <Redirect to="/" />
      </Switch>
    </Modal>
  );
};

export default MetadataModal;
