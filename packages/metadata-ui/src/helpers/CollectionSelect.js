import { gql, useMutation, useQuery } from "@apollo/client";
import { Select } from "@mvpi/ui-helpers";
import { devLogger as logger, propSortFn } from "@mvpi/util";
import { useField, useFormikContext } from "formik";
import React, { useEffect, useMemo } from "react";

const CREATE_COLLECTION = gql`
  mutation createCollection($name: String!) {
    collection: createCollection(name: $name) {
      id
      name
    }
  }
`;

const FETCH_COLLECTIONS = gql`
  query fetchCollections {
    collections {
      id
      name
    }
  }
`;

const CollectionSelect = ({ ...props }) => {
  const [field] = useField(props);
  const { setFieldError, setFieldTouched, setFieldValue } = useFormikContext();

  const [
    createCollection,
    { error: createError, loading: isCreating },
  ] = useMutation(CREATE_COLLECTION);

  const {
    data: fetchData,
    error: fetchError,
    loading: isFetching,
    refetch,
  } = useQuery(FETCH_COLLECTIONS, {
    pollInterval: 300000,
  });

  const loading = isCreating || isFetching;
  const error = createError || fetchError;

  const collections = useMemo(
    () =>
      fetchData?.collections
        .map((collection) => ({
          label: collection.name,
          value: collection.id,
        }))
        .sort(propSortFn("label")),
    [fetchData]
  );

  useEffect(() => {
    if (error) {
      setFieldError(field.name, error.message);
      setFieldTouched(field.name, true, false);
    }
  }, [error, field.name, setFieldError, setFieldTouched]);

  useEffect(() => {
    if (!loading) {
      const isMulti = Array.isArray(field.value);
      const collectionIds =
        collections?.map((collection) => collection.value) || [];
      if (
        isMulti &&
        field.value.some((collection) => !collectionIds.includes(collection))
      ) {
        setFieldValue(
          field.name,
          field.value.filter((collection) => collectionIds.includes(collection))
        );
      } else if (
        !isMulti &&
        field.value &&
        !collectionIds.includes(field.value)
      ) {
        setFieldValue(field.name, "");
      }
    }
  }, [field.name, field.value, collections, loading, setFieldValue]);

  return (
    <Select
      isDisabled={loading}
      isLoading={loading}
      onCreateOption={async (name) => {
        try {
          const { data } = await createCollection({ variables: { name } });
          await refetch();

          if (Array.isArray(field.value)) {
            setFieldValue(field.name, [...field.value, data.collection.id]);
          } else {
            setFieldValue(field.name, data.collection.id);
          }
        } catch (err) {
          logger.error(err);
        }
      }}
      options={collections}
      {...props}
    />
  );
};

export default CollectionSelect;
