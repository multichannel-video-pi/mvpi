import { gql, useMutation, useQuery } from "@apollo/client";
import { Select } from "@mvpi/ui-helpers";
import { devLogger as logger, propSortFn } from "@mvpi/util";
import { useField, useFormikContext } from "formik";
import React, { useEffect, useMemo } from "react";

const CREATE_TELEVISION_SHOW = gql`
  mutation createTelevisionShow($name: String!) {
    televisionShow: createTelevisionShow(name: $name) {
      id
      name
    }
  }
`;

const FETCH_TELEVISION_SHOWS = gql`
  query fetchTelevisionShows {
    televisionShows {
      id
      name
    }
  }
`;

const TelevisionShowSelect = (props) => {
  const [field] = useField(props);
  const { setFieldError, setFieldTouched, setFieldValue } = useFormikContext();

  const [
    createTelevisionShow,
    { error: createError, loading: isCreating },
  ] = useMutation(CREATE_TELEVISION_SHOW);

  const {
    data: fetchData,
    error: fetchError,
    loading: isFetching,
    refetch,
  } = useQuery(FETCH_TELEVISION_SHOWS, {
    pollInterval: 300000,
  });

  const loading = isCreating || isFetching;
  const error = createError || fetchError;

  const televisionShows = useMemo(
    () =>
      fetchData?.televisionShows
        .map((televisionShow) => ({
          label: televisionShow.name,
          value: televisionShow.id,
        }))
        .sort(propSortFn("label")),
    [fetchData]
  );

  useEffect(() => {
    if (error) {
      setFieldError(field.name, error.message);
      setFieldTouched(field.name, true, false);
    }
  }, [error, field.name, setFieldError, setFieldTouched]);

  useEffect(() => {
    if (!loading) {
      const isMulti = Array.isArray(field.value);
      const televisionShowIds =
        televisionShows?.map((televisionShow) => televisionShow.value) || [];
      if (
        isMulti &&
        field.value.some(
          (televisionShow) => !televisionShowIds.includes(televisionShow)
        )
      ) {
        setFieldValue(
          field.name,
          field.value.filter((televisionShow) =>
            televisionShowIds.includes(televisionShow)
          )
        );
      } else if (
        !isMulti &&
        field.value &&
        !televisionShowIds.includes(field.value)
      ) {
        setFieldValue(field.name, "");
      }
    }
  }, [field.name, field.value, loading, setFieldValue, televisionShows]);

  return (
    <Select
      isDisabled={loading}
      isLoading={loading}
      onCreateOption={async (name) => {
        try {
          const { data } = await createTelevisionShow({
            variables: { name },
          });
          await refetch();

          if (Array.isArray(field.value)) {
            setFieldValue(field.name, [...field.value, data.televisionShow.id]);
          } else {
            setFieldValue(field.name, data.televisionShow.id);
          }
        } catch (err) {
          logger.error(err);
        }
      }}
      options={televisionShows}
      {...props}
    />
  );
};

export default TelevisionShowSelect;
