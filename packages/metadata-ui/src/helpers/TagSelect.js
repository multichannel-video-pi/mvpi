import { useMutation, useQuery, gql } from "@apollo/client";
import { Select } from "@mvpi/ui-helpers";
import { devLogger as logger, propSortFn } from "@mvpi/util";
import { useField, useFormikContext } from "formik";
import React, { useEffect, useMemo } from "react";

const CREATE_TAG = gql`
  mutation createTag($name: String!) {
    tag: createTag(name: $name) {
      id
      name
      overview
    }
  }
`;

const FETCH_TAGS = gql`
  query fetchTags {
    tags {
      id
      name
      overview
    }
  }
`;

const TagSelect = (props) => {
  const [field] = useField(props);
  const { setFieldError, setFieldTouched, setFieldValue } = useFormikContext();

  const [createTag, { error: createError, loading: isCreating }] = useMutation(
    CREATE_TAG
  );

  const {
    data: fetchData,
    error: fetchError,
    loading: isFetching,
    refetch,
  } = useQuery(FETCH_TAGS, {
    pollInterval: 300000,
  });

  const loading = isCreating || isFetching;
  const error = createError || fetchError;

  const tags = useMemo(
    () =>
      fetchData?.tags
        .map((tag) => ({ label: tag.name, value: tag.id }))
        .sort(propSortFn("label")),
    [fetchData]
  );

  useEffect(() => {
    if (error) {
      setFieldError(field.name, error.message);
      setFieldTouched(field.name, true, false);
    }
  }, [error, field.name, setFieldError, setFieldTouched]);

  useEffect(() => {
    if (!loading) {
      const isMulti = Array.isArray(field.value);
      const tagIds = tags?.map((tag) => tag.value) || [];
      if (isMulti && field.value.some((tag) => !tagIds.includes(tag))) {
        setFieldValue(
          field.name,
          field.value.filter((tag) => tagIds.includes(tag))
        );
      } else if (!isMulti && field.value && !tagIds.includes(field.value)) {
        setFieldValue(field.name, "");
      }
    }
  }, [field.name, field.value, loading, setFieldValue, tags]);

  return (
    <Select
      isDisabled={loading}
      isLoading={loading}
      onCreateOption={async (name) => {
        try {
          const { data } = await createTag({ variables: { name } });
          await refetch();

          if (Array.isArray(field.value)) {
            setFieldValue(field.name, [...field.value, data.tag.id]);
          } else {
            setFieldValue(field.name, data.tag.id);
          }
        } catch (err) {
          logger.error(err);
        }
      }}
      options={tags}
      {...props}
    />
  );
};

export default TagSelect;
