/** @jsxImportSource @emotion/react */
import { Global } from "@emotion/react";
import config from "@mvpi/ui-config";
import { VideosProvider } from "@mvpi/ui-metadata-client";
import styles from "@mvpi/ui-styles";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import "reset-css";

import App from "./App.js";
import reportWebVitals from "./reportWebVitals.js";

ReactDOM.render(
  // disabling StrictMode to prevent issues with ApolloClient
  // see: https://github.com/lostpebble/pullstate/issues/60
  // <React.StrictMode>
  //   <VideosProvider config={config}>
  //     <Global styles={styles} />
  //     <Router basename={config.metadata.ui.prefix}>
  //       <App />
  //     </Router>
  //   </VideosProvider>
  // </React.StrictMode>,
  <VideosProvider config={config}>
    <Global styles={styles} />
    <Router basename={config.metadata.ui.prefix}>
      <App />
    </Router>
  </VideosProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
