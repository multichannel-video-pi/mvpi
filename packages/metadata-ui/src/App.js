/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { Backdrop, NoData, Title } from "@mvpi/ui-helpers";
import { useVideos } from "@mvpi/ui-metadata-client";
import { Formik, Form } from "formik";
import { Route, Switch } from "react-router-dom";

import MetadataModal from "./components/MetadataModal.js";
import VideoList from "./components/VideoList.js";

const App = () => {
  const { error, loading, refetch, videos } = useVideos();

  if (!videos)
    return (
      <Backdrop fullscreen>
        <NoData
          error={error}
          loading={loading}
          loadingText="Fetching Video Metadata..."
          reload={() => refetch()}
        />
      </Backdrop>
    );

  return (
    <article
      css={css`
        padding: 0 1rem;
      `}
    >
      <Title type="1">MVPi Video Metadata</Title>
      <Formik
        initialValues={{
          collection: "",
          rating: "",
          search: "",
          selected: [],
          tag: "",
          televisionShow: "",
        }}
      >
        <Form>
          <VideoList />
        </Form>
      </Formik>
      <Switch>
        <Route component={null} exact path="/" />
        <Route>
          <MetadataModal />
        </Route>
      </Switch>
    </article>
  );
};

export default App;
