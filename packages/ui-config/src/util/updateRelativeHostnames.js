const { location: { hostname } = {} } = window || {};

const RELATIVE_HOSTNAMES = ["localhost", "127.0.0.1"];

const updateRelativeHostnames = (config) => {
  return Object.fromEntries(
    Object.entries(config).map(([key, value]) => {
      if (key === "hostname" && RELATIVE_HOSTNAMES.includes(value))
        return [key, hostname];

      if (value && typeof value === "object" && !Array.isArray(value))
        return [key, updateRelativeHostnames(value)];

      return [key, value];
    })
  );
};

export default updateRelativeHostnames;
