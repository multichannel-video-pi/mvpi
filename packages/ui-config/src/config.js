import updateProductionServices from "@mvpi/config/lib/updateProductionServices.js";
import defaultConfig from "@mvpi/config/config.yml";

import updateRelativeHostnames from "./util/updateRelativeHostnames.js";

const relativeConfig = updateRelativeHostnames(defaultConfig);

const config =
  process.env.NODE_ENV === "production"
    ? updateProductionServices(relativeConfig)
    : relativeConfig;

export default config;
