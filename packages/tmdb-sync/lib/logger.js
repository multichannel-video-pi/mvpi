import config from "@mvpi/config";
import pino from "pino";

const logger = pino({ ...config.logger, name: "tmdb-sync" });

export default logger;
