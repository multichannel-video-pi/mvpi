import database, { insertInto, selectFrom, update } from "@mvpi/database";
import { Tag } from "@mvpi/models";

const upsertTag = async (genreDetails) => {
  const db = await database();

  const values = {
    name: genreDetails.name || null,
    tmdbId: genreDetails.id || null,
  };

  const selectQuery = selectFrom(Tag.table).where(
    Tag.getConditions({ tmdbId: values.tmdbId })
  );
  const existingTag = await db.get(selectQuery);

  if (values.name === existingTag?.name) return existingTag.id;

  if (existingTag) {
    const updateQuery = update(Tag.name, values).where(
      Tag.getConditions({ id: existingTag.id })
    );
    await db.run(updateQuery);
    return existingTag.id;
  }

  const upsertQuery = insertInto(Tag.table, values).onConflict(
    "name",
    "UPDATE"
  );
  await db.run(upsertQuery);

  const tag = await db.get(selectQuery);
  return tag.id;
};

export default upsertTag;
