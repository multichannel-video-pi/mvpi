import database, { insertInto, selectFrom, update } from "@mvpi/database";
import { TelevisionShow } from "@mvpi/models";

const upsertTelevisionShow = async (tvShowDetails, televisionShowId) => {
  const db = await database();

  const values = {
    backdropPath: tvShowDetails.backdrop_path || null,
    firstAirDate: tvShowDetails.first_air_date || null,
    lastAirDate: tvShowDetails.last_air_date || null,
    name: tvShowDetails.name || null,
    overview: tvShowDetails.overview || null,
    posterPath: tvShowDetails.poster_path || null,
    tagline: tvShowDetails.tagline || null,
    tmdbId: tvShowDetails.id || null,
    voteAverage: tvShowDetails.vote_average || null,
    voteCount: tvShowDetails.vote_count || null,
  };

  if (televisionShowId) {
    const updateQuery = update(TelevisionShow.table, values).where(
      TelevisionShow.getConditions({ id: televisionShowId })
    );
    await db.run(updateQuery);
    return televisionShowId;
  }

  const upsertQuery = insertInto(TelevisionShow.table, values).onConflict(
    "tmdbId",
    "UPDATE"
  );
  await db.run(upsertQuery);

  const selectQuery = selectFrom(TelevisionShow.table)
    .fields({
      id: TelevisionShow.column`id`,
    })
    .where(TelevisionShow.getConditions({ tmdbId: values.tmdbId }));

  const { id } = await db.get(selectQuery);
  return id;
};

export default upsertTelevisionShow;
