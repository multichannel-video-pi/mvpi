#!/usr/bin/env node

import sync from "./sync.js";
import logger from "./logger.js";

(async () => {
  try {
    await sync();
    process.exit(0);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
