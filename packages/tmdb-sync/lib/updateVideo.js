import database, { update } from "@mvpi/database";
import { Video } from "@mvpi/models";

const updateVideo = async (details, videoId) => {
  const db = await database();
  const isTVShow = !!details.name;

  const values = isTVShow
    ? {
        overview: details.overview || null,
        releaseDate: details.air_date || null,
        title: details.name || null,
        voteAverage: details.vote_average || null,
        voteCount: details.vote_count || null,
      }
    : {
        overview: details.overview || null,
        releaseDate: details.release_date || null,
        title: details.title || null,
        voteAverage: details.vote_average || null,
        voteCount: details.vote_count || null,
      };

  const conditions = Video.getConditions({ id: videoId });
  const updateQuery = update(Video.table, values).where(conditions);
  await db.run(updateQuery);

  const [video] = await Video.getDenormalizedVideos(conditions);
  return video;
};

export default updateVideo;
