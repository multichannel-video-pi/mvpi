import database, { insertInto, selectFrom, update } from "@mvpi/database";
import { Movie } from "@mvpi/models";

const upsertMovie = async (movieDetails, collectionId, movieId, videoId) => {
  const db = await database();

  const values = {
    backdropPath: movieDetails.backdrop_path || null,
    posterPath: movieDetails.poster_path || null,
    tagline: movieDetails.tagline || null,
    tmdbId: movieDetails.id || null,
    collectionId,
    videoId,
  };

  if (movieId) {
    const updateQuery = update(Movie.table, values).where(
      Movie.getConditions({ id: movieId })
    );
    await db.run(updateQuery);
    return movieId;
  }

  const upsertQuery = insertInto(Movie.table, values).onConflict(
    ["collectionId", "chronology"],
    "UPDATE"
  );
  await db.run(upsertQuery);

  const selectQuery = selectFrom(Movie.table)
    .fields({
      id: Movie.column`id`,
    })
    .where(Movie.getConditions({ videoId }));

  const { id } = await db.get(selectQuery);
  return id;
};

export default upsertMovie;
