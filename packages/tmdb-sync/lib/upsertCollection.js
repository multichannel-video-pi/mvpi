import database, { insertInto, selectFrom, update } from "@mvpi/database";
import { Collection } from "@mvpi/models";

const upsertCollection = async (collectionDetails, collectionId) => {
  const db = await database();

  const values = {
    backdropPath: collectionDetails.backdrop_path || null,
    name: collectionDetails.name || null,
    overview: collectionDetails.overview || null,
    posterPath: collectionDetails.poster_path || null,
    tmdbId: collectionDetails.id || null,
  };

  if (collectionId) {
    const updateQuery = update(Collection.table, values).where(
      Collection.getConditions({ id: collectionId })
    );
    await db.run(updateQuery);
    return collectionId;
  }

  const upsertQuery = insertInto(Collection.table, values).onConflict(
    "tmdbId",
    "UPDATE"
  );
  await db.run(upsertQuery);

  const selectQuery = selectFrom(Collection.table)
    .fields({
      id: Collection.column`id`,
    })
    .where(Collection.getConditions({ tmdbId: values.tmdbId }));

  const { id } = await db.get(selectQuery);
  return id;
};

export default upsertCollection;
