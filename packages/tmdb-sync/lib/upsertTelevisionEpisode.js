import database, { insertInto, selectFrom, update } from "@mvpi/database";
import { TelevisionEpisode } from "@mvpi/models";

const upsertTelevisionEpisode = async (
  tvEpisodeDetails,
  televisionShowId,
  televisionEpisodeId,
  videoId
) => {
  const db = await database();

  const values = {
    episodeNumber: tvEpisodeDetails.episode_number || null,
    seasonNumber: tvEpisodeDetails.season_number || null,
    stillPath: tvEpisodeDetails.still_path || null,
    televisionShowId,
    videoId,
  };

  if (televisionEpisodeId) {
    const updateQuery = update(TelevisionEpisode.table, values).where(
      TelevisionEpisode.getConditions({ id: televisionEpisodeId })
    );
    await db.run(updateQuery);
    return televisionEpisodeId;
  }

  const upsertQuery = insertInto(TelevisionEpisode.table, values).onConflict(
    ["televisionShowId", "seasonNumber", "episodeNumber"],
    "UPDATE"
  );
  await db.run(upsertQuery);

  const selectQuery = selectFrom(TelevisionEpisode.table)
    .fields({
      id: TelevisionEpisode.column`id`,
    })
    .where(TelevisionEpisode.getConditions({ videoId }));

  const { id } = await db.get(selectQuery);
  return id;
};

export default upsertTelevisionEpisode;
