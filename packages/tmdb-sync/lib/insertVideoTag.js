import database, { insertInto, selectFrom } from "@mvpi/database";
import { VideoTag } from "@mvpi/models";

const upsertVideoTag = async (videoId, tagId) => {
  const db = await database();

  const values = {
    tagId,
    videoId,
  };

  const selectQuery = selectFrom(VideoTag.table).where(
    VideoTag.getConditions(values)
  );
  const videoTag = await db.get(selectQuery);

  if (videoTag) return videoTag.id;

  const insertQuery = insertInto(VideoTag.table, values);
  const { lastID } = await db.run(insertQuery);
  return lastID;
};

export default upsertVideoTag;
