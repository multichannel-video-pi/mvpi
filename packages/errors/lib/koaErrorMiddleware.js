import {
  BAD_REQUEST_ERROR,
  CONFLICT_ERROR,
  FORBIDDEN_ERROR,
  NOT_FOUND_ERROR,
  UNAUTHORIZED_ERROR,
  VALIDATION_ERROR,
} from "./keys.js";

const koaErrorMiddleware = (logger) => async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    logger.error(err.stack);

    switch (err.name) {
      case BAD_REQUEST_ERROR:
        ctx.status = 400;
        break;
      case UNAUTHORIZED_ERROR:
        ctx.status = 401;
        break;
      case FORBIDDEN_ERROR:
        ctx.status = 403;
        break;
      case NOT_FOUND_ERROR:
        ctx.status = 404;
        break;
      case CONFLICT_ERROR:
        ctx.status = 409;
        break;
      case VALIDATION_ERROR:
        ctx.status = 422;
        break;
      default:
        ctx.status = 500;
        break;
    }

    ctx.body = err.message;
  }
};

export default koaErrorMiddleware;
