import namedError from "./namedError.js";

import {
  BAD_REQUEST_ERROR,
  CONFLICT_ERROR,
  FORBIDDEN_ERROR,
  NOT_FOUND_ERROR,
  UNAUTHORIZED_ERROR,
  VALIDATION_ERROR,
} from "./keys.js";

export const BadRequestError = namedError(BAD_REQUEST_ERROR, "Bad Request");
export const ConflictError = namedError(CONFLICT_ERROR, "Conflict");
export const ForbiddenError = namedError(FORBIDDEN_ERROR, "Forbidden");
export const NotFoundError = namedError(NOT_FOUND_ERROR, "Not Found");
export const ValidationError = namedError(VALIDATION_ERROR, "Invalid");
export const UnauthorizedError = namedError(UNAUTHORIZED_ERROR, "Unauthorized");
