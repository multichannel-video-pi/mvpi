const namedError = (name, defaultMessage) =>
  class extends Error {
    constructor(...args) {
      super(...args);
      this.message = this.message || defaultMessage || `Error: ${name}`;
      this.name = name;
    }
  };

export default namedError;
