const STREAM_ERROR_CODES = ["ECANCELED", "ECONNRESET", "EPIPE"];

const koaErrorHandler = (logger) => (err) => {
  // don't log errors for terminated video streams
  // see: https://github.com/koajs/koa/issues/1089
  if (!STREAM_ERROR_CODES.includes(err.code)) logger.error(err);
};

export default koaErrorHandler;
