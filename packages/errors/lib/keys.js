import { key } from "@mvpi/util";

export const BAD_REQUEST_ERROR = key`BadRequestError`;
export const CONFLICT_ERROR = key`ConflictError`;
export const FORBIDDEN_ERROR = key`ForbiddenError`;
export const NOT_FOUND_ERROR = key`NotFoundError`;
export const VALIDATION_ERROR = key`ValidationError`;
export const UNAUTHORIZED_ERROR = key`UnauthorizedError`;
