/** @jsxImportSource @emotion/react */
import { css, Global } from "@emotion/react";
import config from "@mvpi/ui-config";
import { RemoteContextProvider } from "@mvpi/ui-remote-client";
import styles, { BLACK_TER } from "@mvpi/ui-styles";
import ReactDOM from "react-dom";

import "reset-css";

import App from "./App.js";
import reportWebVitals from "./reportWebVitals.js";

ReactDOM.render(
  // <React.StrictMode>
  //   <RemoteContextProvider config={config}>
  //     <Global styles={styles} />
  //     <App />
  //   </RemoteContextProvider>
  // </React.StrictMode>,
  <RemoteContextProvider config={config}>
    <Global styles={styles} />
    <Global
      styles={css`
        body {
          background-color: ${BLACK_TER};
          touch-action: manipulation;
          text-size-adjust: none;
          overflow: hidden;
        }
      `}
    />
    <App />
  </RemoteContextProvider>,
  document.getElementById("root")
);

document.addEventListener("gesturestart", (e) => e.preventDefault());

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
