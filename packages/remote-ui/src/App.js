/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { RemoteControl } from "@mvpi/ui-remote-client";

const App = () => {
  return (
    <div
      css={css`
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100vw;
        height: 100vh;
      `}
    >
      <RemoteControl />
    </div>
  );
};

export default App;
