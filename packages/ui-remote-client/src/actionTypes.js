import { key } from "@mvpi/util";

export const REMOTE_CONTROL_EXIT = key`REMOTE_CONTROL_EXIT`;
export const REMOTE_CONTROL_OKAY = key`REMOTE_CONTROL_OKAY`;

export const REMOTE_CONTROL_TOGGLE_FULLSCREEN = key`REMOTE_CONTROL_TOGGLE_FULLSCREEN`;
export const REMOTE_CONTROL_TOGGLE_GUIDE = key`REMOTE_CONTROL_TOGGLE_GUIDE`;
export const REMOTE_CONTROL_TOGGLE_INFO = key`REMOTE_CONTROL_TOGGLE_INFO`;
export const REMOTE_CONTROL_TOGGLE_POWER = key`REMOTE_CONTROL_TOGGLE_POWER`;
export const REMOTE_CONTROL_TOGGLE_REMOTE = key`REMOTE_CONTROL_TOGGLE_REMOTE`;

// channel controls
export const REMOTE_CONTROL_CHANNEL_DOWN = key`REMOTE_CONTROL_CHANNEL_DOWN`;
export const REMOTE_CONTROL_CHANNEL_UP = key`REMOTE_CONTROL_CHANNEL_UP`;

// direction controls
export const REMOTE_CONTROL_MOVE_DOWN = key`REMOTE_CONTROL_MOVE_DOWN`;
export const REMOTE_CONTROL_MOVE_LEFT = key`REMOTE_CONTROL_MOVE_LEFT`;
export const REMOTE_CONTROL_MOVE_RIGHT = key`REMOTE_CONTROL_MOVE_RIGHT`;
export const REMOTE_CONTROL_MOVE_UP = key`REMOTE_CONTROL_MOVE_UP`;

// volume controls
export const REMOTE_CONTROL_VOLUME_DOWN = key`REMOTE_CONTROL_VOLUME_DOWN`;
export const REMOTE_CONTROL_VOLUME_MUTE = key`REMOTE_CONTROL_VOLUME_MUTE`;
export const REMOTE_CONTROL_VOLUME_UP = key`REMOTE_CONTROL_VOLUME_UP`;
