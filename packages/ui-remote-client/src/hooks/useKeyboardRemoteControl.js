import { useEffect } from "react";

import useRemote from "./useRemote.js";

import {
  REMOTE_CONTROL_CHANNEL_DOWN,
  REMOTE_CONTROL_CHANNEL_UP,
  REMOTE_CONTROL_MOVE_DOWN,
  REMOTE_CONTROL_MOVE_LEFT,
  REMOTE_CONTROL_MOVE_RIGHT,
  REMOTE_CONTROL_MOVE_UP,
  REMOTE_CONTROL_TOGGLE_FULLSCREEN,
  REMOTE_CONTROL_TOGGLE_GUIDE,
  REMOTE_CONTROL_TOGGLE_INFO,
  REMOTE_CONTROL_TOGGLE_POWER,
  REMOTE_CONTROL_TOGGLE_REMOTE,
  REMOTE_CONTROL_VOLUME_DOWN,
  REMOTE_CONTROL_VOLUME_MUTE,
  REMOTE_CONTROL_VOLUME_UP,
} from "../actionTypes.js";

const REGISTERED_KEYS = {
  LEFT_ARROW: 37,
  UP_ARROW: 38,
  RIGHT_ARROW: 39,
  DOWN_ARROW: 40,
  F: 70,
  G: 71,
  I: 73,
  M: 77,
  P: 80,
  R: 82,
  EQUAL: 187,
  COMMA: 188,
  MINUS: 189,
  PERIOD: 190,
};

const REGISTERED_KEY_CODES = Object.values(REGISTERED_KEYS);

const {
  LEFT_ARROW,
  UP_ARROW,
  RIGHT_ARROW,
  DOWN_ARROW,
  F,
  G,
  I,
  M,
  P,
  R,
  EQUAL,
  COMMA,
  MINUS,
  PERIOD,
} = REGISTERED_KEYS;

const useKeyboardRemoteControl = () => {
  const { dispatch } = useRemote();

  return useEffect(() => {
    const onKeyDown = (e) => {
      if (!REGISTERED_KEY_CODES.includes(e.keyCode)) return null;
      e.preventDefault();

      switch (e.keyCode) {
        case LEFT_ARROW:
          return dispatch({ type: REMOTE_CONTROL_MOVE_LEFT });
        case UP_ARROW:
          return dispatch({ type: REMOTE_CONTROL_MOVE_UP });
        case RIGHT_ARROW:
          return dispatch({ type: REMOTE_CONTROL_MOVE_RIGHT });
        case DOWN_ARROW:
          return dispatch({ type: REMOTE_CONTROL_MOVE_DOWN });
        case F:
          return dispatch({ type: REMOTE_CONTROL_TOGGLE_FULLSCREEN });
        case G:
          return dispatch({ type: REMOTE_CONTROL_TOGGLE_GUIDE });
        case I:
          return dispatch({ type: REMOTE_CONTROL_TOGGLE_INFO });
        case M:
          return dispatch({ type: REMOTE_CONTROL_VOLUME_MUTE });
        case P:
          return dispatch({ type: REMOTE_CONTROL_TOGGLE_POWER });
        case R:
          return dispatch({ type: REMOTE_CONTROL_TOGGLE_REMOTE });
        case EQUAL:
          return dispatch({ type: REMOTE_CONTROL_VOLUME_UP });
        case COMMA:
          return dispatch({ type: REMOTE_CONTROL_CHANNEL_DOWN });
        case MINUS:
          return dispatch({ type: REMOTE_CONTROL_VOLUME_DOWN });
        case PERIOD:
          return dispatch({ type: REMOTE_CONTROL_CHANNEL_UP });

        default:
          return null;
      }
    };

    document.addEventListener("keydown", onKeyDown);
    return () => {
      document.removeEventListener("keydown", onKeyDown);
    };
  }, [dispatch]);
};

export default useKeyboardRemoteControl;
