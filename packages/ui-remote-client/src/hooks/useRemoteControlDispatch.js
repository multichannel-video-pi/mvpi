import { useCallback } from "react";

import {
  REMOTE_CONTROL_EXIT,
  REMOTE_CONTROL_OKAY,
  REMOTE_CONTROL_TOGGLE_FULLSCREEN,
  REMOTE_CONTROL_TOGGLE_GUIDE,
  REMOTE_CONTROL_TOGGLE_INFO,
  REMOTE_CONTROL_TOGGLE_POWER,
  REMOTE_CONTROL_TOGGLE_REMOTE,
  REMOTE_CONTROL_CHANNEL_DOWN,
  REMOTE_CONTROL_CHANNEL_UP,
  REMOTE_CONTROL_MOVE_DOWN,
  REMOTE_CONTROL_MOVE_LEFT,
  REMOTE_CONTROL_MOVE_RIGHT,
  REMOTE_CONTROL_MOVE_UP,
  REMOTE_CONTROL_VOLUME_DOWN,
  REMOTE_CONTROL_VOLUME_MUTE,
  REMOTE_CONTROL_VOLUME_UP,
} from "../actionTypes.js";

const useRemoteControlDispatch = ({
  setGuideChannelBack,
  setGuideChannelForward,
  setGuideTimeBack,
  setGuideTimeForward,
  setMute,
  setPlayerChannelBack,
  setPlayerChannelForward,
  setPower,
  setVisibility,
  setVolumeDown,
  setVolumeUp,
  visible,
}) => {
  return useCallback(
    (action) => {
      switch (action.type) {
        case REMOTE_CONTROL_EXIT:
          if (visible.guide)
            return setVisibility({ view: "guide", visible: false });
          if (visible.infoBar)
            return setVisibility({ view: "infoBar", visible: false });
          return null;
        case REMOTE_CONTROL_OKAY:
          return null;

        // toggle controls
        case REMOTE_CONTROL_TOGGLE_FULLSCREEN:
          return document.body.requestFullscreen();
        case REMOTE_CONTROL_TOGGLE_GUIDE:
          return setVisibility({ view: "guide" });
        case REMOTE_CONTROL_TOGGLE_INFO:
          return setVisibility({ view: "infoBar" });
        case REMOTE_CONTROL_TOGGLE_POWER:
          return setPower((power) => !power);
        case REMOTE_CONTROL_TOGGLE_REMOTE:
          return setVisibility({ view: "remote" });

        // channel controls
        case REMOTE_CONTROL_CHANNEL_DOWN:
          return setPlayerChannelBack();
        case REMOTE_CONTROL_CHANNEL_UP:
          return setPlayerChannelForward();

        // direction controls
        case REMOTE_CONTROL_MOVE_DOWN:
          if (visible.guide) return setGuideChannelForward();
          return null;
        case REMOTE_CONTROL_MOVE_LEFT:
          if (visible.guide) return setGuideTimeBack();
          return null;
        case REMOTE_CONTROL_MOVE_RIGHT:
          if (visible.guide) return setGuideTimeForward();
          return null;
        case REMOTE_CONTROL_MOVE_UP:
          if (visible.guide) return setGuideChannelBack();
          return null;

        // volume controls
        case REMOTE_CONTROL_VOLUME_DOWN:
          return setVolumeDown();
        case REMOTE_CONTROL_VOLUME_MUTE:
          return setMute((mute) => !mute);
        case REMOTE_CONTROL_VOLUME_UP:
          return setVolumeUp();

        default:
          return null;
      }
    },
    [
      setGuideChannelBack,
      setGuideChannelForward,
      setGuideTimeBack,
      setGuideTimeForward,
      setMute,
      setPlayerChannelBack,
      setPlayerChannelForward,
      setPower,
      setVisibility,
      setVolumeDown,
      setVolumeUp,
      visible,
    ]
  );
};

export default useRemoteControlDispatch;
