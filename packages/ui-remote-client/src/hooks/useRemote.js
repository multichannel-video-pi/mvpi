import { useContext } from "react";
import RemoteContext from "../contexts/RemoteContext.js";

const useRemote = () => useContext(RemoteContext);

export default useRemote;
