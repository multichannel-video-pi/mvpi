import { useEffect } from "react";
import { devLogger as logger } from "@mvpi/util";

import useRemote from "./useRemote.js";

const useRemoteServiceActions = () => {
  const { client, dispatch } = useRemote();

  return useEffect(() => {
    client.onmessage = (message) => {
      try {
        dispatch(JSON.parse(message.data));
      } catch (err) {
        logger.error(`failed to parse remote message data: ${message?.data}`);
        logger.error(err);
      }
    };
  }, [client, dispatch]);
};

export default useRemoteServiceActions;
