/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import {
  faCaretDown,
  faCaretLeft,
  faCaretRight,
  faCaretUp,
} from "@fortawesome/free-solid-svg-icons";

import RemoteControlButton from "./RemoteControlButton.js";

import {
  REMOTE_CONTROL_MOVE_DOWN,
  REMOTE_CONTROL_MOVE_LEFT,
  REMOTE_CONTROL_MOVE_RIGHT,
  REMOTE_CONTROL_MOVE_UP,
  REMOTE_CONTROL_OKAY,
} from "../actionTypes.js";

const RemoteControlDirectionControls = () => (
  <div
    css={css`
      display: inline-block;
    `}
  >
    <div>
      <RemoteControlButton icon={faCaretUp} type={REMOTE_CONTROL_MOVE_UP} />
    </div>
    <div
      css={css`
        display: flex;
        align-content: center;
      `}
    >
      <RemoteControlButton icon={faCaretLeft} type={REMOTE_CONTROL_MOVE_LEFT} />
      <RemoteControlButton type={REMOTE_CONTROL_OKAY}>OK</RemoteControlButton>
      <RemoteControlButton
        icon={faCaretRight}
        type={REMOTE_CONTROL_MOVE_RIGHT}
      />
    </div>
    <div>
      <RemoteControlButton icon={faCaretDown} type={REMOTE_CONTROL_MOVE_DOWN} />
    </div>
  </div>
);

export default RemoteControlDirectionControls;
