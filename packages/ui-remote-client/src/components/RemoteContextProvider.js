import { RemoteClient } from "@mvpi/remote-client";
import { devLogger as logger } from "@mvpi/util";
import React, { useCallback, useEffect, useMemo } from "react";

import RemoteContext from "../contexts/RemoteContext.js";

const RemoteContextProvider = ({
  children,
  config,
  dispatch: consumerDispatch,
}) => {
  const client = useMemo(() => new RemoteClient(config, logger), [config]);

  const clientDispatch = useCallback((action) => client.dispatch(action), [
    client,
  ]);

  const dispatch =
    typeof consumerDispatch === "function" ? consumerDispatch : clientDispatch;

  useEffect(
    () => () => {
      client.close();
    },
    [client]
  );

  return (
    <RemoteContext.Provider value={{ client, dispatch }}>
      {children}
    </RemoteContext.Provider>
  );
};

export default RemoteContextProvider;
