/** @jsx jsx */
import { css, jsx } from "@emotion/react";

import {
  faInfo,
  faPowerOff,
  faTable,
  faTimes,
  faVolumeMute,
} from "@fortawesome/free-solid-svg-icons";

import {
  BLACK_TER,
  RED,
  RED_DARK,
  RED_LIGHT,
  WHITE_BIS,
} from "@mvpi/ui-styles";

import {
  REMOTE_CONTROL_EXIT,
  REMOTE_CONTROL_TOGGLE_GUIDE,
  REMOTE_CONTROL_TOGGLE_INFO,
  REMOTE_CONTROL_TOGGLE_POWER,
  REMOTE_CONTROL_VOLUME_MUTE,
} from "../actionTypes.js";

import RemoteControlButton from "./RemoteControlButton.js";
import RemoteControlChannelControls from "./RemoteControlChannelControls.js";
import RemoteControlDirectionControls from "./RemoteControlDirectionControls.js";
import RemoteControlVolumeControls from "./RemoteControlVolumeControls.js";

const RemoteControl = () => (
  <section
    css={css`
      padding: 2rem 2rem 4rem;
      max-width: 320px;
      background-color: ${BLACK_TER};
      color: ${WHITE_BIS};
      text-align: center;
    `}
  >
    <div
      css={css`
        margin: 2rem 0;
      `}
    >
      <RemoteControlButton
        icon={faTimes}
        styleOverrides={css`
          margin: 0 0.125rem;
          width: 7rem;
        `}
        type={REMOTE_CONTROL_EXIT}
      >
        Exit
      </RemoteControlButton>
      <RemoteControlButton
        icon={faPowerOff}
        styleOverrides={css`
          margin: 0 0.125rem;
          border-color: ${RED};
          background-color: ${RED_DARK};
          width: 7rem;

          &:hover,
          &:focus {
            background-color: ${RED};
          }

          &:active {
            border-color: ${RED_LIGHT};
            background-color: ${RED_LIGHT};
          }
        `}
        type={REMOTE_CONTROL_TOGGLE_POWER}
      >
        Power
      </RemoteControlButton>
    </div>

    <div
      css={css`
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 2rem 0;
      `}
    >
      <RemoteControlVolumeControls />
      <RemoteControlButton
        icon={faVolumeMute}
        styleOverrides={css`
          margin: 0 0.5rem;
        `}
        type={REMOTE_CONTROL_VOLUME_MUTE}
      >
        Mute
      </RemoteControlButton>
      <RemoteControlChannelControls />
    </div>

    <div
      css={css`
        margin: 2rem 0;
      `}
    >
      <RemoteControlButton
        icon={faInfo}
        styleOverrides={css`
          margin: 0 0.125rem;
          width: 7rem;
        `}
        type={REMOTE_CONTROL_TOGGLE_INFO}
      >
        Info
      </RemoteControlButton>
      <RemoteControlButton
        icon={faTable}
        styleOverrides={css`
          margin: 0 0.125rem;
          width: 7rem;
        `}
        type={REMOTE_CONTROL_TOGGLE_GUIDE}
      >
        Guide
      </RemoteControlButton>
    </div>

    <div
      css={css`
        margin: 2rem 0;
      `}
    >
      <RemoteControlDirectionControls />
    </div>
  </section>
);

export default RemoteControl;
