/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";

import RemoteControlButton from "./RemoteControlButton.js";

import {
  REMOTE_CONTROL_VOLUME_DOWN,
  REMOTE_CONTROL_VOLUME_UP,
} from "../actionTypes.js";

const RemoteControlVolumeControls = () => (
  <div
    css={css`
      display: inline-block;
    `}
  >
    <RemoteControlButton icon={faPlus} type={REMOTE_CONTROL_VOLUME_UP} />
    <div>VOL</div>
    <RemoteControlButton icon={faMinus} type={REMOTE_CONTROL_VOLUME_DOWN} />
  </div>
);

export default RemoteControlVolumeControls;
