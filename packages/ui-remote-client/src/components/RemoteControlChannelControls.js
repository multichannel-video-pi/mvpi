/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { faChevronUp, faChevronDown } from "@fortawesome/free-solid-svg-icons";

import RemoteControlButton from "./RemoteControlButton.js";

import {
  REMOTE_CONTROL_CHANNEL_DOWN,
  REMOTE_CONTROL_CHANNEL_UP,
} from "../actionTypes.js";

const RemoteControlChannelControls = () => (
  <div
    css={css`
      display: inline-block;
    `}
  >
    <RemoteControlButton icon={faChevronUp} type={REMOTE_CONTROL_CHANNEL_UP} />
    <div>CH</div>
    <RemoteControlButton
      icon={faChevronDown}
      type={REMOTE_CONTROL_CHANNEL_DOWN}
    />
  </div>
);

export default RemoteControlChannelControls;
