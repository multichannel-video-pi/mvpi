/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { BLACK_TER, GREY, GREY_LIGHT, buttonStyles } from "@mvpi/ui-styles";

import useRemote from "../hooks/useRemote.js";

const RemoteControlButton = ({ children, icon, styleOverrides, type }) => {
  const { dispatch } = useRemote();

  return (
    <button
      css={css`
        ${buttonStyles}

        border-radius: 1rem;
        border-color: ${GREY};
        padding: 0.75rem 1rem;
        background-color: ${BLACK_TER};
        text-transform: uppercase;

        &:hover,
        &:focus {
          background-color: ${GREY};
        }

        &:active {
          border-color: ${GREY_LIGHT};
          background-color: ${GREY_LIGHT};
        }

        ${styleOverrides}
      `}
      type="button"
      onClick={() => dispatch({ type })}
    >
      {children}
      {icon && (
        <div
          css={css`
            display: flex;
            align-content: center;
            justify-content: center;
            padding: 0.25rem;
            font-size: 1.25rem;
          `}
        >
          <FontAwesomeIcon icon={icon} />
        </div>
      )}
    </button>
  );
};

export default RemoteControlButton;
