import config from "@mvpi/config";
import database, { insertInto, selectFrom } from "@mvpi/database";
import { NotFoundError } from "@mvpi/errors";
import { screenshots } from "@mvpi/ffmpeg";
import { ensureWritableDirectory } from "@mvpi/fs";
import { serialAsyncMap } from "@mvpi/util";
import path from "path";

import {
  column as videosColumn,
  getConditions as getVideoConditions,
  VIDEOS_TABLE,
} from "@mvpi/videos";

import {
  column as screenshotsColumn,
  selectScreenshots,
} from "./tableUtilities.js";

import { SCREENSHOTS_TABLE } from "./constants.js";

const createScreenshots = async ({ videoId }, info) => {
  const db = await database();

  const videoQuery = selectFrom(VIDEOS_TABLE)
    .fields({ id: videosColumn`id`, filename: videosColumn`filename` })
    .where(getVideoConditions({ id: videoId }));

  const video = await db.get(videoQuery);
  if (!video) throw new NotFoundError(`video not found in ${VIDEOS_TABLE}`);

  const basename = path.basename(video.filename, path.extname(video.filename));
  const directory = path.join(config.screenshots.directory, basename);
  await ensureWritableDirectory(directory);

  const filenames = await screenshots(video.filename, {
    count: config.screenshots.count,
    filename: "%i",
    folder: directory,
    size: config.screenshots.size,
  });

  await serialAsyncMap(filenames, async (filename) => {
    const screenshot = { filename, videoId: video.id };
    const upsertQuery = insertInto(SCREENSHOTS_TABLE, screenshot).onConflict(
      "filename",
      "NOTHING"
    );
    return db.run(upsertQuery);
  });

  const conditions = [[screenshotsColumn`filename`, "IN", filenames]];
  return selectScreenshots(conditions, null, info);
};

export default createScreenshots;
