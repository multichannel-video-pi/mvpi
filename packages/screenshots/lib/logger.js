import config from "@mvpi/config";
import pino from "pino";

const logger = pino({ ...config.logger, name: "screenshots" });

export default logger;
