import { createTableUtilities } from "@mvpi/database";
import { SCREENSHOTS_TABLE } from "./constants.js";

export const {
  delete: deleteScreenshotsORIGINAL,
  deleteByKey: deleteScreenshot,
  insert: insertScreenshot,
  select: selectScreenshots,
  selectByKey: selectScreenshot,
  update: updateScreenshots,
  updateByKey: updateScreenshot,
  column,
  getConditions,
  getOrder,
  getPrimaryKeyConditions,
} = createTableUtilities(SCREENSHOTS_TABLE);
