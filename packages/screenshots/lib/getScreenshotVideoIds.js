import database, { selectDistinctFrom } from "@mvpi/database";

import { SCREENSHOTS_TABLE } from "./constants.js";
import { column, getOrder } from "./tableUtilities.js";

const getScreenshotVideoIds = async (conditions) => {
  const db = await database();
  const fields = { videoId: column`videoId` };
  const order = getOrder({ orderBy: { videoId: "ASC" } });

  const query = selectDistinctFrom(SCREENSHOTS_TABLE)
    .fields(fields)
    .where(conditions)
    .orderBy(order);

  const records = await db.all(query);
  return records.map(({ videoId }) => videoId);
};

export default getScreenshotVideoIds;
