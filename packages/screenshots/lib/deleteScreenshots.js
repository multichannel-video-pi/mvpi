import { deleteDirectoryIfEmpty, deleteFile } from "@mvpi/fs";
import path from "path";

import database, {
  deleteFrom,
  mergeAndUnflatten,
  parseInfo,
  selectFrom,
} from "@mvpi/database";

import { SCREENSHOTS_TABLE } from "./constants.js";
import logger from "./logger.js";
import { column, getConditions } from "./tableUtilities.js";

const deleteScreenshots = async (conditions, order, info) => {
  const db = await database();

  const { fields, joins, nested, nestedKeys } = await parseInfo(
    info,
    conditions,
    SCREENSHOTS_TABLE
  );

  const selectQuery = selectFrom(SCREENSHOTS_TABLE)
    .fields(fields)
    .fields({ id: column`id`, filename: column`filename` })
    .leftJoins(joins)
    .where(conditions)
    .orderBy(order);

  const screenshots = await db.all(selectQuery);
  const directories = [
    ...new Set(
      screenshots.map((screenshot) => path.dirname(screenshot.filename))
    ),
  ];

  logger.debug(`deleting screenshot files`);
  await Promise.all(
    screenshots.map(async ({ id, filename }) => {
      await deleteFile(filename);
      const deleteConditions = getConditions({ id });
      const deleteQuery = deleteFrom(SCREENSHOTS_TABLE).where(deleteConditions);
      return db.run(deleteQuery);
    })
  );

  logger.debug(`deleting empty screenshot directories`);
  await Promise.all(
    directories.map(async (directory) => deleteDirectoryIfEmpty(directory))
  );

  return screenshots.map((screenshot) =>
    mergeAndUnflatten(screenshot, nested, nestedKeys)
  );
};

export default deleteScreenshots;
