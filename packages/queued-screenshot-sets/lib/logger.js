import config from "@mvpi/config";
import pino from "pino";

const logger = pino({ ...config.logger, name: "queued-screenshot-sets" });

export default logger;
