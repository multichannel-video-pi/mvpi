import database, { deleteFrom, selectFrom, update } from "@mvpi/database";
import { createScreenshots } from "@mvpi/screenshots";
import { isActiveProcess } from "@mvpi/util";
import { EventEmitter } from "events";

import { QUEUED_SCREENSHOT_SETS_TABLE } from "./constants.js";
import logger from "./logger.js";
import { column, getConditions } from "./tableUtilities.js";

const PROCESS_NEXT = Symbol("PROCESS_NEXT");

const STARTED = "started";
const STOPPED = "stopped";

class QueuedSceenshotSetProcessor extends EventEmitter {
  #activeCount = 0;
  #concurrent;
  #delay;
  #onDeactivate;
  #onError;
  #onScreenshots;
  #state = STOPPED;

  constructor(opts) {
    super();
    this.#concurrent = opts?.concurrent || 2;
    this.#delay = opts?.delay || 60000;

    this.#onDeactivate = (inactive) => {
      this.#activeCount = this.#activeCount - inactive;
      if (this.#activeCount === 0) {
        this.stop();
        this.emit("end");
      }
    };

    this.#onError = (error) => {
      logger.error(error);
      this.stop();
    };

    this.#onScreenshots = () => {
      setTimeout(() => {
        this[PROCESS_NEXT](1).catch((error) => {
          this.emit("error", error);
        });
      }, this.#delay);
    };
  }

  async [PROCESS_NEXT](limit) {
    const db = await database();
    await QueuedSceenshotSetProcessor.cleanUp();

    const query = selectFrom(QUEUED_SCREENSHOT_SETS_TABLE)
      .where(getConditions({ isCreating: 0 }))
      .limit(limit);

    const queuedScreenshotSets = await db.all(query);
    queuedScreenshotSets.forEach((queuedScreenshotSet) =>
      QueuedSceenshotSetProcessor.generateScreenshots(queuedScreenshotSet)
        .then((screenshots) => this.emit("screenshots", screenshots))
        .catch((error) => this.emit("error", error))
    );

    const inactiveCount = limit - queuedScreenshotSets.length;
    if (inactiveCount > 0) this.emit("deactivate", inactiveCount);

    return this;
  }

  start() {
    if (this.#state === STARTED) throw new Error("processor already started");
    logger.debug("starting queued screenshot set processor");
    this.#activeCount = this.#concurrent;
    this.#state = STARTED;
    this.on("deactivate", this.#onDeactivate);
    this.on("error", this.#onError);
    this.on("screenshots", this.#onScreenshots);
    this[PROCESS_NEXT](this.#concurrent).catch((error) => {
      this.emit("error", error);
    });
    return this;
  }

  stop() {
    if (this.#state === STOPPED) throw new Error("processor already stopped");
    logger.debug("stopping queued screenshot set processor");
    this.#activeCount = 0;
    this.#state = STOPPED;
    this.off("deactivate", this.#onDeactivate);
    this.off("error", this.#onError);
    this.off("screenshots", this.#onScreenshots);
    return this;
  }

  static async cleanUp() {
    const db = await database();
    const selectQuery = selectFrom(QUEUED_SCREENSHOT_SETS_TABLE).where(
      getConditions({ isCreating: 1 })
    );
    const queuedScreenshotSets = await db.all(selectQuery);

    const ids = queuedScreenshotSets
      .filter(({ pid }) => !isActiveProcess(pid))
      .map(({ id }) => id);

    if (!ids.length) return [];

    const updateQuery = update(QUEUED_SCREENSHOT_SETS_TABLE, {
      isCreating: 0,
      pid: null,
    }).where([[column`id`, "IN", ids]]);

    await db.run(updateQuery);
    return ids;
  }

  static async generateScreenshots(queuedScreenshotSet) {
    const db = await database();
    const { id, videoId } = queuedScreenshotSet;

    const updateQuery = update(QUEUED_SCREENSHOT_SETS_TABLE, {
      isCreating: 1,
      pid: process.pid,
    }).where(getConditions({ id }));

    const [, screenshots] = await Promise.all([
      db.run(updateQuery),
      createScreenshots({ videoId }),
    ]);

    const deleteQuery = deleteFrom(QUEUED_SCREENSHOT_SETS_TABLE).where(
      getConditions({ id })
    );

    await db.run(deleteQuery);
    return screenshots;
  }
}

export default QueuedSceenshotSetProcessor;
