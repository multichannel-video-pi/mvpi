import { createTableUtilities } from "@mvpi/database";
import { QUEUED_SCREENSHOT_SETS_TABLE } from "./constants.js";

export const {
  delete: deleteQueuedScreenshotSets,
  deleteByKey: deleteQueuedScreenshotSet,
  insert: insertQueuedScreenshotSet,
  select: selectQueuedScreenshotSets,
  selectByKey: selectQueuedScreenshotSet,
  update: updateQueuedScreenshotSets,
  updateByKey: updateQueuedScreenshotSet,
  column,
  getConditions,
  getOrder,
  getPrimaryKeyConditions,
} = createTableUtilities(QUEUED_SCREENSHOT_SETS_TABLE);
