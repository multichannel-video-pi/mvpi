import { Backdrop, NoData } from "@mvpi/ui-helpers";
import Guide from "@mvpi/ui-guide";
import { useStreams } from "@mvpi/ui-media-client";
import { useSchedules } from "@mvpi/ui-metadata-client";
import React from "react";

import InfoBar from "./components/InfoBar.js";
import PowerGate from "./components/PowerGate.js";
import RemoteControl from "./components/RemoteControl.js";
import VideoPlayer from "./components/VideoPlayer.js";

const App = () => {
  const { error, loading, refetch, schedules } = useSchedules();
  const { guideChannel, guideTime, visible } = useStreams();

  if (!schedules)
    return (
      <Backdrop fullscreen>
        <NoData
          error={error}
          loading={loading}
          loadingText="Fetching Schedules..."
          reload={() => refetch()}
        />
      </Backdrop>
    );

  return (
    <article>
      <PowerGate>
        {visible.guide && (
          <Guide
            channel={guideChannel}
            schedules={schedules}
            time={guideTime}
          />
        )}
        {visible.infoBar && !visible.guide && <InfoBar />}
        <VideoPlayer />
      </PowerGate>
      <RemoteControl />
    </article>
  );
};

export default App;
