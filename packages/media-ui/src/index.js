/** @jsxImportSource @emotion/react */
import { css, Global } from "@emotion/react";
import config from "@mvpi/ui-config";
import { StreamsProvider } from "@mvpi/ui-media-client";
import styles, { TRUE_BLACK, WHITE } from "@mvpi/ui-styles";
import ReactDOM from "react-dom";

import "reset-css";

import App from "./App.js";
import reportWebVitals from "./reportWebVitals.js";

ReactDOM.render(
  // disabling StrictMode to prevent issues with ApolloClient
  // see: https://github.com/lostpebble/pullstate/issues/60
  // <React.StrictMode>
  //   <StreamsProvider config={config}>
  //     <Global styles={styles} />
  //     <App />
  //   </StreamsProvider>
  // </React.StrictMode>,
  <StreamsProvider config={config}>
    <Global styles={styles} />
    <Global
      styles={css`
        body {
          background-color: ${TRUE_BLACK};
          color: ${WHITE};
        }
      `}
    />
    <App />
  </StreamsProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
