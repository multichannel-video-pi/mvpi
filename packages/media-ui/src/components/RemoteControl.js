/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useStreams } from "@mvpi/ui-media-client";
import { GREY } from "@mvpi/ui-styles";
import { useRef } from "react";
import Draggable from "react-draggable";

import {
  RemoteControl as UIRemoteControl,
  useKeyboardRemoteControl,
  useRemoteServiceActions,
} from "@mvpi/ui-remote-client";

const RemoteControl = () => {
  const nodeRef = useRef(null);
  const { visible } = useStreams();

  useKeyboardRemoteControl();
  useRemoteServiceActions();

  if (!visible.remote) return null;

  return (
    <Draggable bounds="parent" nodeRef={nodeRef}>
      <div
        css={css`
          position: absolute;
          z-index: 1;
          overflow: hidden;
          top: 0;
          right: 0;
          border: 2px solid ${GREY};
          border-radius: 2rem;
        `}
        ref={nodeRef}
      >
        <UIRemoteControl />
      </div>
    </Draggable>
  );
};

export default RemoteControl;
