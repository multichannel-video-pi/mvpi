/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useStreams } from "@mvpi/ui-media-client";
import { key } from "@mvpi/util";

const STREAMS_PLAYER_KEY = key`STREAMS_PLAYER_KEY`;

const VideoPlayer = () => {
  const { playerRef } = useStreams();

  return (
    <video
      css={css`
        width: 100vw;
        height: 100vh;
      `}
      key={STREAMS_PLAYER_KEY}
      ref={playerRef}
    />
  );
};

export default VideoPlayer;
