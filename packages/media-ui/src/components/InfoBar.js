/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { posterUrls } from "@mvpi/tmdb-images";
import { useStreams } from "@mvpi/ui-media-client";
import { useSchedules } from "@mvpi/ui-metadata-client";
import { BLUE_DARK } from "@mvpi/ui-styles";
import { formatTitle } from "@mvpi/util";
import { format } from "date-fns";
import { useEffect, useMemo } from "react";

const InfoBar = () => {
  const { channels, schedules } = useSchedules();
  const { playerChannel, selected, setVisibility, startTime } = useStreams();

  const channel = useMemo(() => channels.find((t) => playerChannel === t.id), [
    channels,
    playerChannel,
  ]);

  const videos = useMemo(
    () => [...new Set(schedules.map((schedule) => schedule.video))],
    [schedules]
  );

  const video = useMemo(() => videos.find((v) => selected === v.id), [
    selected,
    videos,
  ]);

  const poster = useMemo(() => posterUrls(video), [video]);

  const formattedVideoTime = useMemo(() => {
    if (!startTime) return null;
    return {
      start: format(startTime, "h:mm a"),
      end: format(startTime + (video?.duration || 0) * 1000, "h:mm a"),
    };
  }, [video, startTime]);

  useEffect(() => {
    if (!selected) return () => null;
    const interval = setInterval(
      () => setVisibility({ view: "infoBar", visible: false }),
      5000
    );
    return () => clearInterval(interval);
  }, [selected, setVisibility]);

  if (!selected) return null;

  const formattedDate = format(new Date(), "E, MMM dd h:mm a");
  const formattedTitle = video && formatTitle(video);

  const formattedReleaseDate =
    video.releaseDate &&
    format(new Date(`${video.releaseDate} 00:00:00`), "MM/dd/yyyy");

  const tags = video.tags
    .map((tag) => tag.name)
    .sort()
    .join(", ");

  return (
    <header
      css={css`
        position: absolute;
        z-index: 1;
        width: 100vw;
        padding: 1rem;
        background-color: ${BLUE_DARK};
      `}
    >
      <span
        css={css`
          float: right;
          font-size: 1.5rem;
        `}
      >{`${formattedDate}`}</span>

      <h1
        css={css`
          margin-bottom: 0.5rem;
          font-size: 1.5rem;
        `}
      >
        #{channel?.name || "MVPi"}
      </h1>

      <div
        css={css`
          margin-bottom: 1rem;
        `}
      >
        {formattedVideoTime.start} - {formattedVideoTime.end}
      </div>

      {!!poster && (
        <div
          css={css`
            float: left;
            padding-right: 1rem;
          `}
        >
          <img alt="poster" src={poster.w92} />
        </div>
      )}

      <h2
        css={css`
          font-size: 2rem;
        `}
      >
        {formattedTitle}
      </h2>

      <div
        css={css`
          margin-top: 0.25rem;
          font-size: 0.75rem;
          line-height: 1.5;
        `}
      >
        {formattedReleaseDate && `${formattedReleaseDate} - `} {tags}
      </div>

      {!!video.overview && (
        <p
          css={css`
            margin-top: 0.5rem;
            line-height: 1.5;
          `}
        >
          {video.overview}
        </p>
      )}
    </header>
  );
};

export default InfoBar;
