import { Backdrop, Section } from "@mvpi/ui-helpers";
import { useStreams } from "@mvpi/ui-media-client";
import React from "react";

const PowerGate = ({ children }) => {
  const { power } = useStreams();

  if (!power)
    return (
      <Backdrop fullscreen>
        <Section>Turn Power On</Section>
      </Backdrop>
    );

  return children;
};

export default PowerGate;
