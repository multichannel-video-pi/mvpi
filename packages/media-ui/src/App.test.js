import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "./App.js";

test("renders learn react link", () => {
  render(<App />);
  const linkElement = screen.getByText(/MVPi/i);
  userEvent.hover(linkElement);
  expect(linkElement).toBeInTheDocument();
});
