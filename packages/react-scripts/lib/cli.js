#!/usr/bin/env node

import spawnMVPiReactScripts from "./index.js";

const args = process.argv.slice(2);
const cwd = process.cwd();
const env = process.env;
const logger = console;

(async () => {
  try {
    await spawnMVPiReactScripts({ args, cwd, env });
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
