import config from "@mvpi/config";
import { spawn } from "child_process";
import { promises as fs } from "fs";
import path from "path";

const spawnMVPiReactScripts = async ({ args, cwd, env }) => {
  const pkg = JSON.parse(
    await fs.readFile(path.join(cwd, "package.json"), "utf8")
  );
  const key = pkg.name.replace(/^@mvpi\//, "").replace(/-ui$/, "");
  const { prefix, port } = config[key]?.ui || {};

  if (!prefix) throw new Error(`missing value "prefix" in config.${key}.ui`);
  if (!port) throw new Error(`missing value "port" in config.${key}.ui`);

  return spawn("react-scripts", args, {
    cwd,
    env: { ...env, PORT: port, PUBLIC_URL: prefix },
    stdio: "inherit",
  });
};

export default spawnMVPiReactScripts;
