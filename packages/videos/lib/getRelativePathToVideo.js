import config from "@mvpi/config";
import path from "path";

const getRelativePathToVideo = (filename) => {
  return filename.replace(`${config.videos.directory}${path.sep}`, "");
};

export default getRelativePathToVideo;
