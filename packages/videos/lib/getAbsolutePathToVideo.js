import config from "@mvpi/config";
import path from "path";

const getAbsolutePathToVideo = (filename) => {
  return filename.startsWith(path.sep)
    ? filename
    : path.join(config.videos.directory, filename);
};

export default getAbsolutePathToVideo;
