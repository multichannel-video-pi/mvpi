import { ffprobe } from "@mvpi/ffmpeg";

import getAbsolutePathToVideo from "./getAbsolutePathToVideo.js";
import getRelativePathToVideo from "./getRelativePathToVideo.js";
import { insertVideo } from "./tableUtilities.js";

const createVideo = async (values, info) => {
  const absolutePathToVideo = getAbsolutePathToVideo(values.filename);

  const {
    format: { bit_rate: bitrate, duration, size },
  } = await ffprobe(absolutePathToVideo);

  const filename = getRelativePathToVideo(values.filename);

  return insertVideo({ ...values, bitrate, duration, filename, size }, info);
};

export default createVideo;
