import { PARENT_ID } from "@mvpi/database";

export const VIDEOS_TABLE = "Videos";
export const VIDEO_TAGS_TABLE = "VideoTags";

export const DENORMALIZED_VIDEO_FIELDS = {
  id: "Videos.id",
  bitrate: "Videos.bitrate",
  duration: "Videos.duration",
  filename: "Videos.filename",
  isActive: "Videos.isActive",
  overview: "Videos.overview",
  rating: "Videos.rating",
  releaseDate: "Videos.releaseDate",
  size: "Videos.size",
  title: "Videos.title",
  voteAverage: "Videos.voteAverage",
  voteCount: "Videos.voteCount",

  movie__id: "Movies.id",
  movie__backdropPath: "Movies.backdropPath",
  movie__chronology: "Movies.chronology",
  movie__posterPath: "Movies.posterPath",
  movie__tagline: "Movies.tagline",
  movie__tmdbId: "Movies.tmdbId",

  movie__collection__id: "Collections.id",
  movie__collection__backdropPath: "Collections.backdropPath",
  movie__collection__name: "Collections.name",
  movie__collection__overview: "Collections.overview",
  movie__collection__posterPath: "Collections.posterPath",
  movie__collection__tmdbId: "Collections.tmdbId",

  televisionEpisode__id: "TelevisionEpisodes.id",
  televisionEpisode__episodeNumber: "TelevisionEpisodes.episodeNumber",
  televisionEpisode__seasonNumber: "TelevisionEpisodes.seasonNumber",
  televisionEpisode__stillPath: "TelevisionEpisodes.stillPath",

  televisionEpisode__televisionShow__id: "TelevisionShows.id",
  televisionEpisode__televisionShow__backdropPath:
    "TelevisionShows.backdropPath",
  televisionEpisode__televisionShow__firstAirDate:
    "TelevisionShows.firstAirDate",
  televisionEpisode__televisionShow__lastAirDate: "TelevisionShows.lastAirDate",
  televisionEpisode__televisionShow__name: "TelevisionShows.name",
  televisionEpisode__televisionShow__overview: "TelevisionShows.overview",
  televisionEpisode__televisionShow__posterPath: "TelevisionShows.posterPath",
  televisionEpisode__televisionShow__rating: "TelevisionShows.rating",
  televisionEpisode__televisionShow__tagline: "TelevisionShows.tagline",
  televisionEpisode__televisionShow__tmdbId: "TelevisionShows.tmdbId",
  televisionEpisode__televisionShow__voteAverage: "TelevisionShows.voteAverage",
  televisionEpisode__televisionShow__voteCount: "TelevisionShows.voteCount",
};

export const DENORMALIZED_VIDEO_LEFT_JOINS = [
  ["Movies", "Movies.videoId = Videos.id"],
  ["Collections", "Movies.collectionId = Collections.id"],
  ["TelevisionEpisodes", "TelevisionEpisodes.videoId = Videos.id"],
  [
    "TelevisionShows",
    "TelevisionEpisodes.televisionShowId = TelevisionShows.id",
  ],
];

export const DENORMALIZED_VIDEO_TAG_FIELDS = {
  id: "Tags.id",
  name: "Tags.name",
  overview: "Tags.overview",
  tmdbId: "Tags.tmdbId",
  [PARENT_ID]: "Videos.id",
};

export const DENORMALIZED_VIDEO_TAG_LEFT_JOINS = [
  ["Videos", "VideoTags.videoId = Videos.id"],
  ["Tags", "VideoTags.tagId = Tags.id"],
];
