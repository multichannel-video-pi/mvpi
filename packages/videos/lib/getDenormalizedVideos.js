import database, { mergeAndUnflatten, selectFrom } from "@mvpi/database";

import {
  DENORMALIZED_VIDEO_FIELDS,
  DENORMALIZED_VIDEO_LEFT_JOINS,
  DENORMALIZED_VIDEO_TAG_FIELDS,
  DENORMALIZED_VIDEO_TAG_LEFT_JOINS,
  VIDEOS_TABLE,
  VIDEO_TAGS_TABLE,
} from "./constants.js";

const getDenormalizedVideos = async (conditions, order) => {
  const db = await database();

  const videosQuery = selectFrom(VIDEOS_TABLE)
    .fields(DENORMALIZED_VIDEO_FIELDS)
    .leftJoins(DENORMALIZED_VIDEO_LEFT_JOINS)
    .where(conditions)
    .orderBy(order);

  const videoTagsQuery = selectFrom(VIDEO_TAGS_TABLE)
    .fields(DENORMALIZED_VIDEO_TAG_FIELDS)
    .leftJoins(DENORMALIZED_VIDEO_TAG_LEFT_JOINS)
    .where(conditions);

  const [videos, tags] = await Promise.all([
    db.all(videosQuery),
    db.all(videoTagsQuery),
  ]);

  return videos.map((video) => mergeAndUnflatten(video, { tags }, ["tags"]));
};

export default getDenormalizedVideos;
