import { createTableUtilities } from "@mvpi/database";
import { VIDEOS_TABLE } from "./constants.js";

export const {
  delete: deleteVideos,
  deleteByKey: deleteVideo,
  insert: insertVideo,
  select: selectVideos,
  selectByKey: selectVideo,
  update: updateVideosORIGINAL,
  updateByKey: updateVideo,
  column,
  getConditions,
  getOrder,
  getPrimaryKeyConditions,
} = createTableUtilities(VIDEOS_TABLE);
