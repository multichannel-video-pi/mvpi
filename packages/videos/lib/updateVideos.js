import { getChunks } from "@mvpi/util";

import { column, updateVideosORIGINAL } from "./tableUtilities.js";

const updateVideos = async (params, conditions, order, info) => {
  const { ids, ...values } = params;
  const idChunks = getChunks(ids, 25);
  return (
    await Promise.all(
      idChunks.map(async (idChunk) =>
        updateVideosORIGINAL(values, [[column`id`, "IN", idChunk]], order, info)
      )
    )
  ).flat();
};

export default updateVideos;
