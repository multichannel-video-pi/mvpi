import { ValidationError } from "@mvpi/errors";
import { url } from "@mvpi/util";
import websocket from "websocket";

import { dispatch, subscribe, unsubscribe } from "./actions.js";

const W3CWebSocket = websocket.w3cwebsocket;

const APPLICATION_PROTOCOL = Symbol("APPLICATION_PROTOCOL");
const BASE = Symbol("BASE");
const LOGGER = Symbol("LOGGER");
const ON_MESSAGE = Symbol("ON_MESSAGE");
const RECONNECT = Symbol("RECONNECT");
const SOCKET = Symbol("SOCKET");

class RemoteClient {
  constructor(config, logger) {
    this[APPLICATION_PROTOCOL] = config.remote.service.applicationProtocol;
    this[BASE] = url`${config.remote.service}`;
    this[LOGGER] = logger || console;
    this[ON_MESSAGE] = () => null;
    this[RECONNECT] = true;
    this[SOCKET] = this.connect();
  }

  connect() {
    const socket = (this[SOCKET] = new W3CWebSocket(
      this[BASE],
      this[APPLICATION_PROTOCOL]
    ));

    socket.onclose = () => {
      if (this[RECONNECT]) {
        this[LOGGER].warn(
          `connection to ${this[BASE]} has closed, reconnecting in 5 seconds`
        );
        setTimeout(() => this.connect(), 5000);
      } else {
        this[LOGGER].warn(`connection to ${this[BASE]} has closed`);
      }
    };

    socket.onerror = (err) => {
      this[LOGGER].error(err);
      socket.close();
    };

    socket.onopen = () => {
      this[LOGGER].debug(`connected to websocket ${this[BASE]}`);
      socket.send(subscribe());
    };

    socket.onmessage = (message) => this.onmessage(message);

    return socket;
  }

  close() {
    this[RECONNECT] = false;
    this.socket.close();
  }

  dispatch(action) {
    this.socket.send(dispatch(action));
  }

  subscribe() {
    this.socket.send(subscribe());
  }

  unsubscribe() {
    this.socket.send(unsubscribe());
  }

  get onmessage() {
    return this[ON_MESSAGE];
  }

  set onmessage(fn) {
    if (typeof fn !== "function")
      throw new ValidationError("onmessage value must be a function");
    this[ON_MESSAGE] = fn;
  }

  get socket() {
    return this[SOCKET];
  }
}

export default RemoteClient;
