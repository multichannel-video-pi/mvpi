import { key } from "@mvpi/util";

export const REMOTE_CLIENT_DISPATCH = key`REMOTE_CLIENT_DISPATCH`;
export const REMOTE_CLIENT_SUBSCRIBE = key`REMOTE_CLIENT_SUBSCRIBE`;
export const REMOTE_CLIENT_UNSUBSCRIBE = key`REMOTE_CLIENT_UNSUBSCRIBE`;

export const dispatch = (action) =>
  JSON.stringify({
    payload: JSON.stringify(action),
    type: REMOTE_CLIENT_DISPATCH,
  });

export const subscribe = () =>
  JSON.stringify({ type: REMOTE_CLIENT_SUBSCRIBE });

export const unsubscribe = () =>
  JSON.stringify({ type: REMOTE_CLIENT_UNSUBSCRIBE });

export default dispatch;
