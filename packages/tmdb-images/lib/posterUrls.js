import { POSTER_SIZES } from "./constants.js";
import imageUrls from "./imageUrls.js";

const posterUrls = (video) => {
  const { movie, televisionEpisode } = video || {};
  const { collection } = movie || {};
  const { televisionShow } = televisionEpisode || {};

  if (televisionShow && televisionShow.posterPath) {
    return imageUrls(televisionShow.posterPath, POSTER_SIZES);
  }

  if (movie && movie.posterPath) {
    return imageUrls(movie.posterPath, POSTER_SIZES);
  }

  if (collection && collection.posterPath) {
    return imageUrls(collection.posterPath, POSTER_SIZES);
  }

  return null;
};

export default posterUrls;
