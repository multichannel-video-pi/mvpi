import { SECURE_BASE_URL } from "./constants.js";

const imageUrls = (path, sizes) => {
  return Object.fromEntries(
    sizes.map((size) => [size, `${SECURE_BASE_URL}${size}${path}`])
  );
};

export default imageUrls;
