import { STILL_SIZES } from "./constants.js";
import imageUrls from "./imageUrls.js";

const stillUrls = (video) => {
  const { televisionEpisode } = video || {};

  if (televisionEpisode && televisionEpisode.stillPath) {
    return imageUrls(televisionEpisode.stillPath, STILL_SIZES);
  }

  return null;
};

export default stillUrls;
