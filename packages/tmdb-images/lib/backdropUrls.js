import { BACKDROP_SIZES } from "./constants.js";
import imageUrls from "./imageUrls.js";

const backdropUrls = (video) => {
  const { movie, televisionEpisode } = video || {};
  const { collection } = movie || {};
  const { televisionShow } = televisionEpisode || {};

  if (televisionShow && televisionShow.backdropPath) {
    return imageUrls(televisionShow.backdropPath, BACKDROP_SIZES);
  }

  if (movie && movie.backdropPath) {
    return imageUrls(movie.backdropPath, BACKDROP_SIZES);
  }

  if (collection && collection.backdropPath) {
    return imageUrls(collection.backdropPath, BACKDROP_SIZES);
  }

  return null;
};

export default backdropUrls;
