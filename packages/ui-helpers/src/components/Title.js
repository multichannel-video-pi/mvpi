/** @jsx jsx */
import { css, jsx } from "@emotion/react";

const styles = {
  h1: css`
    margin-top: 3rem;
    margin-bottom: 3rem;
    font-size: 3rem;
  `,
  h2: css`
    margin-top: 2.5rem;
    margin-bottom: 2.5rem;
    font-size: 2.5rem;
  `,
  h3: css`
    margin-top: 2rem;
    margin-bottom: 2rem;
    font-size: 2rem;
  `,
  h4: css`
    margin-top: 1.5rem;
    margin-bottom: 1.5rem;
    font-size: 1.5rem;
  `,
  h5: css`
    margin-top: 1.25rem;
    margin-bottom: 1.25rem;
    font-size: 1.25rem;
  `,
  h6: css`
    margin-top: 1rem;
    margin-bottom: 1rem;
    font-size: 1rem;
  `,
};

const Title = ({ children, color, margin, type = 1, size = type }) => {
  const TitleTag = `h${type}`;
  return (
    <TitleTag
      css={css`
        ${styles[`h${size}`] || ""}
        ${margin ? `margin: ${margin}` : ""};
        ${color ? `color: ${color}` : ""};
        line-height: 1.125;
        font-weight: 700;
      `}
    >
      {children}
    </TitleTag>
  );
};

export default Title;
