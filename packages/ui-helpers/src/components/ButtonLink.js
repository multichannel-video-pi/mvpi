/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { BLUE, RED } from "@mvpi/ui-styles";

const ButtonLink = ({ children, ...props }) => (
  <button
    css={css`
      margin: 0;
      border: 0;
      padding: 0;
      font-family: inherit;
      font-size: inherit;
      font-weight: inherit;
      letter-spacing: inherit;
      text-decoration: underline;
      color: ${BLUE};
      cursor: pointer;

      &:active {
        color: ${RED};
      }
    `}
    type="button"
    {...props}
  >
    {children}
  </button>
);

export default ButtonLink;
