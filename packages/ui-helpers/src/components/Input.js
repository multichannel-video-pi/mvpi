/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useField } from "formik";
import { Fragment } from "react";

import {
  BLACK_BIS,
  BLUE,
  BLUE_LIGHT,
  BLUE_LIGHTEST,
  GREY_LIGHT,
  GREY_LIGHTER,
  MIN_INPUT_HEIGHT,
} from "@mvpi/ui-styles";

const Input = ({ inline, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <Fragment>
      <input
        css={css`
          outline: 0 !important;
          margin-bottom: 1rem;

          border: 1px solid ${GREY_LIGHTER};
          padding: 0.25rem 0.75rem;
          min-height: ${MIN_INPUT_HEIGHT};
          font-size: 1rem;
          letter-spacing: 1px;
          color: ${BLACK_BIS};
          transition: all 100ms;

          ${!inline && "width: 100%"};

          label + &,
          label > & {
            margin-top: 0.25rem;
          }

          &::placeholder {
            font-weight: 300;
            color: ${GREY_LIGHTER};
            transition: color 100ms;
          }

          &:hover {
            border-color: ${GREY_LIGHT};

            &::placeholder {
              color: ${GREY_LIGHT};
            }
          }

          &:focus {
            border-color: ${BLUE};
            background-color: ${BLUE_LIGHTEST};

            &::placeholder {
              color: ${BLUE_LIGHT};
            }
          }
        `}
        {...field}
        {...props}
      />
      {meta.error && meta.touched && <div>{meta.error}</div>}
    </Fragment>
  );
};

export default Input;
