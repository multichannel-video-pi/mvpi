/** @jsx jsx */
import { css, jsx } from "@emotion/react";

const Backdrop = ({
  backgroundColor,
  color,
  fullscreen,
  children,
  ...props
}) => {
  return (
    <div
      css={css`
        display: flex;
        align-items: center;
        justify-content: center;
        width: ${fullscreen ? "100vw" : "100%"};
        height: ${fullscreen ? "100vh" : "100%"};
        ${backgroundColor && `background-color: ${backgroundColor};`}
        ${color && `color: ${color};`}
      `}
      {...props}
    >
      {children}
    </div>
  );
};

export default Backdrop;
