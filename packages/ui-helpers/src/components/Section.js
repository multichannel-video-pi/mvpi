/** @jsx jsx */
import { css, jsx } from "@emotion/react";

const Section = ({ children, ...props }) => (
  <section
    css={css`
      margin: 1rem 1rem 2rem;
    `}
    {...props}
  >
    {children}
  </section>
);

export default Section;
