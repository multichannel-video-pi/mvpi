/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useField, useFormikContext } from "formik";
import { Fragment, useEffect, useMemo, useState } from "react";

import {
  BLACK_BIS,
  BLUE,
  BLUE_LIGHT,
  BLUE_LIGHTEST,
  GREY_LIGHT,
  GREY_LIGHTER,
  MIN_INPUT_HEIGHT,
} from "@mvpi/ui-styles";

import Label from "./Label.js";

const FileInput = ({
  label = "File",
  name,
  placeholder = "Select file...",
  ...props
}) => {
  const [fileLabel, setFileLabel] = useState("");
  const reader = useMemo(() => new FileReader(), []);

  const [field, meta] = useField({ name });
  const { setFieldError, setFieldValue, setFieldTouched } = useFormikContext();

  useEffect(() => {
    const handleEvent = (event) => {
      switch (event.type) {
        case "abort":
          setFieldError(name, `file load aborted for field ${name}`);
          break;
        case "error":
          setFieldError(name, `failed to load file for field "${name}"`);
          break;
        case "load":
          setFieldValue(name, reader.result);
          break;
        default:
          break;
      }
    };

    reader.addEventListener("loadstart", handleEvent);
    reader.addEventListener("load", handleEvent);
    reader.addEventListener("loadend", handleEvent);
    reader.addEventListener("progress", handleEvent);
    reader.addEventListener("error", handleEvent);
    reader.addEventListener("abort", handleEvent);

    return () => {
      reader.removeEventListener("loadstart", handleEvent);
      reader.removeEventListener("load", handleEvent);
      reader.removeEventListener("loadend", handleEvent);
      reader.removeEventListener("progress", handleEvent);
      reader.removeEventListener("error", handleEvent);
      reader.removeEventListener("abort", handleEvent);
    };
  }, [name, reader, setFieldError, setFieldValue]);

  return (
    <Fragment>
      <input type="hidden" name={field.name} value={field.value} />
      <Label>
        {label}
        <input
          css={css`
            position: absolute;
            width: 0;
            height: 0;
            opacity: 0;
          `}
          onBlur={() => setFieldTouched(name, true)}
          onChange={(event) => {
            const [file] = event.currentTarget.files || [];
            if (!file) {
              setFieldValue(name, "");
              return setFileLabel("");
            }
            reader.readAsDataURL(file);
            return setFileLabel(file.name);
          }}
          name={`${name}-file-selector`}
          type="file"
          {...props}
        />
        <div
          css={css`
            display: flex;
            align-items: center;
            margin: 0.25rem 0 1rem;
            border: 1px solid ${GREY_LIGHTER};
            padding: 0.25rem 0.75rem;
            min-height: ${MIN_INPUT_HEIGHT};
            font-size: 1rem;
            font-weight: 400;
            letter-spacing: 1px;
            color: ${BLACK_BIS};
            cursor: text;
            transition: all 100ms;

            label:hover > & {
              border-color: ${GREY_LIGHT};
            }

            input:focus + & {
              border-color: ${BLUE};
              background-color: ${BLUE_LIGHTEST};
            }
          `}
        >
          {fileLabel || (
            <span
              css={css`
                font-weight: 300;
                color: ${GREY_LIGHTER};
                transition: color 100ms;

                label:hover & {
                  color: ${GREY_LIGHT};
                }

                input:focus + div > & {
                  color: ${BLUE_LIGHT};
                }
              `}
            >
              {placeholder}
            </span>
          )}
        </div>
      </Label>
      {meta.error && meta.touched && <div>{meta.error}</div>}
    </Fragment>
  );
};

export default FileInput;
