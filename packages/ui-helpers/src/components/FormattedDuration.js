import { useMemo } from "react";

const FormattedDuration = ({ duration }) => {
  return useMemo(() => {
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration - hours * 3600) / 60);
    const seconds = Math.ceil(duration - (hours * 3600 + minutes * 60));

    const formattedHours = hours && `${hours} hours`;
    const formattedMinutes = minutes && `${minutes} minutes`;
    const formattedSeconds = seconds && `${seconds} seconds`;

    return (
      [formattedHours, formattedMinutes, formattedSeconds]
        .filter((part) => part)
        .join(", ") || "0 seconds"
    );
  }, [duration]);
};

export default FormattedDuration;
