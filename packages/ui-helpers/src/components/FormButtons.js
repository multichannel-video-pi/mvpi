/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useFormikContext } from "formik";
import { Fragment } from "react";

import Button from "./Button.js";

const FormButtons = ({
  isDanger,
  isLoading,
  isSuccess,
  isWarning,
  reset = "Reset",
  submit = "Submit",
}) => {
  const { dirty, errors, isSubmitting, isValid, touched } = useFormikContext();

  const hasTouchedErrors = !!Object.entries(errors).filter(
    ([key, error]) => error && touched[key]
  ).length;

  const isException = hasTouchedErrors || isDanger;
  const isPending = isLoading || isSubmitting;
  const disabled = !dirty || !isValid || isPending;

  return (
    <div
      css={css`
        text-align: right;
      `}
    >
      {dirty && !isPending && (
        <Fragment>
          <Button
            isDanger={isException}
            isSuccess={isSuccess}
            isWarning={isWarning}
            type="reset"
          >
            {reset}
          </Button>{" "}
        </Fragment>
      )}
      <Button
        disabled={disabled}
        isDanger={isException}
        isLoading={isPending}
        isSuccess={isSuccess}
        isWarning={isWarning}
        type="submit"
      >
        {submit}
      </Button>
    </div>
  );
};

export default FormButtons;
