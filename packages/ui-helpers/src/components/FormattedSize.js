import { useMemo } from "react";

const FormattedSize = ({ size }) => {
  return useMemo(() => {
    const gigabytes = Math.round(100 * (size / 1073741824)) / 100;
    const megabytes = Math.round(100 * (size / 1048576)) / 100;
    const kilobytes = Math.round(100 * (size / 1024)) / 100;

    if (gigabytes >= 1) return `${gigabytes} GB`;
    if (megabytes >= 1) return `${megabytes} MB`;
    if (kilobytes >= 1) return `${kilobytes} KB`;
    return `${size} bytes`;
  }, [size]);
};

export default FormattedSize;
