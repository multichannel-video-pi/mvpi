/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar as emptyStar } from "@fortawesome/free-regular-svg-icons";
import { faStar as fullStar } from "@fortawesome/free-solid-svg-icons";
import { BLUE, BLUE_LIGHT } from "@mvpi/ui-styles";
import { useField, useFormikContext } from "formik";
import { Fragment, useMemo, useState } from "react";

const StarsInput = ({ count = 5, inline, name, size }) => {
  const [field, meta] = useField(name);
  const { setFieldValue } = useFormikContext();
  const [hoveredValue, setHoveredValue] = useState(null);

  const starInputs = useMemo(() => {
    return [...Array(count)].map((empty, index) => {
      const value = index + 1;
      const icon =
        (hoveredValue || field.value) >= value ? fullStar : emptyStar;
      return { icon, value };
    });
  }, [count, field.value, hoveredValue]);

  return (
    <Fragment>
      <span
        css={css`
          display: inline-flex;
          justify-content: space-evenly;
          margin-bottom: 1rem;
          ${!inline && "width: 100%"};

          label + & {
            ${!inline ? "margin-top: 0.25rem" : "margin-left: 0.25rem"};
          }
        `}
      >
        {starInputs.map(({ icon, value }) => (
          <label
            css={css`
              flex-grow: 1;
              text-align: center;
              max-height: ${size || "1em"};
              font-size: ${size || "inherit"};
              color: ${BLUE};
              cursor: pointer;

              &:focus-within,
              &:hover {
                color: ${BLUE_LIGHT};
              }
            `}
            key={value}
            onMouseOver={() => setHoveredValue(value)}
            onMouseOut={() => setHoveredValue(null)}
          >
            <input
              checked={field.value === value}
              css={css`
                position: absolute;
                opacity: 0;
                height: 0;
                width: 0;
              `}
              name={name}
              onBlur={field.onBlur}
              onChange={() => setFieldValue(name, value)}
              type="radio"
              value={value}
            />
            <FontAwesomeIcon
              css={css`
                vertical-align: 0;
              `}
              icon={icon}
            />
          </label>
        ))}
      </span>
      {meta.error && meta.touched && <div>{meta.error}</div>}
    </Fragment>
  );
};

export default StarsInput;
