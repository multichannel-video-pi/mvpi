/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { RED_DARK } from "@mvpi/ui-styles";

const Paragraph = ({ children, isDanger, ...props }) => {
  return (
    <p
      css={css`
        margin: 1rem 0;
        ${isDanger && `color: ${RED_DARK};`}
      `}
      {...props}
    >
      {children}
    </p>
  );
};

export default Paragraph;
