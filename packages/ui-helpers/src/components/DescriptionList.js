/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { GREY_DARK } from "@mvpi/ui-styles";

const DescriptionList = ({ entries = [] }) => {
  return (
    <dl
      css={css`
        margin: 0 0 1rem;
      `}
    >
      {entries.flatMap(([term, details], index) => {
        const termKey = `${term}-term-${index}`;
        const detailsKey = `${term}-details-${index}`;
        return [
          <dt
            key={termKey}
            css={css`
              color: ${GREY_DARK};
              font-weight: 300;
            `}
          >
            {term}
          </dt>,
          <dd
            key={detailsKey}
            css={css`
              margin-bottom: 1rem;

              &:last-child {
                margin-bottom: 0;
              }
            `}
          >
            {details}
          </dd>,
        ];
      })}
    </dl>
  );
};

export default DescriptionList;
