/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useField } from "formik";
import { Fragment } from "react";

import {
  BLACK_BIS,
  BLUE,
  BLUE_LIGHT,
  BLUE_LIGHTEST,
  DEFAULT_FONT,
  GREY_LIGHT,
  GREY_LIGHTER,
} from "@mvpi/ui-styles";

const Textarea = ({ children, inline, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <Fragment>
      <textarea
        css={css`
          outline: 0 !important;
          margin-bottom: 1rem;
          border: 1px solid ${GREY_LIGHTER};
          padding: 0.5rem 0.75rem;
          min-height: 7.25rem;
          resize: vertical;
          font-family: ${DEFAULT_FONT};
          font-size: 1rem;
          line-height: 1.5;
          letter-spacing: 1px;
          color: ${BLACK_BIS};
          transition: all 100ms;

          ${!inline && "width: 100%"};

          label + &,
          label > & {
            margin-top: 0.25rem;
          }

          &::placeholder {
            font-weight: 300;
            color: ${GREY_LIGHTER};
            transition: color 100ms;
          }

          &:hover {
            border-color: ${GREY_LIGHT};

            &::placeholder {
              color: ${GREY_LIGHT};
            }
          }

          &:focus {
            border-color: ${BLUE};
            background-color: ${BLUE_LIGHTEST};

            &::placeholder {
              color: ${BLUE_LIGHT};
            }
          }
        `}
        {...field}
        {...props}
      >
        {children}
      </textarea>
      {meta.error && meta.touched && <div>{meta.error}</div>}
    </Fragment>
  );
};

export default Textarea;
