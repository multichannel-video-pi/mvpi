/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useField, useFormikContext } from "formik";
import { Fragment, useCallback, useMemo, useState } from "react";
import ReactSelect from "react-select";
import ReactSelectCreatable from "react-select/creatable";

import {
  BLUE,
  BLUE_LIGHT,
  BLUE_LIGHTER,
  BLUE_LIGHTEST,
  BLACK,
  BLACK_BIS,
  BLACK_TER,
  GREY_DARKER,
  GREY_DARK,
  GREY,
  GREY_LIGHT,
  GREY_LIGHTER,
  WHITE_TER,
  WHITE_BIS,
  WHITE,
  RED,
  RED_DARK,
} from "@mvpi/ui-styles";

const Select = ({ isCreatable, maxListSize = 50, options = [], ...props }) => {
  const [inputValue, setInputValue] = useState("");

  const [field, meta] = useField(props);
  const { setFieldValue } = useFormikContext();

  const isMulti = Array.isArray(field.value);

  const handleChange = useCallback(
    (selected) => {
      if (isMulti) {
        const selectedArray = Array.isArray(selected) ? selected : [];
        const fieldValue = selectedArray.map(
          (option) => (option || {}).value || ""
        );
        return setFieldValue(field.name, fieldValue);
      }
      const fieldValue = (selected || {}).value || "";
      return setFieldValue(field.name, fieldValue);
    },
    [field.name, isMulti, setFieldValue]
  );

  const mapValueToOption = useCallback(
    (value) =>
      options.find((option) => option.value === value) ||
      (value && { label: value, value }) ||
      "",
    [options]
  );

  const value = useMemo(
    () =>
      isMulti
        ? field.value.map((itemValue) => mapValueToOption(itemValue))
        : mapValueToOption(field.value),
    [field.value, isMulti, mapValueToOption]
  );

  const visibleOptions = useMemo(() => {
    const lowerCaseInputValue = inputValue.toLowerCase();
    return !inputValue
      ? options.slice(0, maxListSize)
      : options
          .filter((option) =>
            [
              option.label,
              option.value,
              ...Object.values(option.meta || {}),
            ].some((prop) =>
              `${prop}`.toLowerCase().includes(lowerCaseInputValue)
            )
          )
          .slice(0, maxListSize);
  }, [inputValue, maxListSize, options]);

  const SelectComponent = useMemo(
    () => (isCreatable ? ReactSelectCreatable : ReactSelect),
    [isCreatable]
  );

  return (
    <Fragment>
      <SelectComponent
        blurInputOnSelect
        filterOption={() => true}
        isMulti={isMulti}
        onBlur={field.onBlur}
        onChange={handleChange}
        onInputChange={(iV) => setInputValue(iV)}
        options={visibleOptions}
        styles={{
          container: (provided) => [
            { ...provided, marginBottom: "1rem" },
            css`
              label + &,
              label > & {
                margin-top: 0.25rem;
              }
            `,
          ],
          control: (provided, state) => ({
            ...provided,
            boxShadow: "none",
            backgroundColor: state.isFocused
              ? BLUE_LIGHTEST
              : provided.backgroundColor,
            cursor: "text",
            fontWeight: 400,
          }),
          clearIndicator: (provided, state) => ({
            ...provided,
            color: state.isFocused ? BLUE : provided.color,
            cursor: "pointer",
          }),
          dropdownIndicator: (provided, state) => ({
            ...provided,
            color: state.isFocused ? BLUE : provided.color,
            cursor: "pointer",
          }),
          indicatorSeparator: (provided, state) => ({
            ...provided,
            backgroundColor: state.isFocused ? BLUE : provided.backgroundColor,
          }),
          input: (provided) => [
            provided,
            css`
              & input {
                letter-spacing: 1px;
              }
            `,
          ],
          multiValue: (provided) => ({
            ...provided,
            backgroundColor: BLUE_LIGHTER,
          }),
          multiValueLabel: (provided) => ({
            ...provided,
            padding: "0.25rem",
            paddingLeft: "0.5rem",
          }),
          multiValueRemove: (provided) => ({
            ...provided,
            cursor: "pointer",
          }),
          placeholder: (provided, state) => [
            {
              ...provided,
              color: state.isFocused ? BLUE_LIGHT : GREY_LIGHTER,
              fontWeight: 300,
            },
            css`
              div:hover > div > & {
                color: ${state.isFocused ? BLUE_LIGHT : GREY_LIGHT};
              }
            `,
          ],
          singleValue: (provided) => ({
            ...provided,
            padding: "1px 0",
          }),
        }}
        theme={(theme) => ({
          ...theme,
          borderRadius: 0,
          colors: {
            ...theme.colors,
            primary: BLUE,
            primary75: BLUE_LIGHT,
            primary50: BLUE_LIGHTER,
            primary25: BLUE_LIGHTEST,
            danger: RED_DARK,
            dangerLight: RED,
            neutral0: WHITE,
            neutral5: WHITE_BIS,
            neutral10: WHITE_TER,
            neutral20: GREY_LIGHTER,
            neutral30: GREY_LIGHT,
            neutral40: GREY,
            neutral50: GREY_DARK,
            neutral60: GREY_DARKER,
            neutral70: BLACK_TER,
            neutral80: BLACK_BIS,
            neutral90: BLACK,
          },
          spacing: {
            ...theme.spacing,
            menuGutter: 4,
          },
        })}
        value={value}
        {...props}
      />
      {meta.error && meta.touched && <div>{meta.error}</div>}
    </Fragment>
  );
};

export default Select;
