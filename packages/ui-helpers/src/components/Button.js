/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useEffect, useState } from "react";

import {
  buttonStyles,
  buttonDangerStyles,
  buttonLoadingStyles,
  buttonWarningStyles,
  buttonSuccessStyles,
} from "@mvpi/ui-styles";

const Button = ({
  children,
  isDanger,
  isLoading,
  isSuccess,
  isWarning,
  onClick,
  type = "button",
  ...props
}) => {
  const [pseudoContent, setPseudoContent] = useState("");

  useEffect(() => {
    if (!isLoading) {
      setPseudoContent("");
      return () => null;
    }

    setPseudoContent("...");
    const interval = setInterval(
      () => setPseudoContent((cur) => (cur.length < 5 ? `${cur}.` : ".")),
      250
    );

    return () => clearInterval(interval);
  }, [isLoading]);

  return (
    <button
      css={css`
        ${buttonStyles}
        ${isSuccess && buttonSuccessStyles}
        ${isWarning && buttonWarningStyles}
        ${isDanger && buttonDangerStyles}
        ${isLoading && buttonLoadingStyles}

        &::before {
          content: "${pseudoContent}";
        }
      `}
      onClick={onClick}
      type={type}
      {...props}
    >
      {children}
    </button>
  );
};

export default Button;
