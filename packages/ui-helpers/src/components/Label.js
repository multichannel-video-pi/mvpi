/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { GREY_DARK } from "@mvpi/ui-styles";

const Label = ({ children, ...props }) => (
  <label
    css={css`
      color: ${GREY_DARK};
      font-weight: 300;
    `}
    {...props}
  >
    {children}
  </label>
);

export default Label;
