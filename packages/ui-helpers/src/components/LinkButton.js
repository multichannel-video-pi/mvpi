/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import {
  buttonStyles,
  buttonDangerStyles,
  buttonLoadingStyles,
  buttonWarningStyles,
  buttonSuccessStyles,
} from "@mvpi/ui-styles";

import Button from "./Button.js";

const LinkButton = ({
  children,
  disabled,
  href,
  isDanger,
  isLoading,
  isSuccess,
  isWarning,
  to,
  ...props
}) => {
  const [pseudoContent, setPseudoContent] = useState("");

  useEffect(() => {
    if (!isLoading) {
      setPseudoContent("");
      return () => null;
    }

    setPseudoContent("...");
    const interval = setInterval(
      () => setPseudoContent((cur) => (cur.length < 5 ? `${cur}.` : ".")),
      250
    );

    return () => clearInterval(interval);
  }, [isLoading]);

  const linkStyles = css`
    ${buttonStyles}
    ${isSuccess && buttonSuccessStyles}
    ${isWarning && buttonWarningStyles}
    ${isDanger && buttonDangerStyles}
    ${isLoading && buttonLoadingStyles}

    display: inline-flex;
    align-items: center;
    justify-content: center;
    text-decoration: none;

    &::before {
      content: "${pseudoContent}";
    }
  `;

  if (disabled)
    return (
      <Button disabled {...props}>
        {children}
      </Button>
    );

  if (to)
    return (
      <Link css={linkStyles} to={to} {...props}>
        {children}
      </Link>
    );

  return (
    <a css={linkStyles} href={href} {...props}>
      {children}
    </a>
  );
};

export default LinkButton;
