/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { desktop, mobileWide, tablet, tabletWide } from "@mvpi/ui-styles";
import { useMemo } from "react";

const Grid = ({
  children,
  columnGap,
  display = "grid",
  rowGap,
  template,
  templateMobileWide,
  templateTablet,
  templateTabletWide,
  templateDesktop,
}) => {
  const styles = useMemo(
    () => css`
      display: ${display};
      ${template ? `grid-template: ${template};` : ""}
      ${columnGap ? `grid-column-gap: ${columnGap};` : ""}
      ${rowGap ? `grid-row-gap: ${rowGap};` : ""}

      ${templateMobileWide
        ? css`
            ${mobileWide} {
              grid-template: ${templateMobileWide};
            }
          `
        : ""}

      ${templateTablet
        ? css`
            ${tablet} {
              grid-template: ${templateTablet};
            }
          `
        : ""}

      ${templateTabletWide
        ? css`
            ${tabletWide} {
              grid-template: ${templateTabletWide};
            }
          `
        : ""}

      ${templateDesktop
        ? css`
            ${desktop} {
              grid-template: ${templateDesktop};
            }
          `
        : ""}
    `,
    [
      columnGap,
      display,
      rowGap,
      template,
      templateMobileWide,
      templateTablet,
      templateTabletWide,
      templateDesktop,
    ]
  );

  return <div css={styles}>{children}</div>;
};

export default Grid;
