/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { NavLink } from "react-router-dom";

const Tab = ({ children, to }) => (
  <li>
    <NavLink
      exact
      css={css`
        text-decoration: none;

        &.active {
          font-weight: 700;
        }
      `}
      to={to}
    >
      {children}
    </NavLink>
  </li>
);

export default Tab;
