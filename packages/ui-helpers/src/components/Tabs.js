/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { GREY_LIGHTER } from "@mvpi/ui-styles";

const Tabs = ({ children }) => (
  <nav
    css={css`
      border-bottom: 1px solid ${GREY_LIGHTER};
    `}
  >
    <ul
      css={css`
        display: flex;
        align-items: center;
        justify-content: space-around;
        margin: 2rem 0 0.5rem;
      `}
    >
      {children}
    </ul>
  </nav>
);

export default Tabs;
