/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { useMemo } from "react";

const GridItem = ({ align, area, children, justify, place }) => {
  const styles = useMemo(
    () => css`
      ${area ? `grid-area: ${area};` : ""}
      ${align ? `align-self: ${align};` : ""}
      ${justify ? `justify-self: ${justify};` : ""}
      ${place ? `place-self: ${place};` : ""}
    `,
    [align, area, justify, place]
  );

  return <div css={styles}>{children}</div>;
};

export default GridItem;
