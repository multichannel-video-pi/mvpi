/** @jsx jsx */
import { css, jsx } from "@emotion/react";

const ImageGrid = ({ altText = "", images = [] }) => {
  return (
    <ul
      css={css`
        margin: 1rem 0;
        text-align: center;
      `}
    >
      {images.map((image, index) => (
        <li
          key={image}
          css={css`
            display: inline-block;
            margin: 0.05rem 0.1rem;
          `}
        >
          <img
            alt={`${altText} (${index + 1} of ${images.length})`}
            css={css`
              width: 160px;
              max-width: 100%;
            `}
            src={image}
          />
        </li>
      ))}
    </ul>
  );
};

export default ImageGrid;
