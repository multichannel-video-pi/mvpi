/** @jsx jsx */
import { css, Global, jsx } from "@emotion/react";
import { BACKGROUND_MASK, WHITE_BIS } from "@mvpi/ui-styles";

const Modal = ({ children, onBackgroundClick, noScroll }) => {
  return (
    <div
      css={css`
        z-index: 9000;
        position: fixed;
        top: 0;
        left: 0;
        width: 100vw;
        height: 100vh;
      `}
    >
      <Global
        styles={css`
          body {
            overflow: hidden;
          }
        `}
      />
      <div
        css={css`
          position: fixed;
          top: 0;
          left: 0;
          width: 100vw;
          height: 100vh;
          background-color: ${BACKGROUND_MASK};
        `}
        onClick={onBackgroundClick}
      ></div>
      <div
        css={css`
          position: absolute;
          top: 50%;
          left: 50%;
          max-width: 100%;
          max-height: 100%;
          overflow: ${noScroll ? "hidden" : "scroll"};
          transform: translate(-50%, -50%);
          background: ${WHITE_BIS};
        `}
      >
        {children}
      </div>
    </div>
  );
};

export default Modal;
