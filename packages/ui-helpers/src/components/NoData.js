/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { RED_DARK } from "@mvpi/ui-styles";
import { Fragment } from "react";

import Button from "./Button.js";
import Section from "./Section.js";

const NoData = ({
  loading,
  loadingText = "Fetching...",
  error,
  reload,
  reloadText = "Reload",
}) => {
  return (
    <Section
      css={css`
        text-align: center;
      `}
    >
      {loading && <p>{loadingText}</p>}
      {!loading && error && (
        <Fragment>
          <p
            css={css`
              padding: 0 0 1rem;
              color: ${RED_DARK};
            `}
          >
            {error.message}
          </p>
          {reload && (
            <Button isDanger onClick={reload}>
              {reloadText}
            </Button>
          )}
        </Fragment>
      )}
    </Section>
  );
};

export default NoData;
