/** @jsx jsx */
import { jsx } from "@emotion/react";
import { useField, useFormikContext } from "formik";
import { useCallback, useMemo } from "react";

const Checkbox = ({
  value,
  checkedValue = value || true,
  uncheckedValue = false,
  children,
  ...props
}) => {
  const [field, meta] = useField(props);
  const { setFieldValue } = useFormikContext();

  const isArray = Array.isArray(field.value);
  const checked = useMemo(() => {
    return isArray
      ? field.value.includes(checkedValue)
      : field.value === checkedValue;
  }, [checkedValue, field.value, isArray]);

  const handleChange = useCallback(() => {
    if (!isArray)
      return setFieldValue(
        field.name,
        field.value === checkedValue ? uncheckedValue : checkedValue
      );
    return setFieldValue(
      field.name,
      field.value.includes(checkedValue)
        ? field.value.filter((item) => item !== checkedValue)
        : [...field.value, checkedValue]
    );
  }, [
    checkedValue,
    field.name,
    field.value,
    isArray,
    setFieldValue,
    uncheckedValue,
  ]);

  return (
    <label>
      <input
        checked={checked}
        type="checkbox"
        onBlur={field.onBlur}
        onChange={handleChange}
        onFocus={field.onFocus}
        {...props}
      />{" "}
      {children}
      {meta.error && meta.touched && <div>{meta.error}</div>}
    </label>
  );
};

export default Checkbox;
