import { url } from "@mvpi/util";
import fetch from "node-fetch";

import {
  BadRequestError,
  ConflictError,
  ForbiddenError,
  NotFoundError,
  UnauthorizedError,
  ValidationError,
} from "@mvpi/errors";

class TMDbClient {
  #baseURL;
  #logger;
  #token;

  constructor(config, logger) {
    this.#baseURL = url`${config.tmdb.service}`;
    this.#logger = logger;
    this.#token = config.tmdb.service.token;
  }

  async getCollectionDetails(id) {
    return this.request(`/collection/${id}`);
  }

  async getMovieDetails(id) {
    return this.request(`/movie/${id}`);
  }

  async getTVEpisodeDetails(tvShowId, seasonNumber, episodeNumber) {
    return this.request(
      `/tv/${tvShowId}/season/${seasonNumber}/episode/${episodeNumber}`
    );
  }

  async getTVShowDetails(id) {
    return this.request(`/tv/${id}`);
  }

  async searchMulti(query) {
    return this.request(`/search/multi?query=${encodeURIComponent(query)}`);
  }

  async searchMovies(query) {
    return this.request(`/search/movie?query=${encodeURIComponent(query)}`);
  }

  async searchTVShows(query) {
    return this.request(`/search/tv?query=${encodeURIComponent(query)}`);
  }

  async request(endpoint, options) {
    const requestURL = `${this.#baseURL}${endpoint}`;
    this.#logger?.debug(`tmdb request: ${requestURL}`);

    const res = await fetch(requestURL, {
      headers: {
        Authorization: `Bearer ${this.#token}`,
        "Content-Type": "application/json;charset=utf-8",
      },
      ...options,
    });

    if (res.ok) return res.json();

    switch (res.status) {
      case 400:
        throw new BadRequestError(res.statusText);
      case 401:
        throw new UnauthorizedError(res.statusText);
      case 403:
        throw new ForbiddenError(res.statusText);
      case 404:
        throw new NotFoundError(res.statusText);
      case 409:
        throw new ConflictError(res.statusText);
      case 422:
        throw new ValidationError(res.statusText);
      default:
        throw new Error(res.statusText);
    }
  }
}

export default TMDbClient;
