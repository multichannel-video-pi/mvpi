import { parseValue } from "@mvpi/util";
import { useEffect, useState } from "react";

const EMPTY_VALUES = [null, undefined];

const useStoredState = (key, defaultValue) => {
  const typeKey = `${key}/TYPE`;
  const storedValue = localStorage.getItem(key);
  const storedType = localStorage.getItem(typeKey);

  const parsedStoreValue =
    storedValue && storedType !== "string"
      ? parseValue(storedValue)
      : storedValue;

  const initialValue = !EMPTY_VALUES.includes(parsedStoreValue)
    ? parsedStoreValue
    : defaultValue;

  const [state, setState] = useState(initialValue);

  useEffect(() => {
    if (EMPTY_VALUES.includes(state)) {
      localStorage.removeItem(key);
      localStorage.removeItem(typeKey);
    } else {
      const stateType = typeof state;
      const stateValue = stateType !== "string" ? JSON.stringify(state) : state;

      localStorage.setItem(key, stateValue);
      localStorage.setItem(typeKey, stateType);
    }
  }, [key, state, typeKey]);

  return [state, setState];
};

export default useStoredState;
