import { formatSize } from "@mvpi/util";
import { useMemo } from "react";

const useFormattedSize = (size) => {
  return useMemo(() => formatSize(size), [size]);
};

export default useFormattedSize;
