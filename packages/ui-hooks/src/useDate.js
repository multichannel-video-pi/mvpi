import { getOffsetDate } from "@mvpi/util";
import { useEffect, useState } from "react";

const useDate = ({ offset = 0, update = 60000 } = {}) => {
  const [date, setDate] = useState(getOffsetDate(offset));

  useEffect(() => {
    setDate(getOffsetDate(offset));
    const interval = setInterval(() => setDate(getOffsetDate(offset)), update);
    return () => {
      clearInterval(interval);
    };
  }, [offset, update]);

  return date;
};

export default useDate;
