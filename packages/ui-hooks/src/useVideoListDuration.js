import { sum } from "@mvpi/util";
import { useMemo } from "react";

const useVideoListDuration = (videos) => {
  return useMemo(() => {
    const durations = videos.map((video) => video.duration).sort();
    const total = sum(durations);
    return !total
      ? { average: 0, median: 0, total: 0 }
      : {
          average: total / durations.length,
          median: durations[Math.ceil(durations.length / 2) - 1],
          total,
        };
  }, [videos]);
};

export default useVideoListDuration;
