import { useReducer } from "react";

const useVisibilityState = (initialState) => {
  const views = Object.keys(initialState);

  return useReducer((state, { view, visible }) => {
    if (!views.includes(view)) throw new Error(`unknown view: ${view}`);

    return {
      ...state,
      [view]: typeof visible === "boolean" ? visible : !state[view],
    };
  }, initialState);
};

export default useVisibilityState;
