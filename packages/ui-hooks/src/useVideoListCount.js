import { useMemo } from "react";

const useVideoListCount = (videos) => {
  return useMemo(() => {
    const collectionCount = videos
      .map((video) => video.movie?.collection?.id)
      .filter((id, index, source) => id && source.indexOf(id) === index).length;

    const collection =
      collectionCount === 1
        ? `${collectionCount} collection`
        : `${collectionCount} collections`;

    const tagCount = videos
      .flatMap((video) => video.tags)
      .map((tag) => tag.id)
      .filter((id, index, source) => id && source.indexOf(id) === index).length;

    const tag = tagCount === 1 ? `${tagCount} tag` : `${tagCount} tags`;

    const televisionShowCount = videos
      .map((video) => video.televisionEpisode?.televisionShow.id)
      .filter((id, index, source) => id && source.indexOf(id) === index).length;

    const televisionShow =
      televisionShowCount === 1
        ? `${televisionShowCount} television show`
        : `${televisionShowCount} television shows`;

    const video =
      videos.length === 1
        ? `${videos.length} video`
        : `${videos.length} videos`;

    return { collection, tag, televisionShow, video };
  }, [videos]);
};

export default useVideoListCount;
