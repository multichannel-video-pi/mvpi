import { formatDuration } from "@mvpi/util";
import { useMemo } from "react";

const useFormattedDuration = (duration) => {
  return useMemo(() => formatDuration(duration), [duration]);
};

export default useFormattedDuration;
