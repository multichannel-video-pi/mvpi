import { sum } from "@mvpi/util";
import { useMemo } from "react";

const useVideoListSize = (videos) => {
  return useMemo(() => {
    const sizes = videos.map((video) => video.size).sort();
    const total = sum(sizes);
    return !total
      ? { average: 0, median: 0, total: 0 }
      : {
          average: total / sizes.length,
          median: sizes[Math.ceil(sizes.length / 2) - 1],
          total,
        };
  }, [videos]);
};

export default useVideoListSize;
