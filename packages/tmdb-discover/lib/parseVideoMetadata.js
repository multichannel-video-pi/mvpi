import { sanitizeFilename } from "@mvpi/util";
import { noCase } from "no-case";

const tvShowRegExp = /(.+)S([0-9]+)E([0-9]+)/i;

const parseVideoMetadata = (video) => {
  const { id, duration, filename } = video;
  const basename = sanitizeFilename(filename);
  const durationInMinutes = Math.round(duration / 60);
  const matches = basename.match(tvShowRegExp);

  if (!matches) {
    return {
      id,
      duration,
      durationInMinutes,
      basename,
      title: noCase(basename),
    };
  }

  const [, rawTitle, seasonNumber, episodeNumber] = matches;
  const title = noCase(rawTitle);

  return {
    id,
    duration,
    durationInMinutes,
    basename,
    title,
    seasonNumber: +seasonNumber,
    episodeNumber: +episodeNumber,
  };
};

export default parseVideoMetadata;
