const titleFragmentChoices = (title, message) => {
  return title
    .split(" ")
    .reduce((accumulated, part) => {
      const last = accumulated[accumulated.length - 1];
      const value = !last ? part : `${last} ${part}`;
      return [...accumulated, value];
    }, [])
    .reverse()
    .map((value) => {
      return {
        title: message ? `${message} "${value}"` : value,
        value,
      };
    });
};

export default titleFragmentChoices;
