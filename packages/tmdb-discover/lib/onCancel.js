import logger from "./logger.js";

const onCancel = () => {
  logger.warn("selection cancelled");
  process.exit(0);
};

export default onCancel;
