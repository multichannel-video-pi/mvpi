#!/usr/bin/env node

import discover from "./discover.js";
import logger from "./logger.js";

(async () => {
  try {
    await discover({ interactive: true });
    process.exit(0);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
})();
