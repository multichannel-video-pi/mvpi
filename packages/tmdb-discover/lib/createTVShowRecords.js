import mem from "mem";

import {
  updateVideo,
  upsertTelevisionEpisode,
  upsertTelevisionShow,
} from "@mvpi/tmdb-sync";

import client from "./client.js";
import logger from "./logger.js";

const memoizedUpsertTelevisionShow = mem(upsertTelevisionShow, {
  cacheKey: ([tvShowDetails]) => tvShowDetails.id,
  maxAge: 3600000,
});

const createTVShowRecords = async (
  video,
  tvShowDetails,
  seasonNumber,
  episodeNumber
) => {
  const existingTelevisionEpisodeId = video.televisionEpisode?.id;
  const existingTelevisionShowId = video.televisionEpisode?.televisionShow.id;
  const existingVideoId = video.id;

  const defaultEpisodeDetails = { episodeNumber, seasonNumber };
  const tvEpisodeDetails =
    Number.isInteger(seasonNumber) && Number.isInteger(episodeNumber)
      ? await client
          .getTVEpisodeDetails(tvShowDetails.id, seasonNumber, episodeNumber)
          .catch((err) => {
            logger.error(err.message);
            return defaultEpisodeDetails;
          })
      : defaultEpisodeDetails;

  const televisionShowId = await memoizedUpsertTelevisionShow(
    tvShowDetails,
    existingTelevisionShowId
  );

  await upsertTelevisionEpisode(
    tvEpisodeDetails,
    televisionShowId,
    existingTelevisionEpisodeId,
    existingVideoId
  );

  return updateVideo(tvEpisodeDetails, existingVideoId);
};

export default createTVShowRecords;
