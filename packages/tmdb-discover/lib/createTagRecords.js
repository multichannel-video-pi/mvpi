import { insertVideoTag, upsertTag } from "@mvpi/tmdb-sync";
import mem from "mem";

const memoizedUpsertTag = mem(upsertTag, {
  cacheKey: ([genre]) => genre.id,
  maxAge: 3600000,
});

const createTagRecords = async (video, details) => {
  const tagIds = await Promise.all(details.genres.map(memoizedUpsertTag));
  return Promise.all(
    tagIds
      .filter((tagId) => !video.tags.some((tag) => tagId === tag.id))
      .map(async (tagId) => insertVideoTag(video.id, tagId))
  );
};

export default createTagRecords;
