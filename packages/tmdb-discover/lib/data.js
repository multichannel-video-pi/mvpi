export const forceTVShowSelectionSet = new Set();
export const selectMediaAsTVShowMap = new Map();
export const selectTVShowMap = new Map();
export const skipMediaSet = new Set();
export const skipTVShowSet = new Set();

export const forceTVShowSelectionFor = (title) =>
  [...forceTVShowSelectionSet].some((fragment) => title.startsWith(fragment));

export const selectMediaAsTVShowFor = (title) =>
  [...selectMediaAsTVShowMap.entries()].find(([fragment]) =>
    title.startsWith(fragment)
  )?.[1];

export const selectTVShowFor = (title) =>
  [...selectTVShowMap.entries()].find(([fragment]) =>
    title.startsWith(fragment)
  )?.[1];

export const skipMediaFor = (title) =>
  [...skipMediaSet].some((fragment) => title.startsWith(fragment));

export const skipTVShowFor = (title) =>
  [...skipTVShowSet].some((fragment) => title.startsWith(fragment));
