import { Video } from "@mvpi/models";
import { serialAsyncMap } from "@mvpi/util";

import createMovieRecords from "./createMovieRecords.js";
import createTagRecords from "./createTagRecords.js";
import createTVShowRecords from "./createTVShowRecords.js";
import getMediaDetails from "./getMediaDetails.js";
import logger from "./logger.js";
import parseVideoMetadata from "./parseVideoMetadata.js";

const discover = async (opts) => {
  const interactive = opts?.interactive === true;

  const videos = await Video.getDenormalizedVideos(
    {},
    { "Videos.filename": "ASC" }
  );

  await serialAsyncMap(videos, async (video) => {
    try {
      const hasTMDBId =
        !!video.movie?.tmdbId ||
        !!video.televisionEpisode?.televisionShow.tmdbId;

      if (hasTMDBId) return null;

      const discoveryMetadata = parseVideoMetadata(video);
      const { seasonNumber, episodeNumber } = discoveryMetadata;

      const details = await getMediaDetails(discoveryMetadata, interactive);
      if (!details) return null;

      await createTagRecords(video, details);

      const isTVShow = !!details.name;
      if (isTVShow) {
        return createTVShowRecords(video, details, seasonNumber, episodeNumber);
      }

      return createMovieRecords(video, details);
    } catch (err) {
      logger.error(err.message);
      return null;
    }
  });
};

export default discover;
