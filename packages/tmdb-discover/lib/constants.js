export const BULK_SKIP = Symbol("bulkSkip");
export const MEDIA = Symbol("media");
export const NEW_SEARCH = Symbol("newSearch");
export const SELECT = Symbol("select");
export const SKIP = Symbol("skip");
export const TV_SHOW = Symbol("tvShow");
