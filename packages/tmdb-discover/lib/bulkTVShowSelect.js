import prompts from "prompts";

import { MEDIA, TV_SHOW } from "./constants.js";
import onCancel from "./onCancel.js";
import titleFragmentChoices from "./titleFragmentChoices.js";

import {
  forceTVShowSelectionSet,
  forceTVShowSelectionFor,
  selectMediaAsTVShowMap,
  selectTVShowMap,
} from "./data.js";

const TYPES = [MEDIA, TV_SHOW];

const bulkTVShowSelect = async (type, tvShow, discoveryMetadata) => {
  if (!TYPES.includes(type)) throw new Error(`unknown type: ${type}`);

  const { basename, title } = discoveryMetadata;
  if (forceTVShowSelectionFor(title)) return tvShow;

  const tvShowYear = new Date(tvShow.first_air_date).getUTCFullYear();
  const tvShowTitle = Number.isInteger(tvShowYear)
    ? `${tvShow.name} (${tvShowYear})`
    : tvShow.name;

  const { alwaysUseSelected } = await prompts(
    {
      initial: true,
      message: `Always use "${tvShowTitle}" for TV shows similar to "${basename}"?`,
      name: "alwaysUseSelected",
      type: "confirm",
    },
    { onCancel }
  );

  if (alwaysUseSelected) {
    const { titleFragment } = await prompts(
      {
        choices: [...titleFragmentChoices(title)],
        message: `Use "${tvShowTitle}" for search queries that start with`,
        name: "titleFragment",
        type: "select",
      },
      { onCancel }
    );
    if (type === TV_SHOW) selectTVShowMap.set(titleFragment, tvShow);
    else selectMediaAsTVShowMap.set(titleFragment, tvShow);
  } else {
    const { titleFragment } = await prompts(
      {
        choices: [...titleFragmentChoices(title)],
        message: `Force interactive selection for search queries that start with`,
        name: "titleFragment",
        type: "select",
      },
      { onCancel }
    );
    forceTVShowSelectionSet.add(titleFragment);
  }

  return tvShow;
};

export default bulkTVShowSelect;
