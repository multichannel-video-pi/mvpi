import { updateVideo, upsertCollection, upsertMovie } from "@mvpi/tmdb-sync";
import mem from "mem";

import client from "./client.js";
import logger from "./logger.js";

const memoizedUpsertCollection = mem(upsertCollection, {
  cacheKey: ([collectionDetails]) => collectionDetails.id,
  maxAge: 3600000,
});

const createMovieRecords = async (video, movieDetails) => {
  const existingMovieId = video.movie?.id;
  const existingCollectionId = video.movie?.collection?.id;
  const existingVideoId = video.id;

  const collectionDetails = movieDetails.belongs_to_collection?.id
    ? await client
        .getCollectionDetails(movieDetails.belongs_to_collection.id)
        .catch((err) => {
          logger.error(err.message);
          return null;
        })
    : null;

  const collectionId =
    collectionDetails &&
    (await memoizedUpsertCollection(collectionDetails, existingCollectionId));

  await upsertMovie(
    movieDetails,
    collectionId,
    existingMovieId,
    existingVideoId
  );

  return updateVideo(movieDetails, existingVideoId);
};

export default createMovieRecords;
