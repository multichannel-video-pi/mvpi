import prompts from "prompts";

import bulkSkip from "./bulkSkip.js";
import bulkTVShowSelect from "./bulkTVShowSelect.js";
import client from "./client.js";
import { BULK_SKIP, MEDIA, NEW_SEARCH, SELECT, SKIP } from "./constants.js";
import onCancel from "./onCancel.js";

const searchAllMediaTypes = async (query, discoveryMetadata, interactive) => {
  const { basename, durationInMinutes, title } = discoveryMetadata;
  const data = await client.searchMulti(query);
  const results = data?.results || [];

  const movieResults = results.filter((media) => media.media_type === "movie");
  const tvShowResults = results.filter((media) => media.media_type === "tv");

  const movieDetails = new WeakMap();
  for (const movieResult of movieResults) {
    const movie = await client.getMovieDetails(movieResult.id);
    const low = durationInMinutes - 1;
    const high = durationInMinutes + 1;
    if (movie.runtime <= high && movie.runtime >= low) return movie;
    movieDetails.set(movieResult, movie);
  }

  if (interactive !== true) return null;

  const resultChoices = [...movieResults, ...tvShowResults].map((result) => {
    switch (result.media_type) {
      case "movie": {
        const runtime = movieDetails.get(result).runtime;
        const resultYear = new Date(result.release_date).getUTCFullYear();
        const resultTitle = Number.isInteger(resultYear)
          ? `${result.title} (${resultYear})`
          : result.title;

        return {
          title: `${resultTitle} (movie) (${runtime} minutes)`,
          description: result.overview,
          value: { payload: result, type: SELECT },
        };
      }
      case "tv": {
        const resultYear = new Date(result.first_air_date).getUTCFullYear();
        const resultTitle = Number.isInteger(resultYear)
          ? `${result.name} (${resultYear})`
          : result.name;

        return {
          title: `${resultTitle} (tv show)`,
          description: result.overview,
          value: { payload: result, type: SELECT },
        };
      }
      default:
        throw new Error("unknown media type");
    }
  });

  const message = !resultChoices.length
    ? `No results for media "${query}"`
    : `Pick a movie or tv show for "${basename}" (${durationInMinutes} minutes)`;

  const { resultsAction } = await prompts(
    {
      type: "select",
      name: "resultsAction",
      message,
      choices: [
        ...resultChoices,
        { title: "new media search", value: { type: NEW_SEARCH } },
        { title: "skip this media", value: { type: SKIP } },
        {
          title: `skip all media similar to "${basename}"`,
          value: { type: BULK_SKIP },
        },
      ],
    },
    { onCancel }
  );

  switch (resultsAction?.type) {
    case BULK_SKIP:
      return bulkSkip(MEDIA, title);

    case NEW_SEARCH: {
      const { newSearchQuery } = await prompts(
        {
          type: "text",
          name: "newSearchQuery",
          message: `Enter new media search query for "${basename}"`,
        },
        { onCancel }
      );
      return searchAllMediaTypes(
        newSearchQuery,
        discoveryMetadata,
        interactive
      );
    }

    case SELECT: {
      const result = resultsAction.payload;
      switch (result.media_type) {
        case "movie":
          return movieDetails.get(result);
        case "tv": {
          const tvShow = await client.getTVShowDetails(result.id);
          return bulkTVShowSelect(MEDIA, tvShow, discoveryMetadata);
        }
        default:
          throw new Error("unknown media type");
      }
    }

    case SKIP:
      return null;

    default:
      throw new Error("unknown selection type");
  }
};

export default searchAllMediaTypes;
