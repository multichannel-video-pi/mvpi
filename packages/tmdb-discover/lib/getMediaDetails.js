import searchAllMediaTypes from "./searchAllMediaTypes.js";
import searchTVShows from "./searchTVShows.js";

import {
  selectMediaAsTVShowFor,
  selectTVShowFor,
  skipMediaFor,
  skipTVShowFor,
} from "./data.js";

const getMediaDetails = async (discoveryMetadata, interactive) => {
  const { episodeNumber, seasonNumber, title } = discoveryMetadata;

  const hasTVShowMetadata =
    Number.isInteger(seasonNumber) && Number.isInteger(episodeNumber);

  if (hasTVShowMetadata) {
    if (skipTVShowFor(title)) return null;
    return (
      selectTVShowFor(title) ||
      searchTVShows(title, discoveryMetadata, interactive)
    );
  }

  if (skipMediaFor(title)) return null;
  return (
    selectMediaAsTVShowFor(title) ||
    searchAllMediaTypes(title, discoveryMetadata, interactive)
  );
};

export default getMediaDetails;
