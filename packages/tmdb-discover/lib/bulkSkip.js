import prompts from "prompts";

import { MEDIA, TV_SHOW } from "./constants.js";
import { skipMediaSet, skipTVShowSet } from "./data.js";
import onCancel from "./onCancel.js";
import titleFragmentChoices from "./titleFragmentChoices.js";

const TYPES = [MEDIA, TV_SHOW];

const bulkSkip = async (type, title) => {
  if (!TYPES.includes(type)) throw new Error(`unknown type: ${type}`);

  const message =
    type === TV_SHOW
      ? "Skip TV shows with search queries that start with"
      : "Skip media with search queries that start with";

  const { titleFragment } = await prompts(
    {
      choices: [...titleFragmentChoices(title)],
      message,
      name: "titleFragment",
      type: "select",
    },
    { onCancel }
  );

  if (type === TV_SHOW) skipTVShowSet.add(titleFragment);
  else skipMediaSet.add(titleFragment);

  return null;
};

export default bulkSkip;
