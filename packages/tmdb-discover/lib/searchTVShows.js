import prompts from "prompts";

import bulkSkip from "./bulkSkip.js";
import bulkTVShowSelect from "./bulkTVShowSelect.js";
import client from "./client.js";
import { BULK_SKIP, NEW_SEARCH, SELECT, SKIP, TV_SHOW } from "./constants.js";
import { forceTVShowSelectionFor, selectTVShowMap } from "./data.js";
import onCancel from "./onCancel.js";

const searchTVShows = async (query, discoveryMetadata, interactive) => {
  const { basename, title } = discoveryMetadata;
  const data = await client.searchTVShows(query);
  const results = data?.results || [];

  if (results.length === 1) {
    const [result] = results;
    const tvShow = await client.getTVShowDetails(result.id);
    if (!forceTVShowSelectionFor(title)) selectTVShowMap.set(title, tvShow);
    return tvShow;
  }

  if (interactive !== true) return null;

  const resultChoices = results.map((result) => {
    const resultYear = new Date(result.first_air_date).getUTCFullYear();
    const resultTitle = Number.isInteger(resultYear)
      ? `${result.name} (${resultYear})`
      : result.name;

    return {
      title: resultTitle,
      description: result.overview,
      value: { payload: result, type: SELECT },
    };
  });

  const message = !resultChoices.length
    ? `No results for TV show "${query}"`
    : `Pick a TV show for "${basename}"`;

  const { resultsAction } = await prompts(
    {
      type: "select",
      name: "resultsAction",
      message,
      choices: [
        ...resultChoices,
        { title: "new TV show search", value: { type: NEW_SEARCH } },
        { title: "skip this TV show", value: { type: SKIP } },
        {
          title: `skip all TV shows similar to "${basename}"`,
          value: { type: BULK_SKIP },
        },
      ],
    },
    { onCancel }
  );

  switch (resultsAction?.type) {
    case BULK_SKIP:
      return bulkSkip(TV_SHOW, title);

    case NEW_SEARCH: {
      const { newSearchQuery } = await prompts(
        {
          type: "text",
          name: "newSearchQuery",
          message: `Enter new TV show search query for "${basename}"`,
        },
        { onCancel }
      );
      return searchTVShows(newSearchQuery, discoveryMetadata, interactive);
    }

    case SELECT: {
      const result = resultsAction.payload;
      const tvShow = await client.getTVShowDetails(result.id);
      return bulkTVShowSelect(TV_SHOW, tvShow, discoveryMetadata);
    }

    case SKIP:
      return null;

    default:
      throw new Error("unknown selection type");
  }
};

export default searchTVShows;
