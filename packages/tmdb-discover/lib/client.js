import config from "@mvpi/config";
import TMDbClient from "@mvpi/tmdb-client";

const client = new TMDbClient(config, null);

export default client;
