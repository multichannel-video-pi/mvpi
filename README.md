# MVPi

**M**ultichannel **V**ideo **Pi** is a project that emulates [multichannel video programming distributors](https://www.law.cornell.edu/definitions/uscode.php?width=840&height=800&iframe=true&def_id=47-USC-1329842821-2064614876&term_occur=1&term_src=title:47:chapter:5:subchapter:III:part:I:section:325) using private media collections and tagging.

## What? Why?

Over time, I have accumulated a large collection of DVDs, Blu-rays, and VHS tapes. The collection has not grown much in recent years, and a lot of the content has sat dormant and unwatched for even longer. 

Inspired by virtual multichannel video services, I decided to digitize this media collection, and serve it in a scheduled, multichannel format over the existing coaxial network in my house using a Raspberry Pi.

In layman's terms, I would described this project as an open-source cable provider that is entirely dependent upon the media collection of the user.

## TODO

- Support captions through a toggle when available.

## License

Copyright (c) 2019-2021 Michael J. Bondra <mjbondra@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
