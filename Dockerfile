# Alias node base image
FROM node:14-alpine as node

# Image for settings up our monorepo
FROM node as builder

WORKDIR /app

COPY package.json lerna.json yarn.lock /app/
COPY packages/ /app/packages/

RUN yarn --frozen-lockfile

# Only build the server project with the "builder" as base
FROM builder as server-builder

RUN cd packages/server && \
  yarn build

# Our minimal node server image
FROM node as server

WORKDIR /app

COPY --from=server-builder /app/packages/server/dist/ /app/

ENTRYPOINT ["node", "/app/server.js"]

# Extend the file for all your docker containers and only copy the resulting bundles from your *-builder image
